AntyCheatHelpers <- {};

local lastKeyTab = {};
local listenKeys = [];

local lastKey = -1;

addEventHandler("onKey", function (key) {
    lastKey = key;

    if(key in lastKeyTab) {
        lastKeyTab[key].tick = getTickCount();
        lastKeyTab[key].work = true;
    }
})

addEventHandler("onRender", function () {
    foreach(key, item in lastKeyTab)
    {
        if(item.work == false)
            continue;

        if(!isKeyPressed(key))
        {
            item.work = false;
            item.tick = getTickCount() - item.tick;
        }
    }
})

function AntyCheatHelpers::getLastKeyTime(key)
{
    if(key in lastKeyTab) {
        if(lastKeyTab[key].work)
            return getTickCount() - lastKeyTab[key].tick;
        else
            return lastKeyTab[key].tick;
    }
    return 0;
}

function AntyCheatHelpers::addKeyToListening(key) {
    if(!(key in lastKeyTab)) 
        lastKeyTab[key] <- { tick = -1, work = false };
}

function AntyCheatHelpers::getLastKey() {
    return lastKey;
}

function AntyCheatHelpers::isComparableIntegers(one, two, diff) {
    if(abs(one - two) > diff)
        return false;

    return true;
}