StatisticsChangeDetector <- {};

local lastCheck = null;

function StatisticsChangeDetector::work() 
{
    local str = getPlayerStrength(heroId);

    if(str > 200)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.AntyCheat);
        packet.writeUInt8(AntyCheatPackets.Statistics);
        packet.writeInt32(str);
        packet.send(RELIABLE_ORDERED);             
    }
}

setTimer(StatisticsChangeDetector.work, 500, 0);