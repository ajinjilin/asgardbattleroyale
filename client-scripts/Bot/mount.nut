local pidtomounts = {};

class createMount extends Bot
{
    mountedUserId = -1;

    constructor(id, streamer, name, posx, posy, posz, angle, mountedUserId, armor, melee, ranged, headTxt, headModel, bodyTxt, bodyModel)
    {
        base.constructor(id, streamer, name);

        setBotPosition(id, posx, posy, posz);    
        setBotAngle(id, angle);

        this.armor = armor;
        this.melee = melee;
        this.ranged = ranged;
        
        this.headTxt = headTxt;
        this.headModel = headModel;
        this.bodyTxt = bodyTxt;
        this.bodyModel = bodyModel;

        pidtomounts[element] <- mountedUserId;

        this.mountedUserId = mountedUserId;
    }

    function spawn() {
        spawned = true;

        setPlayerInstance(element, "PC_HERO");

        setPlayerMaxHealth(element,1000);
        setPlayerHealth(element, health);

        setPlayerAngle(element, angle);
        setPlayerPosition(element, position.x, position.y, position.z);

        if(animation != "STOP")
            playAni(element, animation);

        if(melee != -1)
            equipItem(element, melee);

        if(ranged != -1)
            equipItem(element, ranged);
            
        if(armor != -1)
            equipItem(element, armor);

        setPlayerVisual(element, bodyModel, bodyTxt, headModel, headTxt);

        setPlayerCollision(element, false);
    }

    function unspawn() {
        spawned = false;
    }
}

function removeMounting(pid) {
    if(id, pid in pidtomounts)
        pidtomounts.rawdelete(id);
}

addEventHandler("onRender", function () {
    foreach(bot, player in pidtomounts)
    {
        local pos = getPlayerPosition(player);

        if(pos == null) 
            continue;

        pos.x = pos.x + (sin(getPlayerAngle(player) * 3.14 / 180.0) * 40);
        pos.z = pos.z + (cos(getPlayerAngle(player) * 3.14 / 180.0) * 40);	

        setPlayerPosition(bot, pos.x, pos.y + 30, pos.z);
        playAniIfOther(bot, "S_SMOKE_S1");

        setPlayerAngle(bot, getPlayerAngle(player))
    }       
})