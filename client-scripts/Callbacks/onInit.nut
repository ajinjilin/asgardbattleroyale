
notesDisabled <- false;

addEventHandler("onInit", function () {
    foreach(instance, item in RegisteredItems)
        addItem(instance, item);

    setCursorTxt("CURSOR_ENDLESS.TGA");
    enable_DamageAnims(false);

    srand(getTickCount());

	clearMultiplayerMessages();
    StartGUI.show();

    checkAddons();
    checkWeNeedVobs();
})

local addons = [
    "data\\historia_kolonii_v2.0.vdf",
    "data\\Awantura_Addon.vdf",
    "data\\Eskaton_Language.vdf",
    "data\\Eskaton_GUI.vdf",
    "data\\Eskaton_Languages.vdf",
    "data\\Eskaton_Main.vdf",
    "data\\Eskaton_Armors.vdf",
    "data\\nezeroth.vdf",
    "data\\axyl.vdf",
    "data\\Union.vdf",
    "data\\Awantura_Sekta.vdf",
]

function checkAddons() {
    local kick = false;
    local messageKick = "";
    foreach(addon in addons)
    {
        if(fileMd5(addon) != null)
        {
            messageKick = "Wykryto plik "+addon + ". Uruchom ponownie gre bez tego pliku.";
            kick = true;
        }
    }
    if(fileMd5("data\\AsgardBattleRoyale.vdf") != "3f1c0b3a9b4fbd3106c5cdf3aff4910a")
    {
        kick = true;
        messageKick = "Pobierz addon z naszego discorda lub przez launcher. (Launcher aby pobra� musisz uruchomi� jako administrator).";
    }

    if(kick == true)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Helpers);
        packet.writeUInt8(HelperPackets.Kick);
        packet.writeString(messageKick);
        packet.send(RELIABLE_ORDERED);
    }
}


addEventHandler("onCommand", function (cmd, params) {
    if(cmd == "disable_notes")
    {
        notesDisabled = true;
        Chat.print(255, 0, 0, "Wy��czono drawy 3d");
    }
    if(cmd == "enable_notes")
    {
        notesDisabled = false;
        Chat.print(255, 0, 0, "W��czono drawy 3d");
    }
})


local tempoVobs = [];
function addVob(posx, posy, posz,anglex,angley,anglez,f,vobname)
{
    local vob = Vob(vobname);
    vob.addToWorld()
    vob.setPosition(posx, posy, posz);
    vob.setRotation(anglex,angley,anglez);

    vob.cdDynamic = true;
    vob.cdStatic = true;

    tempoVobs.append(vob);
}

function checkWeNeedVobs()
{
    tempoVobs.clear();

	local world = getWorld()

	switch (world)
	{
		case "NEWWORLD\\NEWWORLD.ZEN":
            TimerTeleport.Deactive();
			break

		case "OLDWORLD\\OLDWORLD.ZEN":
            TimerTeleport.Active();
            addVob(5352.5,294.287,-1582.56,25,-126,0,true,"NW_HARBOUR_CRATE_01.3DS");
            addVob(5780.02,12472.3,-2916.87,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5772.18,12472.3,-3300.08,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5343.23,12472.3,-2909.27,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5339.29,12472.3,-3294.55,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5791.89,12472.3,-2536.37,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5357.89,12472.3,-2527.5,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6216.9,12472.3,-2544.1,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6210.19,12472.3,-2926.33,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6196.47,12472.3,-3304.45,90,91,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5865.36,12665.3,-3371.96,206,360,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5522.36,12665.3,-3369.35,206,360,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5306.35,12665.3,-3366.01,206,360,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5341.96,12665.3,-2455.38,206,541,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5707.91,12665.3,-2464.33,206,541,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6070.92,12665.3,-2467.8,206,541,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6096.95,12665.3,-3373.48,206,722,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5243.97,12643.3,-3260,206,813,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5260.5,12643.3,-2885.9,206,813,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(5276.28,12643.3,-2505.76,206,813,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(6286.65,12679.3,-2473.47,350,1190,0,false,"TPL_LIGHTER_V1.3DS");
            addVob(6264.81,12679.3,-3368.66,350,1190,0,false,"TPL_LIGHTER_V1.3DS");
            addVob(5362.41,12504.3,-2883.68,350,1349,0,false,"TPL_ORCSTATUE.3DS");
            addVob(6646.82,12378.3,-2956,350,1349,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(7008.73,12378.3,-2804.27,350,1349,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(7275.38,12378.3,-3089.68,350,1394,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(7689.22,12345.3,-3004.81,350,1436,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(7996.38,12345.3,-2660.32,350,1436,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(8422.05,12345.3,-2813.23,350,1436,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(8740.03,12345.3,-2682.6,350,1436,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(9176.87,12345.3,-2740.86,350,1436,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(9646.24,12185.3,-2679.03,-1,1436,0,false,"OW_TUNNELCOVER.3DS");
            addVob(10093.8,12183.3,-2636.22,-1,1436,0,false,"OW_TUNNELCOVER.3DS");
            addVob(10569,12173.3,-2598.94,-1,1436,0,false,"OW_TUNNELCOVER.3DS");
            addVob(10582.7,12173.3,-2719.25,-1,1616,0,false,"OW_TUNNELCOVER.3DS");
            addVob(10152.2,12173.3,-2757.56,-1,1616,0,false,"OW_TUNNELCOVER.3DS");
            addVob(9647.01,12173.3,-2801.28,-1,1616,0,false,"OW_TUNNELCOVER.3DS");
            addVob(10958.4,12412.3,-2620.65,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11297.3,12412.3,-2599.88,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11624.9,12412.3,-2574.82,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11965.1,12412.3,-2545.25,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11960.1,12412.3,-2191.89,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11933.6,12417.3,-1834.19,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11972.9,12407.3,-2923.78,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11995.5,12407.3,-3290.44,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11673.5,12407.3,-3306.03,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11650.6,12407.3,-2943.31,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11307.9,12409.3,-2975.42,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(10981.1,12407.3,-3006.34,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11363.3,12407.3,-3331.4,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11017.2,12407.3,-3359.02,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(10929.9,12421.3,-2255.78,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(10899.2,12434.3,-1883.33,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11230.6,12434.3,-1856.05,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11262.1,12426.3,-2222.97,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11593.1,12426.3,-2202.97,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(11561.4,12432.3,-1866.6,91,1616,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(12143.4,12637.3,-3158.39,189,1529,0,false,"EVT_GATE_SMALL_01.3DS");
            addVob(12232.6,12588.3,-2535.54,189,1529,0,false,"EVT_GATE_SMALL_01.3DS");
            addVob(12111.1,12588.3,-1927.48,184,1508,0,false,"EVT_GATE_SMALL_01.3DS");
            addVob(11862.6,12680.3,-1653.47,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11384.2,12680.3,-1689.05,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(10962.6,12680.3,-1700.97,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(10977.2,12980.3,-1720.77,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11388.3,12980.3,-1686.3,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11846.6,12980.3,-1661.92,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11891.6,12651.3,-3457.31,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11388.9,12651.3,-3511.8,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11075.6,12651.3,-3549.23,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11053,13039.3,-3560.36,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11517.6,13039.3,-3528.46,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(11940.3,13039.3,-3499.98,184,1615,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(10790.9,12371.3,-2241.33,359,1615,0,false,"OC_LOB_LIGHTER_HUGE3.3DS");
            addVob(10845.3,12371.3,-3015.63,359,1615,0,false,"OC_LOB_LIGHTER_HUGE3.3DS");
			break

		case "ADDON\\ADDONWORLD.ZEN":
            TimerTeleport.Deactive();
            addVob(1630.8,-994.059,-6202.48,-2,-187,0,false,"NW_HARBOUR_CRATEPILE_01.3DS");
            addVob(1459.61,-994.059,-6317.22,-2,-187,0,false,"NW_HARBOUR_CRATEPILE_01.3DS");
            addVob(1080.83,-643.059,-7310.47,-2,-116,0,false,"PC_BRIDGE_1.3DS");
            addVob(852.456,-643.059,-6843.13,-2,-116,0,false,"PC_BRIDGE_1.3DS");
            addVob(510.318,-606.059,-6343.44,-2,-143,0,false,"PC_BRIDGE_1.3DS");
            addVob(-13.3203,-492.059,-5914.03,91,-326,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-315.413,-492.059,-5724.31,91,-151,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-242.064,-492.059,-6245.52,91,-151,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-510.246,-492.059,-6070.21,91,-151,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(206.845,-492.059,-5581.85,91,-151,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-131.267,-492.059,-5396.49,91,-151,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(338.333,-1112.06,-5497.2,2,-237,0,false,"TPL_EVT_JUMPPLATE_01.3DS");
            addVob(-671.323,-1112.06,-6142.06,2,-328,0,false,"TPL_EVT_JUMPPLATE_01.3DS");
            addVob(-837.358,-586.059,-5905.67,65,-243,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-1177.17,-765.059,-5736.44,65,-243,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-1535.02,-917.059,-5699.27,64,-267,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(302.03,-198.059,-3702.28,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(301.03,-198.059,-3262.16,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(304.03,-198.059,-2841.01,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(304.03,-198.059,-2397.88,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-296.97,-198.059,-3680.14,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-295.97,-198.059,-3243.07,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-292.97,-198.059,-2929,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-294.97,-198.059,-2561.92,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-302.97,-198.059,-2209.84,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-305.97,-198.059,-1886.69,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(299.03,-198.059,-1973.19,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(304.03,-198.059,-1557.07,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-314.97,-198.059,-1453.46,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(311.03,-198.059,-1325.87,90,0,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(-3.30744,-200.059,-3686.06,90,2,0,false,"EVT_ADDON_MAYA_STONEMOVER_01.3DS");
            addVob(1357.67,-779.059,-5821.02,359,-98,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
            addVob(1126.55,-852.059,-4512.46,449,-98,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
            addVob(2119.42,-1074.06,-5621.97,719,-178,0,false,"OW_LONEHUT.3DS");
            addVob(154.589,-568.059,-5329.7,719,-505,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_05.3DS");
            addVob(197.224,-555.059,-5338.41,719,-154,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_07.3DS");
            addVob(1455.59,-1146.06,-5483.64,734,-124,0,false,"ORC_GATE_5X5M.3DS");
            addVob(2969.63,-1204.06,-5735.57,734,-150,0,false,"ORC_GATE_5X5M.3DS");
            addVob(2921.24,-1204.06,-4821.8,706,-207,0,false,"ORC_GATE_5X5M.3DS");
            addVob(1448.41,-842.059,-7521.04,725,189,0,false,"OC_LOB_STONE.3DS");
            addVob(1270.95,-842.059,-7556.72,725,157,0,false,"OC_LOB_STONE.3DS");
            addVob(1284.63,-784.059,-7540.34,725,157,0,false,"OC_LOB_STONE.3DS");
            addVob(1384.63,-823.059,-7540.34,725,92,0,false,"OC_LOB_STONE.3DS");
            addVob(2239.16,-823.059,-8193.17,725,274,0,false,"OW_TROLLPALISSADE.3DS");
            addVob(1158.86,-782.059,-7548.33,725,274,0,false,"ADDON_MAYA_PILLAR_03.3DS");
            addVob(-570.052,-936.059,-7891.82,1082,-181,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-893.667,-937.059,-7928.13,1,-191,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-31.914,-937.059,-7955.17,1,-164,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1202.83,-937.059,-7915.98,1,-164,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1321.79,-937.059,-8074.23,1,-95,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1294.69,-937.059,-8408.78,1,-95,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1222.41,-937.059,-8726.68,1,249,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1030.13,-932.059,-8965.24,0,214,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(168.913,-932.059,-8150.38,0,431,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(120.105,-932.059,-8426.27,0,490,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(63.0286,-932.059,-8700.42,0,430,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(91.0901,-932.059,-9007.2,0,458,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-94.9413,-932.059,-9131.09,0,543,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-742.431,-932.059,-9064.69,0,546,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-607.026,-932.059,-9082.45,0,546,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-122.696,-748.059,-8068.5,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-200.033,-748.059,-8394.54,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-273.469,-748.059,-8716.48,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-345.703,-748.059,-9008.85,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-53.0353,-746.059,-9075.66,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(46.4166,-746.059,-8767.55,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(121.814,-746.059,-8436.03,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(162.832,-746.059,-8151.73,89,464,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-500.569,-745.059,-8042.78,90,448,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-870.434,-745.059,-8099.41,90,435,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-597.928,-745.059,-8330.89,90,461,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-767.281,-745.059,-8937.68,90,461,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1078.52,-745.059,-8855.27,90,516,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-971.152,-745.059,-8386.31,90,588,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1184.24,-739.059,-8508.88,90,588,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-1195.39,-749.059,-8033.81,90,521,0,false,"EVT_ADDON_NW_TEMPELDOOR_01.3DS");
            addVob(-984.197,-805.059,-7937.17,4,521,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-665.493,-805.059,-7925.66,4,537,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-257.926,-805.059,-7909.26,4,537,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-0.999569,-805.059,-7959.44,4,557,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(255.772,-805.059,-8038.74,4,562,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(283.585,-805.059,-8398.07,4,644,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(222.536,-805.059,-8640.89,4,644,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(177.512,-805.059,-8885.54,4,644,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(56.6022,-805.059,-9261.99,4,644,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(118.754,-805.059,-9063.53,4,644,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-1273.51,-805.059,-8485.75,4,809,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-1265.61,-805.059,-8251.9,4,809,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-1243.93,-805.059,-8761.04,4,809,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-1139.49,-805.059,-8994.04,4,809,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS");
            addVob(-911.491,-1014.06,-8627.77,4,809,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-762.39,-1113.06,-8624.58,4,809,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-610.406,-1182.06,-8623.31,4,809,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-773.781,-640.059,-8202.56,4,809,0,false,"ORC_STANDING_LAMP.3DS");
            addVob(-90.0364,-640.059,-8289.35,4,809,0,false,"ORC_STANDING_LAMP.3DS");
            addVob(-1424.85,-640.059,-7879.87,4,821,0,false,"NW_MISC_DECO_INNOS_LARGE_01.3DS");
            addVob(-1250.8,-640.059,-7911.3,4,999,0,false,"NW_MISC_DECO_INNOS_LARGE_01.3DS");
            addVob(-70.3331,-365.059,-9552.67,4,1269,0,false,"OW_SHIPWRECK_TAIL.3DS");
            addVob(-12068.8,-5612.06,-1367.25,4,1479,0,false,"TPL_EVT_JUMPPLATE_01.3DS");
            addVob(5352.5,294.287,-1582.56,25,-126,0,true,"NW_HARBOUR_CRATE_01.3DS");
            addVob(-13922.4,-1993.78,-3337.3,-16,4,0,false,"OC_BARONSLEDGE_V1.3DS");
            addVob(-13212.4,-1990.78,-3391.31,-16,4,0,false,"OC_BARONSLEDGE_V1.3DS");
            addVob(-13449.6,-1924.78,-2830.91,107,4,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-13379,-2009.78,-2527.65,109,45,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-13196,-2101.78,-2330.42,109,40,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-13023.2,-2161.78,-2152.85,90,302,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-12832.8,-2201.78,-2161.25,118,476,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-12586.5,-2429.78,-2292.93,360,476,0,false,"ORC_SQUAREPLANKS_2X3M.3DS");
            addVob(-13711.1,-1648.78,-3590.67,1,-75,0,false,"OC_LOB_STAIRS_03.3DS");
            addVob(-13540.9,-1951.78,-3375.43,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13650.5,-1951.78,-3334.65,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13768.4,-1951.78,-3317.5,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13609.9,-1967.78,-3272.13,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13736.7,-1967.78,-3265.03,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13487.8,-1967.78,-3323.67,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13492.4,-1967.78,-3444.64,1,-75,0,false,"PC_LOB_TABLE2.3DS");
            addVob(-13632.6,-1349.78,-3325.03,89,-75,0,false,"OC_LOB_GATE_SMALL3.3DS");
            addVob(-13261.9,-1363.78,-3419.49,89,-75,0,false,"OC_LOB_GATE_SMALL3.3DS");
            addVob(-12881.3,-1363.78,-3519.73,89,-75,0,false,"OC_LOB_GATE_SMALL3.3DS");
            addVob(-12754.3,-2102.78,-1929.7,84,-58,0,false,"OC_LOB_GATE_SMALL3.3DS");
            addVob(-12415.8,-2143.78,-2117.85,84,-58,0,false,"OC_LOB_GATE_SMALL3.3DS");
            addVob(-12956,-2454.78,-2588.18,1,40,0,false,"NC_STAIRS_V01.3DS");
            addVob(-13402,-2895.78,-3115.83,1,40,0,false,"NC_STAIRS_V01.3DS");
            addVob(-13056.3,-3306.78,-3436.45,1,220,0,false,"NC_STAIRS_V01.3DS");
            addVob(-13717.9,-3204.78,-3412.21,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13456.5,-3204.78,-3607.32,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13243.2,-3186.78,-3789.78,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13408.5,-3186.78,-3962.04,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13678.7,-3186.78,-3722.13,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13878.7,-3186.78,-3561.9,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-12690.7,-3612.78,-3110.2,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-12787.4,-3606.78,-2991.63,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-12524.2,-3606.78,-2902.53,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-12829.6,-3606.78,-2717.19,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13061.2,-3606.78,-2758.36,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-12960.4,-3606.78,-2609.32,1,220,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-13320.5,-3772.78,-3057.9,1,315,0,false,"OC_LOB_STAIRS_10.3DS");
            addVob(-13479.2,-4125.78,-3140.85,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-14247.6,-4118.78,-3599.5,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-13931.3,-4118.78,-2701.12,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12929,-4118.78,-3536.43,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12363.9,-4118.78,-3998.96,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11820,-4118.78,-3553.71,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11162.3,-4118.78,-2991.61,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11543.4,-4118.78,-2649.98,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12110.6,-4118.78,-3237.93,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12702.4,-4118.78,-2830.85,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12988.5,-4118.78,-2307.22,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-14824.2,-4118.78,-4247.08,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-15091.8,-4118.78,-4773.11,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-14318.3,-4118.78,-4814.03,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-14726.6,-4118.78,-5322.02,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-13347.9,-4118.78,-4476.38,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-13662.6,-4118.78,-4063.91,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-13836.1,-4118.78,-4634.16,88,315,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11085.6,-4405.78,-2185.91,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11743.3,-4405.78,-1630.2,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12423.6,-4405.78,-1068.8,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12014.1,-4774.78,-1011.09,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11402.3,-4774.78,-1399.59,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-11058.4,-4774.78,-1665.99,140,400,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12106.8,-4132.78,-2143.69,87,133,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-12648.2,-4132.78,-1645.57,87,133,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-19895,-2740.78,-7005.91,-1,385,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-20700.6,-2740.78,-6635.48,-1,385,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-21509,-2740.78,-6273.8,-1,385,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-22816.6,-2740.78,-6149.22,-1,350,0,false,"TPL_EVT_SECRETWALL_02.3DS");
            addVob(-21828,-2599.78,-6237.85,-92,387,0,false,"TPL_EVT_PURPURSKULLSTONE_1X1M.3DS");
            addVob(-22433.3,-2614.78,-6195.81,-92,347,0,false,"TPL_EVT_PURPURSKULLSTONE_1X1M.3DS");
            addVob(-21683.9,-2465.78,-6395.22,15,745,0,false,"TPL_DECOHEAD_V1.3DS");
            addVob(-20752.9,-2465.78,-6781.91,15,745,0,false,"TPL_DECOHEAD_V1.3DS");
            addVob(-22075,-2034.78,-5933.51,15,745,0,false,"STALAG_V03.3DS");
            addVob(-19656.7,-3222.78,-7377.53,-15,823,0,false,"TPL_EVT_BRIDGEBROKEN_01.3DS");
            addVob(-20196.2,-2845.78,-6406.83,-49,302,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-19522.1,-2450.78,-6803.57,-77,302,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-22632.7,-2390.78,-3790.68,5,323,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_07.3DS");
            addVob(-22593.8,-2428.78,-3767.58,5,323,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_01.3DS");
            addVob(-22547.3,-2387.78,-3746.48,5,323,0,false,"EVT_ADDON_NW_TROLLPORTAL_PORTAL_05.3DS");
            addVob(-22568.2,-2979.78,-3805.35,26,323,0,false,"PC_LOB_SLEEPER4.3DS");
            addVob(-23812.5,-2902.78,-4365.92,-11,299,0,false,"OW_TROLLPALISSADE.3DS");
            addVob(-21933.1,-2902.78,-3462.46,-11,427,0,false,"OW_TUNNELCOVER.3DS");
            addVob(-21758.9,-2902.78,-3851.79,-11,427,0,false,"OW_TUNNELCOVER.3DS");
            addVob(-22111.9,-2902.78,-4535.68,-11,427,0,false,"ADDON_MISC_HOLZSTUETZEN_01.3DS");
            addVob(-22493.3,-2727.78,-4671.91,7,337,0,false,"EVT_OC_MAINGATE02_02.3DS");
            addVob(-22804.1,-2727.78,-4797.23,7,337,0,false,"EVT_OC_MAINGATE02_02.3DS");
            addVob(-23106.5,-2727.78,-4922.11,7,337,0,false,"EVT_OC_MAINGATE02_02.3DS");
            addVob(-23426.5,-2727.78,-5058.99,7,337,0,false,"EVT_OC_MAINGATE02_02.3DS");
            addVob(-23694,-2727.78,-5171.21,7,337,0,false,"EVT_OC_MAINGATE02_02.3DS");
            addVob(-22942.2,-2959.78,-5671.3,-4,235,0,false,"MIN_LOB_MELTER.3DS");
            addVob(-16377.2,-7780.78,-8934.33,-3,8,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16251.2,-7881.78,-8855.71,-3,8,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16347.9,-7698.78,-9207.11,-3,8,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16327.1,-7616.78,-9448.35,-3,8,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16527.2,-7505.78,-9593.85,-3,8,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16787.8,-4967.78,-9050.11,-3,-45,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16865.3,-5150.78,-9354.62,-3,-45,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16835.6,-5364.78,-9545.86,-3,-45,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16650.6,-5563.78,-9913.91,-3,-150,0,false,"MIN_LOB_WINKELSTEG.3DS");
            addVob(-16424.4,-5927.78,-9783.81,4,180,0,false,"MIN_LOB_BRIDGE_4X4M.3DS");
            addVob(-16047.6,-6680.78,-8596.66,4,191,0,false,"MIN_LOB_BRIDGE_4X4M.3DS");
            addVob(-16080.2,-5874.78,-9120.92,2,106,0,false,"OC_LOB_STAIRS_10.3DS");
            addVob(-15994.1,-6239.78,-8760.61,2,106,0,false,"OC_LOB_STAIRS_10.3DS");
            addVob(-16224.8,-6668.78,-8810.4,0,287,0,false,"OC_LOB_STAIRS_10.3DS");
            addVob(-16347.5,-7133.78,-9259.39,0,287,0,false,"OC_LOB_STAIRS_10.3DS");
            addVob(-16461.7,-7435.78,-9605.12,0,458,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-16405.8,-7435.78,-9821.08,0,458,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-16211.6,-7435.78,-9833.89,0,458,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20211.6,-6159.78,-18269.2,0,300,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20076.2,-6042.78,-18400.2,0,300,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19984.6,-5887.78,-18553.4,0,300,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19988.1,-5682.78,-18638.3,0,343,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19993.7,-5554.78,-18411.4,0,343,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20166.5,-5554.78,-18270.4,0,284,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20360,-5447.78,-18174,0,308,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20567.4,-5358.78,-17988.2,0,308,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20603.4,-5358.78,-17772.4,0,368,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20383.8,-5269.78,-17618.1,0,414,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-20009.6,-5269.78,-17483.6,1,432,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19594.4,-5269.78,-17609.4,1,472,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19766.4,-5269.78,-17644.7,1,376,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19442.6,-5133.78,-17760.6,-1,438,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19234.3,-5041.78,-17855.8,-1,504,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19127.5,-4909.78,-17974.5,-1,510,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19327.9,-4727.78,-17827.2,-2,784,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19537.1,-4549.78,-17549,-2,784,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19386.3,-4421.78,-17736.1,-2,844,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19272.3,-4275.78,-18036.4,-2,1159,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-19264.2,-4057.78,-18239.6,-2,1159,0,false,"PC_LOB_BRIDGE2.3DS");
            addVob(-16404.7,-245.779,-5839.03,-2,1124,0,false,"OC_BARONSLEDGE_V1.3DS");
            addVob(-15534.4,-24.7793,-6312.94,-2,1177,0,false,"NW_MISC_DECO_INNOS_LARGE_01.3DS");
            addVob(-16849.8,-445.779,-5365.91,-2,1214,0,false,"NC_STAIRS_V01.3DS");
            addVob(-16185.7,-87.7793,-6069.52,-2,1306,0,false,"NC_PLANKS_V01.3DS");
            addVob(-16633,-87.7793,-5616.54,-2,1306,0,false,"NC_PLANKS_V01.3DS");
            addVob(-17228.7,-529.779,-5942.17,-2,1306,0,false,"NW_MISC_POOR_HUT_01.3DS");
            addVob(-16377,-584.779,-5291.05,-2,1221,0,false,"OC_LOB_FLAG_SMALL.3DS");
            addVob(-16912.4,-584.779,-5833.92,-2,1221,0,false,"OC_LOB_FLAG_SMALL.3DS");
            addVob(5352.5,294.287,-1582.56,25,-126,0,true,"NW_HARBOUR_CRATE_01.3DS");
            addVob(-13923.4,91.4974,-502.165,-5,-95,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
            addVob(-13219.6,73.4974,-186.2,-3,82,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
            addVob(-13995.3,73.4974,-943.672,-3,1,0,false,"ADDON_PLANTS_PALM_SHORT_BENT_02_170P.3DS");
            addVob(-14040.4,73.4974,-1116.07,-3,18,0,false,"ADDON_PLANTS_PALM_SHORT_BENT_02_170P.3DS");
            addVob(-13561.8,73.4974,792.394,-3,114,0,false,"ADDON_PLANTS_PALM_SHORT_BENT_02_170P.3DS");
            addVob(-13408,-555.503,233.346,-3,114,0,false,"OW_MISC_TENT_01.3DS");
            addVob(-13424.8,-47.5026,264.776,-3,12,0,false,"DT_SKELETON_V01_HEAD.3DS");
            addVob(-13324.8,-47.5026,264.776,-3,12,0,false,"DT_SKELETON_V01_HEAD.3DS");
            addVob(-19993.3,-556.503,2467.6,-3,-53,0,false,"OC_BARONSLEDGE_V1.3DS");
            addVob(-19672.6,-964.503,2305.19,9,-53,0,false,"NW_NATURE_STONE_CULT_PART_01.3DS");
            addVob(-19716.4,-874.503,2413.6,9,-53,0,false,"NW_NATURE_STONE_CULT_PART_01.3DS");
            addVob(-19910.7,-331.503,2773.86,9,-53,0,false,"ADDON_MISC_FLAGMAST_01.3DS");
            addVob(-19839.4,-207.503,2889.51,9,-50,0,false,"ADD_PIRATEFLAG.MMS");
            addVob(-19182.8,-700.503,3101.05,269,30,0,false,"NW_CITY_SHIP_01.3DS");
            addVob(-19660.5,-504.503,2572.11,269,30,0,false,"OW_FOCUS_V02.3DS");
            addVob(-19554.7,-586.503,2571.25,347,217,0,false,"NC_LOB_NICE2.3DS");
            addVob(-20523.5,-506.503,2586.19,347,317,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-26100.9,-690.503,14530.5,0,496,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25850.6,-690.503,14599.4,0,582,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25589.1,-690.503,14364.4,0,582,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25379.8,-690.503,13877,0,628,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26291.5,-690.503,14264.1,0,834,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26432.2,-690.503,13944.1,0,834,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26401.1,-690.503,13691.5,0,1132,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25954.8,-690.503,13326.5,0,1103,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25484.4,-690.503,13618,0,1216,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25730.4,-690.503,13380.2,0,1216,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25621.8,-899.503,14123.8,90,1300,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25904,-899.503,14329.3,90,1300,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26131.3,-899.503,14082.6,90,1287,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25938.4,-899.503,13538.9,90,1466,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25871.1,-901.503,13924.5,90,1489,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25600.4,-900.503,13797,90,1489,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25805.6,-900.503,13611.7,90,1489,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26259.5,-900.503,13772.1,90,1556,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-26100.6,-907.503,13642.3,90,1573,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25579.6,-905.503,13982.4,90,1618,0,false,"NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            addVob(-25951.2,-718.503,14146.6,7,1699,0,false,"OC_LOB_STAIRS_ROUND.3DS");
            addVob(-25676.3,-659.503,13549.9,7,1575,0,false,"NW_CITY_DECO_SHADOWHEAD_01.3DS");
            addVob(-25441.1,-604.503,13893.6,7,1528,0,false,"NW_CITY_DECO_SHIELD_01.3DS");
            addVob(-25446.7,-549.503,14163.9,1,1502,0,false,"NW_CITY_COUNTER_01.3DS");
            addVob(-26189,-544.503,13475.3,1,1659,0,false,"NW_CITY_COUNTER_01.3DS");
            addVob(-25894.6,-842.503,13445,-4,1618,0,false,"OC_WEAPON_SHELF_OLD_V1.3DS");
            addVob(-26348.5,-850.503,13781.3,-8,1687,0,false,"OC_WEAPON_SHELF_OLD_V1.3DS");
            addVob(-26267.5,-492.503,13890.5,-87,1915,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-25949.5,-492.503,13628.4,-87,1937,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-25695.3,-492.503,13491.8,-87,1937,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-25439.1,-492.503,13861,-87,1937,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-25846.9,-504.503,13834.9,-87,1937,0,false,"EVT_NC_MAINGATE01.3DS");
            addVob(-26202,-811.503,14746.3,-3,1949,0,false,"AW_CRATEPILE_01.3DS");
            addVob(-26344,-811.503,14667.3,-3,1981,0,false,"AW_CRATEPILE_01.3DS");
            addVob(-26544,-858.503,14284.7,-3,1998,0,false,"OC_SAECKE_01.3DS");
            addVob(-20645.6,-1261.5,18275,60,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-19912.6,-924.503,18650.7,78,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-19102,-821.503,19070.8,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-18293,-811.503,19486.8,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-17542.7,-798.503,19868.3,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-16741.7,-784.503,20273.8,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-15928.1,-775.503,20686.3,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-15082.8,-759.503,21112,90,1863,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-14315.4,-925.503,21549,67,240,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-13790.1,-1237.5,21994.7,67,214,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-13330.3,-1583.5,22649.1,67,214,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12869.6,-1959.5,23294.7,62,214,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12482.3,-2429.5,23844.5,50,214,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12234.9,-2669.5,24283.3,89,534,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12294.6,-2960.5,25067.6,54,534,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12373.7,-3525.5,25808.6,54,534,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-12444.7,-4083.5,26548.5,54,534,0,false,"TPL_EVT_SECRETWALL_01.3DS");
            addVob(-15802.2,-384.502,20540.1,0,516,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
            addVob(-15919.9,-413.502,20937.4,0,700,0,false,"ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS");
			break
	}
}

