
addEventHandler("onEquip", function(instance) {
	local itemId = Items.id(instance)
	if (itemId == -1)
		return

    ItemIntegration.packetEquipItem(itemId);
	PlayerItem.equipItemTable(itemId);
})
