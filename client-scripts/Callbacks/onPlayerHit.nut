addEventHandler("onPlayerHit", function (kid, pid, dmg) {
    if(kid == heroId)
        MakroDetector.GetData();

    if(Player.isObserver)
    {
        eventValue(0);
        cancelEvent();
        return;
    }

    Bot_onPlayerHit(kid, pid, dmg);
})