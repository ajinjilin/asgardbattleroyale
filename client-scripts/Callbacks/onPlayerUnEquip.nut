
addEventHandler("onUnequip", function(instance) {
	local itemId = Items.id(instance)
	if (itemId == -1)
		return

    ItemIntegration.packetUnEquipItem(itemId);
	PlayerItem.unEquipItemTable(itemId);
});
