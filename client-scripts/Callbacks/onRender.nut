
addEventHandler("onRender", function () {
    LandingGUI.onRender();
    DesktopGameGUI.onRender();
    Player.Skill.onRender();
    VobController.onRender();
    SmallNotes.onRender();
    EffectRegister.onRender();
    TimerTeleport.onRender();
});