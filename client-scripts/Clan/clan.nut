Clan <- {};
Clan.List <- {};

function Clan::register(id, name, r, g, b) {
    Clan.List[id] <- {
        name = name,
        color = { r = r, g = g, b = b},
        members = [],
        tag = makeTagFromName(name)
    }
}

function Clan::destroy(id) {
    Clan.List.rawdelete(id);
}

function Clan::close() {
    Clan.List.clear();
}

function getClanByName(nameClan) {
    foreach(idClan, _clan in Clan.List)
    {
        if(_clan.name == nameClan)
            return idClan;
    }
}

function makeTagFromName(name)
{
    local originalName = name;
    local string = "";
    name = split(name, " ");

    if(name.len() == 0)
        name = [originalName];

    foreach(_name in name)
        string = string + _name.slice(0, 1).toupper();

    return string;
}

local draws = [];

addEventHandler("onRender", function () {
    local clanId = getPlayerClan(heroId);
    draws.clear();

    if(clanId == -1)
        return;

    if(Player.GUI != false)
        return;

    local color = Clan.List[clanId].color;
    local tag = Clan.List[clanId].tag;
    local pos = getPlayerPosition(heroId);

    for(local i = 0; i <= getMaxSlots(); i++)
    {
        if(!isPlayerStreamed(i))
            continue;

        if(getPlayerClan(i) != clanId)
            continue;

        if(getPlayerFocus(heroId) == i)
            continue;

        local ipos = getPlayerPosition(i);
        if(getDistance3d(pos.x, pos.y, pos.z, ipos.x, ipos.y, ipos.z) <= 3000)
        {
            local draw = Draw3d(ipos.x,ipos.y + 109.2,ipos.z);
            draw.insertText("("+tag+") "+getPlayerName(i));
            draw.setColor(color.r, color.g, color.b);
            draw.visible = true;
            draw.distance = 3000;
            draw.top();
            draws.append(draw);
        }
    }
})