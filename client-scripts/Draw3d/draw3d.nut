local All = {};

class draw3d
{
    element = null;

    constructor(text, x, y, z, r, g, b)
    {
        element = Draw3d(x,y,z);
        element.visible = true;
        element.distance = 1000;
        element.top();
        element.setColor(r,g,b);
        element.insertText(text);
    }
}

function add3dDraw(id, text, x, y, z, r = 255, g = 255, b = 255) {
    All[id] <- draw3d(text, x, y, z, r, g, b);
}

function remove3dDraw(id) {
    All.rawdelete(id);
}