/*

	Script: area-manager.nut
	Side: Shared

	Author: Patrix

*/

local getDefaultWorld = null

if (!("ban" in getroottable()))
{ // Client-side 
	getDefaultWorld = getWorld
}
else
{ // Server-side
	getDefaultWorld = getServerWorld
}

class BaseArea
{
	static region = {}

	world = getDefaultWorld()

	polyX = null
	polyY = null
	polyZ = null

	isIn = null

	onEnter = null
	onExit = null
	
	constructor() {}

	function destroy() {}

	function isHeightBetweenMinMax(y)
	{
		if ( (type(polyY) == "null")
			|| (type(polyY) == "table" && (y >= polyY.min && y <= polyY.max)) )
			return true
			
		return false
	}
	
	function setIsIn() {}
	
	function checkIsIn(x, y, z)
	{
		local verticesCount = polyX.len()
		local j = verticesCount - 1
		
		local isIn = false

		if (isHeightBetweenMinMax(y))
		{
			for (local i = 0; i < verticesCount; j = i++)
			{
				if ( (polyZ[i] > z) != (polyZ[j] > z)
					&& (x < (polyX[j] - polyX[i]) * (z - polyZ[i]) / (polyZ[j] - polyZ[i]) + polyX[i]) )
				{
				   isIn = !isIn
				}
			}
		}
		
		return isIn
	}
	
	static function onCheck() {}
}

if (!("ban" in getroottable()))
{ // Client-side 
	BaseArea.constructor <- function(arg = null)
	{
		polyX = []
		polyZ = []
		
		if ("y" in arg && type(arg.y) == "table")
		{
			polyY = {}

			polyY.min <- arg.y.min
			polyY.max <- arg.y.max
		}

		if ("world" in arg && type(arg.world) == "string")
			world = arg.world.toupper()

		if (!(world in region))
			region[world] <- {}

		region[world][this] <- this
		
		isIn = false
	}
	
	BaseArea.destroy <- function()
	{
		if (isIn && type(onExit) != null)
			onExit()

		if (region[world].len() <= 0)
			delete region[world]
		else
			delete region[world][this]
			
		return null
	}

	BaseArea.setIsIn <- function(isIn)
	{	
		if (this.isIn == isIn)
			return
	
		if (isIn && !this.isIn)
		{
			if (type(onEnter) == "function")
				onEnter()
		}
		else if (!isIn && this.isIn)
		{
			if (type(onExit) == "function")
				onExit()
		}
		
		this.isIn = isIn
	}

	BaseArea.onCheck <- function()
	{
		local playerWorld = getWorld()

		if (playerWorld in region)
		{
			local playerPosition = getPlayerPosition(heroId)
		
			foreach (area in region[playerWorld])
				area.setIsIn(area.checkIsIn(playerPosition.x, playerPosition.y, playerPosition.z))
		}
	}
}
else
{ // Server-side
	BaseArea.constructor <- function(arg = null)
	{
		polyX = []
		polyZ = []
		
		if ("y" in arg && type(arg.y) == "table")
		{
			polyY = {}

			polyY.min <- arg.y.min
			polyY.max <- arg.y.max
		}

		if ("world" in arg && type(arg.world) == "string")
			world = arg.world.toupper()

		if (!(world in region))
			region[world] <- {}

		region[world][this] <- this
		
		isIn = array(getMaxSlots(), false)
	}
	
	BaseArea.destroy <- function()
	{
		if (isIn && type(onExit) != null)
		{
			for (local pid = 0, maxSlots = getMaxSlots(); pid < maxSlots; ++pid)
			{
				if (!isPlayerSpawned(pid))
					continue
				
				if (isIn[pid])
					onExit(pid)
			}
		}

		if (region[world].len() <= 0)
			delete region[world]
		else
			delete region[world][this]
			
		return null
	}

	BaseArea.setIsIn <- function(pid, isIn)
	{
		if (this.isIn[pid] == isIn)
			return
	
		if (isIn && !this.isIn[pid])
		{
			if (type(onEnter) == "function")
				onEnter(pid)
		}
		else if (!isIn && this.isIn[pid])
		{
			if (type(onExit) == "function")
				onExit(pid)
		}
		
		this.isIn[pid] = isIn
	}

	BaseArea.onCheck <- function()
	{
		for (local pid = 0, maxSlots = getMaxSlots(); pid < maxSlots; ++pid)
		{
			if (!isPlayerSpawned(pid))
				continue
		
			local playerWorld = getPlayerWorld(pid)

			if (playerWorld in region)
			{
				local playerPosition = getPlayerPosition(pid)
			
				foreach (area in region[playerWorld])
					area.setIsIn(pid, area.checkIsIn(playerPosition.x, playerPosition.y, playerPosition.z))
			}
		}
	}
	
	local function playerExitAreas(pid)
	{
		local playerWorld = getPlayerWorld(pid)

		if (playerWorld in BaseArea.region)
		{
			local playerPosition = getPlayerPosition(pid)
		
			foreach (area in BaseArea.region[playerWorld])
			{
				if (area.onExit && area.isIn[pid])
				{
					area.onExit(pid)
					area.isIn[pid] = false
				}
			}
		}
	}
	
	addEventHandler("onPlayerDisconnect", function(pid, reason)
	{
		playerExitAreas(pid)
	})
	
	local _unspawnPlayer = unspawnPlayer
	function unspawnPlayer(pid)
	{
		_unspawnPlayer(pid)
		playerExitAreas(pid)
	}
	
}

setTimer(function()
{
	BaseArea.onCheck()
}, 500, 0)

class Area extends BaseArea
{
	constructor(arg)
	{
		if (type(arg) == "table")
		{
			base.constructor(arg)

			if ("pos" in arg && type(arg.pos) == "array")
			{
				foreach(v in arg.pos)
				{
					polyX.push(v.x)
					polyZ.push(v.z)
				}
			}
		}
	}
}

class Circle extends BaseArea
{
	x = 0
	z = 0

	verticesCount = 0
	radius = 500

	constructor(arg)
	{
		if (type(arg) == "table"
			&& ("x" in arg && (type(arg.x) == "integer" || type(arg.x) == "float"))
			&& ("z" in arg && (type(arg.z) == "integer" || type(arg.z) == "float")))
		{
			base.constructor(arg)

			x = arg.x
			z = arg.z

			if ("radius" in arg && type(arg.radius) == "integer")
				radius = arg.radius

			if ("verticesCount" in arg && type(arg.verticesCount) == "integer")
				verticesCount = arg.verticesCount
		}

		local verticesCount = this.verticesCount
		this.verticesCount = 0
		
		setVerticesCount(verticesCount)
	}

	function checkIsIn(x, y, z)
	{
		if (verticesCount > 0)
			return base.checkIsIn(x, y, z)
		else
		{		
			local isIn = false
		
			if (isHeightBetweenMinMax(y))
			{
				if (getDistance2d(x, z, this.x, this.z) <= radius)
				{
					local distanceX = fabs(x - this.x)
					local distanceY = fabs(z - this.z)
				
					if ((distanceX + distanceY <= radius)
					|| (pow(distanceX, 2) + pow(distanceY, 2) <= pow(radius, 2)))
						isIn = true
				}
			}
			
			return isIn
		}
	}

	function setRadius(radius)
	{
		if (type(radius) == "integer")
		{
			for (local i = 0, angle = 0, distanceAngle = PI / (verticesCount / 2); 
					i < verticesCount; 
					++i, angle += distanceAngle)
			{
				polyX[i] = x + (cos(angle) * radius)
				polyZ[i] = z + (sin(angle) * radius)
			}

			this.radius = radius
		}
	}

	function setPosition(x,y,z)
	{
		if ((type(x) == "integer" || type(x) == "float")
			&& (type(z) == "integer" || type(z) == "float"))
		{
			local diffX = x - this.x
			local diffZ = z - this.z

			if (type(y) == "table"
				&& (type(y.min) == "integer" || type(y.min) == "float")
				&& (type(y.max) == "integer" || type(y.max) == "float"))
			{
				polyY.min = y.min
				polyY.max = y.max
			}

			for (local i = 0; i < verticesCount; i++)
			{
				polyX[i] = polyX[i] + diffX
				polyZ[i] = polyZ[i] + diffZ
			}

			this.x = x
			this.z = z
		}
	}

	function setVerticesCount(verticesCount)
	{
		if (type(verticesCount) == "integer" && verticesCount >= 0)
		{
			// if current verticesCount count is smaller than target count, increase it by adding new elements
			for (local i = this.verticesCount; i < verticesCount; ++i)
			{
				polyX.push(0)
				polyZ.push(0)
			}
				
			// if current verticesCount count is larger than target count, decrease it, by popping last elements
			for (local i = this.verticesCount - 1; i >= verticesCount; --i)
			{
				polyX.pop()
				polyZ.pop()
			}
			
			this.verticesCount = verticesCount

			for (local i = 0, angle = 0, distanceAngle = PI / (verticesCount / 2); 
				i < verticesCount; 
				++i, angle += distanceAngle)
			{
				polyX[i] = x + cos(angle) * radius
				polyZ[i] = z + sin(angle) * radius
			}
		}
	}
}
