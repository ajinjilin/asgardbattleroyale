
Game.Controller <- {
    "onEnter" : function (pid, statusGame, isAdmin) {
        Player.isAdmin = isAdmin;

        Game.status = statusGame;

        switch(statusGame)
        {
            case GameStatus.Lobby:
                StartGUI.hide(false);
                LobbyGUI.show();
            break;
            case GameStatus.Game:
                StartGUI.hide();
                DesktopGameGUI.show();
            break;
        }
    },
    "onExit" : function (pid) {

    },
    "startGame" : function (x,y,z, worldId) {
        Game.status = GameStatus.Game;

        LobbyGUI.hide();
        LandingGUI.show();

        Game.activeWorld = worldId;

        Game.activeCirclePosition = { x = x, y = y, z = z};
        Game.circle.setPosition(x,y,z);
    },
    "endGame": function () {
        clearInventory();

        Bot_onEndGame();
        VobController.clearVobs();
        clearListLobbyChat();
        StructureEndGame();

        Player.isObserver = false;

        if(Player.GUI != PlayerGUIEnum.Lobby) {
            HideActiveGUI();
            DesktopGameGUI.hideOut();

            StartGUI.hide(false);
            LobbyGUI.show();
        }
    },
    "tryStartGame" : function (worldId) {
        local packet = Packet();
        packet.writeUInt8(Packets.Game)
        packet.writeUInt8(GamePackets.StartGame);
        packet.writeInt16(worldId);
        packet.send(RELIABLE_ORDERED);
    },
    "setGameTimer" : function (val, playersInGame) {
        Game.timer = val;
        DesktopGameGUI.gameTimer();
        Observer.gameTimer();
        DesktopGameGUI.setPlayersInGame(playersInGame);
        Observer.setPlayersInGame(playersInGame);
        Game.Controller.circleMath();
    },
    "circleMath" : function () {
        if(Game.timer <= 1800)
        {
            local radiusGameTime = Game.timer - 100;
            if(radiusGameTime < 0)
                radiusGameTime = 0;

            local newRadius = abs(radiusGameTime * 60);
            if(newRadius < 10)
                newRadius = 1;

            local verticesCount = newRadius/100;

            if(verticesCount < 50)
                verticesCount = 50;

            if(Game.timer <= 360 && Game.timer > 200)
                Game.circle.polyY = {min = Game.activeCirclePosition.y - 1500, max = Game.activeCirclePosition.y + 1500}
            else if(Game.timer <= 200)
                Game.circle.polyY = {min = Game.activeCirclePosition.y - 400, max = Game.activeCirclePosition.y + 400}
            else
                Game.circle.polyY = null;
                
            Game.circle.setVerticesCount(verticesCount);
            Game.circle.setRadius(newRadius);
            VobController.mapCircle();
        }
    },
    "setMapStartPoint" : function (x,y,z) {
        Game.circle.setPosition(x,y,z);
    }
}
