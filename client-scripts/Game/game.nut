Game <- {};

Game.vobs <- [];
Game.bots <- [];

Game.timer <- 0;
Game.status <- GameStatus.Lobby;
Game.activeWorld <- Worlds.Jarkendar;

Game.activeCirclePosition <- {x = 0, y = 0, z = 0};
Game.circle <- Circle({x = -170, z = -5853, radius = 5000});

addEventHandler("onCommand", function (command, params) {
    if(command == "mapstate")
    {
        Chat.print("PolyX " +Game.circle.polyX.len());
        Chat.print("GameTimer " +Game.timer);
        Chat.print("Area " +Game.circle);
        Chat.print("Area Circle " +Game.circle.radius);
        Chat.print("Vertices count " +Game.circle.verticesCount);
        Chat.print("Radius " +Game.circle.radius);
        Chat.print("Position "+Game.circle.x + ", "+Game.circle.z);

        local radiusGameTime = Game.timer - 100;
        if(radiusGameTime < 0)
            radiusGameTime = 0;
            
        local newRadius = abs(radiusGameTime * 60);
        if(newRadius < 10)
            newRadius = 1;

        local verticesCount = newRadius/100;

        if(verticesCount < 50)
            verticesCount = 50;

        Game.circle.setVerticesCount(verticesCount);
        Game.circle.setRadius(newRadius);
        VobController.mapCircle();
    }
})