
function serverPrint(value) {
    local packet = Packet();
    packet.writeUInt8(Packets.Helpers);
    packet.writeUInt8(HelperPackets.Print);
    packet.writeString(value);
    packet.send(RELIABLE_ORDERED);
}
