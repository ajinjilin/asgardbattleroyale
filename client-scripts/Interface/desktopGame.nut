
DesktopGameGUI <- {};

local check = getTickCount();

DesktopGameGUI.textures <- [
    Texture(anx(10), any(Resolution.y - 120), anx(384), any(96), "BACKGROUND_DESKTOP_UI_ENDLESS.TGA"),
    Texture(anx(105), any(Resolution.y - 120 + 17), anx(260), any(36), "HEALTH_BAR_ENDLESS.TGA"),
    Texture(anx(Resolution.x/2 - 200), any(Resolution.y - 120), anx(84), any(84), "SLOT_ENDLESS.TGA"),
    Texture(anx(Resolution.x/2 - 120), any(Resolution.y - 120), anx(84), any(84), "SLOT_ENDLESS.TGA"),
    Texture(anx(Resolution.x/2 - 40), any(Resolution.y - 120), anx(84), any(84), "SLOT_ENDLESS.TGA"),
    Texture(anx(Resolution.x/2 + 40), any(Resolution.y - 120), anx(84), any(84), "SLOT_ENDLESS.TGA"),
    Texture(anx(Resolution.x/2 + 120), any(Resolution.y - 120), anx(84), any(84), "SLOT_ENDLESS.TGA"),
];

DesktopGameGUI.draws <- [
    Draw(anx(Resolution.x/2 - 190), any(Resolution.y - 50), "Z"),
    Draw(anx(Resolution.x/2 - 110), any(Resolution.y - 50), "X"),
    Draw(anx(Resolution.x/2 - 30), any(Resolution.y - 50), "C"),
    Draw(anx(Resolution.x/2 + 50), any(Resolution.y - 50), "V"),
    Draw(anx(Resolution.x/2 + 130), any(Resolution.y - 50), "B"),
];

DesktopGameGUI.slots <- [
    GUI.Button(anx(Resolution.x/2 - 190), any(Resolution.y - 110), anx(66), any(66), "SKILL_ARROWS.TGA", "0"),
    GUI.Button(anx(Resolution.x/2 - 110), any(Resolution.y - 110), anx(66), any(66), "SKILL_DEATHKISS.TGA", "0"),
    GUI.Button(anx(Resolution.x/2 - 30), any(Resolution.y - 110), anx(66), any(66), "SKILL_FIREBALL.TGA", "0"),
    GUI.Button(anx(Resolution.x/2 + 50), any(Resolution.y - 110), anx(66), any(66), "SKILL_SHIELD.TGA", "0"),
    GUI.Button(anx(Resolution.x/2 + 130), any(Resolution.y - 110), anx(66), any(66), "SKILL_SWORD.TGA", "0"),
];

DesktopGameGUI.hearts <- [
    Texture(anx(140), any(Resolution.y - 50), anx(44), any(44), "ICON_HEART_ENDLESS.TGA"),
    Texture(anx(180), any(Resolution.y - 50), anx(44), any(44), "ICON_HEART_ENDLESS.TGA"),
    Texture(anx(220), any(Resolution.y - 50), anx(44), any(44), "ICON_HEART_ENDLESS.TGA"),
];

local IconWinGames = Texture(anx(280), any(Resolution.y - 45), anx(33), any(33), "SHIELD_ENDLESS.TGA");
local AmountWinGames = Draw(anx(310), any(Resolution.y - 40), "");
local IconKills = Texture(anx(340), any(Resolution.y - 45), anx(33), any(33), "ICON_SWORDS_ENDLESS.TGA");
local AmountKills = Draw(anx(370), any(Resolution.y - 40), "");
local IconDeaths = Texture(anx(400), any(Resolution.y - 45), anx(33), any(33), "ICON_SKULL_ENDLESS.TGA");
local AmountDeaths = Draw(anx(430), any(Resolution.y - 40), "");
local IconBuildingPoints = Texture(anx(460), any(Resolution.y - 45), anx(33), any(33), "ICON_HAMMER_ENDLESS.TGA");
local AmountBuildingPoints = Draw(anx(490), any(Resolution.y - 40), "");

local TimerGame = Draw(anx(10), any(Resolution.y - 185), "");
TimerGame.font = "FONT_OLD_20_WHITE_HI.TGA";

local PlayersInGame = Draw(anx(10), any(Resolution.y - 150), "");
PlayersInGame.font = "FONT_OLD_10_WHITE_HI.TGA";

local choosenClan = Texture(anx(30),any(Resolution.y - 120),anx(90), any(90), "CLAN_SI_ENDLESS.TGA");

function DesktopGameGUI::show()
{
    foreach(text in DesktopGameGUI.textures)
    {
        text.visible = true;
        text.top();
    }

    IconWinGames.visible = true;
    TimerGame.visible = true;
    AmountWinGames.visible = true;
    IconKills.visible = true;
    AmountKills.visible = true;
    IconDeaths.visible = true;
    AmountDeaths.visible = true;
    IconBuildingPoints.visible = true;
    AmountBuildingPoints.visible = true;
    PlayersInGame.visible = true;

    DesktopGameGUI.hearts[0].visible = true;

    if(Player.hearts >= 2)
        DesktopGameGUI.hearts[1].visible = true;

    if(Player.hearts >= 3)
        DesktopGameGUI.hearts[2].visible = true;

    foreach(butt in DesktopGameGUI.slots)
        butt.setVisible(true);

    foreach(draw in DesktopGameGUI.draws)
    {
        draw.visible = true;
        draw.top();
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
    }

    choosenClan.visible = true;
    choosenClan.top();

    resetSkillTimeouts();
    Chat.setVisible(true);

    MessageGUI.showOut();
}

function DesktopGameGUI::showOut()
{
    foreach(text in DesktopGameGUI.textures)
        text.visible = true;

    DesktopGameGUI.hearts[1].visible = false;
    DesktopGameGUI.hearts[2].visible = false;
    DesktopGameGUI.hearts[0].visible = false;

    for(local i = 0; i < Player.hearts; i ++)
        DesktopGameGUI.hearts[i].visible = true;

    TimerGame.visible = true;
    IconWinGames.visible = true;
    AmountWinGames.visible = true;
    IconKills.visible = true;
    AmountKills.visible = true;
    IconDeaths.visible = true;
    AmountDeaths.visible = true;
    IconBuildingPoints.visible = true;
    AmountBuildingPoints.visible = true;
    PlayersInGame.visible = true;

    foreach(butt in DesktopGameGUI.slots)
        butt.setVisible(true);

    foreach(draw in DesktopGameGUI.draws)
    {
        draw.visible = true;
        draw.top();
    }

    choosenClan.visible = true;

    Chat.setVisible(true);

    MessageGUI.showOut();
}

function DesktopGameGUI::hideOut()
{
    foreach(text in DesktopGameGUI.textures)
        text.visible = false;

    TimerGame.visible = false;
    DesktopGameGUI.hearts[0].visible = false;
    DesktopGameGUI.hearts[1].visible = false;
    DesktopGameGUI.hearts[2].visible = false;

    IconWinGames.visible = false;
    AmountWinGames.visible = false;
    IconKills.visible = false;
    AmountKills.visible = false;
    IconDeaths.visible = false;
    AmountDeaths.visible = false;
    IconBuildingPoints.visible = false;
    AmountBuildingPoints.visible = false;
    PlayersInGame.visible = false;

    foreach(butt in DesktopGameGUI.slots)
        butt.setVisible(false);

    foreach(draw in DesktopGameGUI.draws)
    {
        draw.visible = false;
        draw.top();
    }

    choosenClan.visible = false;

    Chat.setVisible(false);

    MessageGUI.hideOut();
}

function DesktopGameGUI::setPlayersInGame(value)
{
    PlayersInGame.text = "Graczy: "+value;
}

function onPlayerGetAdditionalData() {
    AmountWinGames.text = Player.winGames;
    AmountKills.text = Player.kills;
    AmountDeaths.text = Player.deaths;
    choosenClan.file = "BOX_HOVER_ENDLESS"+Player.avatarId+".TGA";
}

function onPlayerGetBuildingPointsGUI()
{
    AmountBuildingPoints.text = Player.buildingPoints;
}

function onPlayerGetHeartsGUI() {
    if(!DesktopGameGUI.hearts[0].visible)
        return;

    DesktopGameGUI.hearts[1].visible = false;
    DesktopGameGUI.hearts[2].visible = false;
    DesktopGameGUI.hearts[0].visible = false;

    for(local i = 0; i < Player.hearts; i ++)
        DesktopGameGUI.hearts[i].visible = true;
}

function DesktopGameGUI::onRender()
{
    if(Player.GUI != false)
        return;

    if(check > getTickCount())
        return;

    local tick = getTickCount()
    local hpPercent = abs((getPlayerHealth(heroId) * 100)/getPlayerMaxHealth(heroId));

    DesktopGameGUI.textures[1].setRect(0,0,anx((260 * hpPercent)/100),any(36));
    DesktopGameGUI.textures[1].setSize(anx((260 * hpPercent)/100), any(36));

    if(!DesktopGameGUI.hearts[0].visible)
        return;

    foreach(index, skill in Player.Skill.timeouts)
    {
        if(skill > 0)
        {
            DesktopGameGUI.textures[index + 2].setColor(255,0,0);
            DesktopGameGUI.slots[index].setText(skill);
        }else
        {
            DesktopGameGUI.textures[index + 2].setColor(255,255,255);
            DesktopGameGUI.slots[index].setText("");
        }
    }

    check = getTickCount() + 200;
}

function DesktopGameGUI::gameTimer()
{
    local minutes = abs(Game.timer/60);
    local seconds = Game.timer - minutes*60;
    TimerGame.text = StringLib.zeroFormat(minutes) + ":"+StringLib.zeroFormat(seconds);
}