
Detector <- {};
Detector.clicked <- false;

Detector.button <- GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y - 300), anx(400), any(100), "BUTTON_ENDLESS.TGA", "Budowanie");

function Detector::show()
{
    Detector.button.setVisible(true);
    Detector.clicked = true;
    setCursorVisible(true);
    Camera.setFreeze(true);
    setFreeze(true);
    Player.GUI = PlayerGUIEnum.Detector;
}

function Detector::hide()
{
    Detector.button.setVisible(false);
    Detector.clicked = false;
    setCursorVisible(false);
    Camera.setFreeze(false);
    setFreeze(false);
    Player.GUI = false;
}

bindKey(KEY_LSHIFT, Detector.show);
bindKey(KEY_LSHIFT, Detector.hide, PlayerGUIEnum.Detector);


addEventHandler("GUI.onClick", function(self)
{
    switch (self)
	{
		case Detector.button:
            Detector.hide();
            BuildingGUI.show();
        break;
	}
})