
MenuGUI <- {};

MenuGUI.Menu <- GUI.Window(anx(50), any(Resolution.y/2 - 200), anx(400), any(560), "MAIN_ENDLESS.TGA");
MenuGUI.Back <- GUI.Button(anx(50),any(60),anx(300),any(80), "BUTTON_ENDLESS.TGA", "Wr��", MenuGUI.Menu);
MenuGUI.Clan <- GUI.Button(anx(50),any(140),anx(300),any(80), "BUTTON_ENDLESS.TGA", "Klan", MenuGUI.Menu);
MenuGUI.Rank <- GUI.Button(anx(50),any(220),anx(300),any(80), "BUTTON_ENDLESS.TGA", "Ranking", MenuGUI.Menu);
MenuGUI.Rule <- GUI.Button(anx(50),any(300),anx(300),any(80), "BUTTON_ENDLESS.TGA", "Zasady", MenuGUI.Menu);
MenuGUI.Exit <- GUI.Button(anx(50),any(380),anx(300),any(80), "BUTTON_ENDLESS.TGA", "Wyjd�", MenuGUI.Menu);

MenuGUI.Exit.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
MenuGUI.Clan.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
MenuGUI.Rank.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
MenuGUI.Rule.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
MenuGUI.Back.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");

function MenuGUI::show() {
    BaseGUI.show();

    DesktopGameGUI.hideOut()

    Player.GUI = PlayerGUIEnum.Menu;
    MenuGUI.Menu.setVisible(true);
}

function MenuGUI::hide() {
    BaseGUI.hide();

    DesktopGameGUI.showOut();

    Player.GUI = false;
    MenuGUI.Menu.setVisible(false);
}

bindKey(KEY_ESCAPE, MenuGUI.show)
bindKey(KEY_ESCAPE, MenuGUI.hide, PlayerGUIEnum.Menu)


addEventHandler("GUI.onClick", function(self)
{
	switch (self)
	{
        case MenuGUI.Back:
			MenuGUI.hide();
        break;
		case MenuGUI.Exit:
			exitGame();
        break;
        case MenuGUI.Rank:
			MenuGUI.hide();
            RankingGUI.show();
        break;
        case MenuGUI.Clan:
            MenuGUI.hide();
            ClanGUI.show();
        break;
        case MenuGUI.Rule:
			MenuGUI.hide();
            HelpMenu.show();
        break;
	}
})