
Observer <- {};

local draw = Draw(3000, 7000, "JESTE� OBSERWATOREM! (WYJ�CIE Z GRY - ESC)");
draw.setPosition(4098 - draw.width/2, 7000);
draw.setColor(250, 0, 0);

local TimerGame = Draw(anx(10), any(Resolution.y - 185), "");
TimerGame.font = "FONT_OLD_20_WHITE_HI.TGA";

local PlayersInGame = Draw(anx(10), any(Resolution.y - 150), "");
PlayersInGame.font = "FONT_OLD_10_WHITE_HI.TGA";

function Observer::show() {
    draw.visible = true;
    BaseGUI.show();
    DesktopGameGUI.hideOut()
    Camera.accessFly();
    Player.GUI = PlayerGUIEnum.Observer;
    TimerGame.visible = true;
    PlayersInGame.visible = true;
}

function Observer::hide() {
    draw.visible = false;
    BaseGUI.hide();
    DesktopGameGUI.showOut();
    Player.GUI = false;
    Camera.stopFly();
    TimerGame.visible = false;
    PlayersInGame.visible = false;
}

bindKey(KEY_ESCAPE, exitGame, PlayerGUIEnum.Observer)

function Observer::setPlayersInGame(value)
{
    PlayersInGame.text = "Graczy: "+value;
}

function Observer::gameTimer()
{
    local minutes = abs(Game.timer/60);
    local seconds = Game.timer - minutes*60;
    TimerGame.text = StringLib.zeroFormat(minutes) + ":"+StringLib.zeroFormat(seconds);
}