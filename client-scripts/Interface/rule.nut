local window = GUI.Window(anx(Resolution.x/2 - 306), any(Resolution.y/2 - 350), anx(612), any(720), "MAIN_ENDLESS.TGA", null, false);
local button = GUI.Button(-anx(30), -any(30), anx(672), any(90), "INPUT_ENDLESS.TGA", "Pomoc", window);
local scroll = GUI.ScrollBar(anx(540), any(50), anx(30), any(600), "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", Orientation.Vertical, window);
local textureBack = Texture(0, 0, 8200, 8200, "DLG_CONVERSATION.TGA");

HelpMenu <- {};
HelpMenu.List <- [];
HelpMenu.ActiveId <- -1;

function HelpMenu::show() {
    textureBack.visible = true;
    Player.GUI = PlayerGUIEnum.HelpMenu;

    BaseGUI.show();
    DesktopGameGUI.hideOut();

    window.setVisible(true)
    HelpMenu.change();
}

function HelpMenu::hide() {
    textureBack.visible = false;

    if(HelpMenu.ActiveId != -1)
    {
        HelpMenu.List[HelpMenu.ActiveId].hideTemplate();
        HelpMenu.ActiveId = -1;
        window.setVisible(true);
        textureBack.visible = true;
        HelpMenu.change();
        return;
    }

    BaseGUI.hide();

    DesktopGameGUI.showOut();

    Player.GUI = false;
    window.setVisible(false)
    HelpMenu.change();

    foreach(item in HelpMenu.List)
        item.hide();
}

function HelpMenu::change() {
    local scrollIndex = scroll.getValue();

    foreach(item in HelpMenu.List)
        item.hide();

    foreach(index, helpItem in HelpMenu.List)
    {
        if(index >= scrollIndex && index < scrollIndex + 8)
        {
            helpItem.show(index - scrollIndex);
        }
    }
}


addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.HelpMenu)
        return;

    foreach(indexItem, listItem in HelpMenu.List)
    {
        if(listItem.label == self)
        {
            HelpMenu.ActiveId = indexItem;
            listItem.showTemplate();
            window.setVisible(false);

            foreach(item in HelpMenu.List)
                item.hide();
        }
    }
});

addEventHandler("GUI.onChange", function (self) {
    if(Player.GUI != PlayerGUIEnum.HelpMenu)
        return;

    if( self == scroll )
        HelpMenu.change()
})

class HelpMenu.Item {
    name = "";
    description = "";

    bucket = null;
    label = null;

    constructor(name, description)
    {
        this.name = name;
        this.description = [];
        this.bucket = [];

        foreach(back in description)
        {
            this.description.append(back);
        }

        this.label = GUI.Button(0,0,anx(300), any(70), "INPUT_ENDLESS.TGA", name);

        HelpMenu.List.append(this);
    }

    function show(index)
    {
        label.setPosition(anx(Resolution.x/2 - 150), any(Resolution.y/2 - 300) + any(70 * index));
        label.setVisible(true);
    }

    function hide()
    {
        label.setVisible(false);
    }

    function showTemplate()
    {
        bucket.append(Draw(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 230), name));

        foreach(index, back in description)
        {
            bucket.append(Draw(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 200 + index * 25), back));
        }

        foreach(bucketItem in bucket)
            bucketItem.visible = true;
    }

    function hideTemplate()
    {
        bucket.clear();
    }
}

bindKey(KEY_H, HelpMenu.show)
bindKey(KEY_H, HelpMenu.hide, PlayerGUIEnum.HelpMenu)
bindKey(KEY_ESCAPE, HelpMenu.hide, PlayerGUIEnum.HelpMenu)

// help

HelpMenu.Item("Komendy chatu", [
    "Na serwerze mamy 2 typy akcji. Oparte na chacie og�lny",
    "dzia�aj�ce jako prefixy i komendy klaszyczne.",
    "Komendy dzia�aj�ce na serwerze.",
    "/pm /pw - wiadomo�� tekstowa / z pomini�ciem GUI.",
    "/pwMode - zmiana trybu wy�wietlania PW GUI/text.",
    "/checksum - sprawdza stan twoich umiejetnosci i ich cooldown.",
    "/account - podsumowanie twojego konta.",
    "/clans - lista klan�w. - zalecana forma GUI",
    "/members - lista cz�onk�w klanu. - zalecana forma GUI",
    "/join - pozwala na do��czenie do klanu. - zalecana forma GUI",
    "Prefixy:",
    "! - pozwala na napisanie wiadomo�ci na chacie globalnym.",
    "# - pozwala na napisanie wiadomo�ci do klanu.",
]);

HelpMenu.Item("Komendy animacji", [
    "Komendy animacji:",
    "/sikaj, /straznik1, /straznik2, /siadaj",
    "/dopinguj, /tancz1, /taczn2, /tancz3",
    "/porazenie, /kopniak, /myj, /trenuj",
]);

HelpMenu.Item("Zmiana wygl�du", [
    "Na serwerze tw�j wygl�d jest losowany",
    "niema mo�liwo�ci zmiany twojego wygl�du.",
    "Je�eli tw�j wygl�d si� zbugowa�, lub masz problem.",
    "To skontaktuj si� z nami na discord."
]);

HelpMenu.Item("Og�lne zasady gry", [
    "Przebieg serwera kontrolowany jest przez game controller.",
    "Game controller mo�e przybra� 4 statusy w kt�rych gra",
    "zmienia zachowania poszczeg�lnych element�w rozgrywki.",
    "1 - Faza lobby, czyli miejsce w kt�rym ka�dy gracz",
    "bez wzgl�du na uprawnienia g�osuje o rozpocz�ciu rozgrywki.",
    "aby rozpocz�� rozgrywk� potrzeba 75% graczy gotowych do gry.",
    "w przypadku kiedy jest na serwerze miej ni� 4 ludzi serwer nie wystartuje.",
    "podczas tej fazy gracz mo�e rozmawia� na chacie.",
    "2 - Faza gry pocz�tkowej trwa 30 minut.",
    "Tzw. faza spokojna w kt�rej gracze mog� zbiera� ekwipunek.",
    "Zgini�cie w tej fazie nie powoduje utraty EQ i serca.",
    "3 - Faza ostra gra (do ko�ca). W tej fazie b�dzie losowany endpoint.",
    "Czyli punkt na mapie wok� kt�rego po 30 minucie pojawi si� okr�g. ",
    "Okr�g b�dzie si� zmniejsza� a� po godzinie od rozpocz�cia fazy zostawi tylko",
    "skrawek ziemii a� do kompletnego zamkni�cia kr�gu.",
    "Zgini�cie w tej fazie spowoduje upuszczenie ekwipunku i utrata hp.",
    "4 - Faza opcjonalna dla graczy, kt�rzy utracili 3 �ycia.",
    "Jest to oczekiwanie w lobby na sko�czenie rozgrywki z opcj� chatu.",
    "Na chat b�d� wp�ywa� informacje o przebiegu gry.",
]);

HelpMenu.Item("Wyposa�enie", [
    "Wyposa�enie gracza sk�ada si� z broni wszelkiego rodzaju.",
    "Mo�emy posiada� bronie 1h, 2h, �uki czy kusze. ",
    "Na serwerze wyst�puje tak�e magia.",
    "Jest kilka opcji zdobycia ekwipunku.",
    "Ekwipunek mo�na zdoby� od gracza, kt�ry nie�yje.",
    "Ekwipunek po prostu wyleci z niego, je�eli jeste�my ",
    "w 3 fazie rozgrywki. Kolejn� opcj� zdobycia ekwipunku",
    "jest zdobycie go z skrzynek rozrzuconych po ca�ej mapie.",
    "Zabicie moba tak�e daje nam spor� ilo�� wyposa�enia.",
    "Musisz pami�ta�, �e twoje wyposa�enie przek�ada sie na obra�enia",
    "ale nie w takim stopniu jak w Gothic 2. Przepisali�my system DMG",
    "tak aby bronie nie r�ni�y si� od siebie tak znacznie."
]);

HelpMenu.Item("Rozw�j", [
    "Rozw�j odbywa si� g��wnie na zasadzie",
    "zdobycia lepszego wyposa�enia (opisane w wyposa�enie)",
    "i na dostaniu si� do klanu. Je�eli bedziemy mie�",
    "siln� grup�, �atwiej osi�gniemy swoje cele.",
    "Najwa�niejszym czynnikiem twojego zwyci�stwa to spryt",
    "i ekwipunek.",
]);

HelpMenu.Item("Klany", [
    "Klany na serwerze istniej� jako zrzeszenie graczy.",
    "Nie ma tutaj tzw. friendly fire, ale widzimy nicki",
    "naszych sojusznik�w na mapie og�lnej.",
    "Kolor twojego nicku tak�e b�dzie od tego zale�ny.",
    "W ostatniej fazie rozgrywki tw�j klan tak�e"
    "b�dzie twoim wrogiem. Tylko jedna osoba mo�e przetrwa�",
    "gr�.",
]);

HelpMenu.Item("Budowanie", [
    "Budowanie na serwerze ogranicza sie do stawiania ",
    "prostych budowli, kt�re b�d� dzia�a� na zasadzie",
    "przeszk�d tereonowych. Postawienie ich w odpowiednim",
    "momencie mo�e uratowa� �ycie. Zniszczenie budowli innego gracza",
    "to zaledwie kilka uderze� atakiem podstawowym w budowle.",
    "�ycie danego voba wida� po podej�ciu do niego bli�ej.",
    "Aby budowa� wystarczy wej�� w tryb budowy przyciskiem P",
    "Wej�cie w ten tryb pozwala wybra� voba i postawi� go enterem.",
    "Aby zbudowa� obiekt potrzeba danych dla niego punkt�w vob�w.",
    "Punkty te na start wynosz� 10, a po zabiciu przeciwnika rosn� o 5.",
]);

HelpMenu.Item("Statystyki", [
    "Jako, �e jest to tryb battle royal nie mamy wypracowanych",
    "skomplikowanych statystyk u�ytkownika.",
    "Domy�lnie zawsze walczymy z 100% umiejetno�ciami trafienia.",
    "Poza �ukiem, kusz� - 500% (RNG).",
    "Statystyki jakie tu maj� znaczenie podczas jednej rozgrywki.",
    "To g��wnie statystyki twojej broni i ilo�� �ycia.",
    "U�ycie umiej�tno�ci chwilowo zwi�ksza statystyki.",
    "Pod I mo�emy zobaczy� statystyki i nasze wyposa�enie.",
]);

HelpMenu.Item("Konto", [
    "Konto zapisuje dane elementy gracza: ",
    "- sta�y wygl�d przypisany losowo do gracza.",
    "- pozycj� i hp na dan� gr�.",
    "- nasze statystyki osobiste. (ilo�� zwyciestw, zabojstw, smierci).",
]);

HelpMenu.Item("Walka", [
    "Walka na serwerze ma charakter bardziej dynamiczny",
    "ni� w domy�lnych walkach DM dzi�ki umiej�tno�ciom specjalnym.",
    "A tak�e dzi�ki mountom (kt�re pozwol� si� szybciej przemie�ci�).",
    "Celem ko�cowym rozgrywki jest zdobycie punktu, kt�ry b�dzie",
    "ko�cowym kr�gem gry. Jezeli uda nam si� zdoby� ten punkt",
    "i zosta� ostatnim �ywym graczem to wygramy gr�.",
]);