
StartGUI <- {};

StartGUI.Background <- Texture(0,0,8200,8200, "BACKGROUND_ENDLESS.TGA");
StartGUI.LogoGlow <- GUI.Button(3000,200,2029,1529, "GLOW_ENDLESS.TGA", "BATTLE ROYALE");

StartGUI.LogoGlow.draw.setFont("METAMORPHOUS_25.TGA");

function StartGUI::show() {
    LobbyGUI.Background.visible = true;
    LobbyGUI.LogoGlow.setVisible(true);

    BaseGUI.show();

    Player.GUI = PlayerGUIEnum.Start;
}

function StartGUI::hide(hideBase = true) {
    LobbyGUI.Background.visible = false;
    LobbyGUI.LogoGlow.setVisible(false);

    if(hideBase == true)
        BaseGUI.hide();
        
    Player.GUI = false;
}

addEventHandler("onKey", function (key) {
    if (chatInputIsOpen())
		return
		
	if (isConsoleOpen())
		return

    if(key == KEY_P)
    {
        local pos = Packet();
        pos.writeUInt8(41);
        pos.send(RELIABLE_ORDERED);
    }
})