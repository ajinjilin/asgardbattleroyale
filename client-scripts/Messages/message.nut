
class Message
{
    message = null;
    typeId = null;
    data = null;

    constructor(message, typeId, data)
    {
        this.message = message;
        this.typeId = typeId;
        this.data = data;
    }
}