
RankingGUI <- {};

RankingGUI.window <- GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 380), anx(800), any(800), "MAIN_ENDLESS.TGA", null, false);
RankingGUI.inners <- [];

RankingGUI.ModeOnline <- GUI.Button(anx(100),any(-50),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Online", RankingGUI.window);
RankingGUI.ModeRanking <- GUI.Button(anx(450),any(-50),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Ranking", RankingGUI.window);

RankingGUI.LeftNav <- GUI.Button(anx(100),any(820),anx(40),any(30), "BUTTON_ENDLESS.TGA", "<", RankingGUI.window);
RankingGUI.RightNav <- GUI.Button(anx(650),any(820),anx(40),any(30), "BUTTON_ENDLESS.TGA", ">", RankingGUI.window);

local texturesToClick = [];
local MODE_ONLINE = 1, MODE_RANKING = 2;

local mode = MODE_ONLINE;
local pagination = 0;

function RankingGUI::show() {
    BaseGUI.show();
    DesktopGameGUI.hideOut();

    getOnlinePlayersFromServer();
    RankingGUI.window.setVisible(true);

    pagination = 0;
    Player.GUI = PlayerGUIEnum.Ranking;
}

function RankingGUI::hide() {
    BaseGUI.hide();
    DesktopGameGUI.showOut();

    textureToClickClear();

    RankingGUI.inners.clear();
    RankingGUI.window.setVisible(false);

    Player.GUI = false;
}

function textureToClickClear() {
    foreach(text in texturesToClick)
        text.button.destroy();

    texturesToClick.clear();
}

bindKey(KEY_F5, RankingGUI.show)
bindKey(KEY_F5, RankingGUI.hide, PlayerGUIEnum.Ranking)

addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.Ranking)
        return;

	switch (self)
	{
		case RankingGUI.ModeOnline:
            textureToClickClear();
            RankingGUI.inners.clear();
			getOnlinePlayersFromServer();
            mode = MODE_ONLINE;
        break;
        case RankingGUI.ModeRanking:
            textureToClickClear();
            RankingGUI.inners.clear();
            getRankingFromServer();
            mode = MODE_RANKING;
        break;
        case RankingGUI.LeftNav:
            RankingGUI.inners.clear();
            textureToClickClear();
            pagination -= 1;

            if(pagination <= 0)
                pagination = 0;

            if(mode == MODE_RANKING)
                getRankingFromServer(pagination);
            else
                getOnlinePlayersFromServer(pagination);
        break;
        case RankingGUI.RightNav:
            textureToClickClear();
            RankingGUI.inners.clear();
            pagination += 1;
            if(mode == MODE_RANKING)
                getRankingFromServer(pagination);
            else
                getOnlinePlayersFromServer(pagination);
        break;
	}

    foreach(item in texturesToClick)
        if(item.button == self)
            if(Messages.rawin(item.id))
                Messages[item.id].maximizeAndOpen();
            else
                MessageStorage(item.id);
})

function RankingGUI::addRanking(name, avatarId, winGames, kills, deaths, points, add = true)
{
    if(add)
        addToCachedRanking(pagination, name, avatarId, winGames, kills, deaths, points)

    local inner = RankingGUI.Inner();
    inner.addAvatar(avatarId);
    inner.addName(name);
    inner.addWinGames(winGames);
    inner.addKills(kills);
    inner.addDeaths(deaths);
    inner.addPoints(points);
    inner.show();
}

function RankingGUI::addOnline(id, name, inGame, avatarId, winGames, kills, deaths, points, add = true)
{
    if(add)
        addToOnlineCachedPlayers(pagination, id, name, inGame, avatarId, winGames, kills, deaths, points)

    local inner = RankingGUI.Inner();
    inner.addAvatar(avatarId);
    inner.addId(id);
    inner.addName(name);
    inner.addInGame(inGame);
    inner.addWinGames(winGames);
    inner.addKills(kills);
    inner.addDeaths(deaths);
    inner.addPoints(points);
    inner.addMessage(id);
    inner.show();
}

class RankingGUI.Inner
{
    background = null;
    draws = null;
    textures = null;
    offset = 0;
    startPos = null;

    constructor()
    {
        offset = RankingGUI.inners.len();

        textures = [];
        draws = [];
        background = Texture(anx(Resolution.x/2 - 350), any(Resolution.y/2 - 350 + offset * 72), anx(700), any(70), "INPUT_ENDLESS.TGA");
        startPos = {x = anx(Resolution.x/2 - 350), y = any(Resolution.y/2 - 350 + offset * 72)};

        RankingGUI.inners.append(this);
    }

    function show() {
        background.visible = true;

        foreach(texture in textures)
            texture.visible = true;

        foreach(draw in draws){
            draw.font = "FONT_OLD_10_WHITE_HI.TGA";
            draw.visible = true;
        }
    }

    function addAvatar(data) {
        local newTexture = Texture(startPos.x, startPos.y, anx(70), any(70), "BOX_HOVER_ENDLESS"+data+".TGA");
        startPos.x = startPos.x + anx(80);
        textures.append(newTexture);
    }

    function addId(data) {
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(30);

        draws.append(newDraw);
    }

    function addInGame(data) {
        foreach(draw in draws)
            if(data)
                draw.setColor(0, 200, 0);
            else
                draw.setColor(200, 0, 0);
    }

    function addName(data) {
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(250);

        draws.append(newDraw);
    }

    function addWinGames(data) {
        local newTexture = Texture(startPos.x, startPos.y + any(20), anx(30), any(30), "SHIELD_ENDLESS.TGA");
        startPos.x = startPos.x + anx(35);
        textures.append(newTexture);
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(30);

        draws.append(newDraw);
    }

    function addKills(data) {
        local newTexture = Texture(startPos.x, startPos.y + any(20), anx(30), any(30), "ICON_SWORDS_ENDLESS.TGA");
        startPos.x = startPos.x + anx(35);
        textures.append(newTexture);
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(30);

        draws.append(newDraw);
    }

    function addDeaths(data) {
        local newTexture = Texture(startPos.x, startPos.y + any(20), anx(30), any(30), "ICON_SKULL_ENDLESS.TGA");
        startPos.x = startPos.x + anx(35);
        textures.append(newTexture);
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(30);

        draws.append(newDraw);
    }

    function addPoints(data) {
        local newTexture = Texture(startPos.x, startPos.y + any(20), anx(30), any(30), "ICON_STAR_ENDLESS.TGA");
        startPos.x = startPos.x + anx(35);
        textures.append(newTexture);
        local newDraw = Draw(startPos.x, startPos.y + any(25), data);
        startPos.x = startPos.x + anx(50);

        draws.append(newDraw);
    }

    function addMessage(id) {
        texturesToClick.append({id = id, button = GUI.Texture(startPos.x, startPos.y + any(20), anx(30), any(30), "ICON_CHAR_ENDLESS.TGA")});
        startPos.x = startPos.x + anx(35);
        texturesToClick[texturesToClick.len()-1].button.setVisible(true);
    }
}
