local cachedPlayers = {}, cachedRanking = {};

setTimer(function() {
    cachedPlayers.clear();
    cachedRanking.clear();
}, 5000, 0)

function getOnlinePlayersFromServer(pagination = 0) {
    if(pagination in cachedPlayers)
    {
        foreach(player in cachedPlayers[pagination])
        {
            RankingGUI.addOnline(player.id,
            player.name,
            player.inGame,
            player.avatarId,
            player.winGames,
            player.kills,
            player.deaths,
            player.points, false);
        }
        return;
    }

    local packet = Packet();
    packet.writeUInt8(Packets.Ranking);
    packet.writeUInt8(RankingPackets.SendOnline);
    packet.writeInt16(pagination);
    packet.send(RELIABLE_ORDERED);
}

function getRankingFromServer(pagination = 0) {
    if(pagination in cachedRanking)
    {
        foreach(player in cachedRanking[pagination])
        {
            RankingGUI.addRanking(
            player.name,
            player.avatarId,
            player.winGames,
            player.kills,
            player.deaths,
            player.points, false);
        }

        return;
    }

    local packet = Packet();
    packet.writeUInt8(Packets.Ranking);
    packet.writeUInt8(RankingPackets.SendRanking);
    packet.writeInt16(pagination);
    packet.send(RELIABLE_ORDERED);
}

function getNextPaginationToGet(ids) {
    return getNextPaginationToGet;
}

function addToOnlineCachedPlayers(pagination, id, name, inGame, avatarId, winGames, kills, deaths, points)
{
    if(!(pagination in cachedPlayers))
        cachedPlayers[pagination] <- [];

    cachedPlayers[pagination].append({ id = id, name = name, inGame = inGame, avatarId = avatarId, winGames = winGames, kills = kills, deaths = deaths, points = points})
}


function addToCachedRanking(pagination, name, avatarId, winGames, kills, deaths, points)
{
    if(!(pagination in cachedRanking))
        cachedRanking[pagination] <- [];

    cachedRanking[pagination].append({name = name, avatarId = avatarId, winGames = winGames, kills = kills, deaths = deaths, points = points})
}