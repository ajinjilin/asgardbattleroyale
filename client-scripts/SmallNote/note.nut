
local check = getTickCount();

class SmallNotes
{
    All = [];

    function onRender()
    {
        if(check > getTickCount())
            return;

        check = getTickCount() + 200;

        if(notesDisabled == true)
            return;

        foreach(index, item in SmallNotes.All)
            if(item.onRender())
                SmallNotes.All.remove(index);
    }
}

class SmallNote
{
    draw = null;
    texture = null;
    left = null;
    typId = -1;
    follow = -1;

    constructor(_typeid, value, pid = -1)
    {
        if(notesDisabled == true)
            return;

        typId = _typeid;
        follow = -1;

        switch(typId)
        {
            case NoteType.Damage:
                local pos = getPlayerPosition(pid);
                if(pos == null) {
                    pid = getBot(pid).element;
                    pos = getPlayerPosition(pid);
                }if(pos.x == 0) {
                    pid = getBot(pid).element;
                    pos = getPlayerPosition(pid);
                }

                draw = Draw3d(pos.x,pos.y + 50,pos.z);
                draw.insertText(value);
                draw.setColor(23, 165, 54);

                if(pid == heroId)
                    draw.setColor(200,0,0);

                draw.visible = true;
                draw.distance = 2000;
                draw.top();

                left = 10;
            break;
            case NoteType.Information:
                local pos = getPlayerPosition(heroId);

                draw = Draw3d(pos.x,pos.y + 110,pos.z);
                draw.insertText(value);
                draw.setColor(18, 114, 143);
                
                draw.visible = true;
                draw.distance = 2000;
                draw.top();

                left = 50;
            break;
            case NoteType.Kill:
                local arg = split(value, "|");
                local testDraw = Draw(0,0,arg[0]);
                draw = Draw(50, 3510, arg[0]+"   "+arg[1]);
                texture = Texture(50 + testDraw.width + 10, 3500, anx(32), any(32), "ICON_SWORDS_ENDLESS.TGA");
                draw.visible = true;
                texture.visible = true;

                left = 20;
            break;
            case NoteType.Skill:
                local pos = getPlayerPosition(pid);

                draw = Draw3d(pos.x + rand() % 200,pos.y + rand() % 200,pos.z + rand() % 200);
                draw.insertText(value);
                draw.setColor(252, 165, 3);
                
                draw.visible = true;
                draw.distance = 2000;
                draw.top();

                left = 15;
            break;
        }

        SmallNotes.All.append(this);
    }

    function onRender()
    {
        if(left <= 0)
            return true;

        left = left - 1;

        switch(typId)
        {
            case NoteType.Kill:
                local pos = draw.getPosition();
                draw.setPosition(pos.x, pos.y + 30);
                pos = texture.getPosition();
                texture.setPosition(pos.x, pos.y + 30);
            break;
        }
    }
}
