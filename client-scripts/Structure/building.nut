
BuildingGUI <- {};

BuildingGUI.currentVob <- null;
BuildingGUI.currentVobId <- 1;

local infoDraw = Draw(100, 4000, "Budowanie: Q-lewo/E-prawo, Enter - Rozstaw, ESC - wyjd�.");
local infoDraw2 = Draw(100, 4200, "Koszt budowli 1 - 1 pkt, 2 - 2pkt, 3 - 3 pkt., 4,5 - 5 pkt. 6 - 4 pkt. 7 - 7 pkt.");

function BuildingGUI::show() {
    BaseGUI.show();

    infoDraw.visible = true;
    infoDraw2.visible = true;

    DesktopGameGUI.hideOut()

    setFreeze(false);
    Camera.setFreeze(false);

    Player.GUI = PlayerGUIEnum.Building;
    BuildingGUI.currentVobId = 1;
    BuildingGUI.currentVob = Vob("NW_HARBOUR_CRATE_01.3DS");
    BuildingGUI.currentVob.addToWorld()

    local position = getPlayerPosition(heroId);
    local angle = getPlayerAngle(heroId);

    position.x = position.x + (sin(angle * 3.14 / 180.0) * 50);
    position.z = position.z + (cos(angle * 3.14 / 180.0) * 50);


    BuildingGUI.currentVob.setPosition(position.x, position.y, position.z);
    BuildingGUI.currentVob.setRotation(0, angle, 0);
}

function BuildingGUI::hide() {
    infoDraw.visible = false;
    infoDraw2.visible = false;

    BuildingGUI.currentVob = null;

    BaseGUI.hide();

    DesktopGameGUI.showOut();

    Player.GUI = false;
}

function BuildingGUI::left() {
    BuildingGUI.currentVobId = BuildingGUI.currentVobId - 1;
    if(BuildingGUI.currentVobId <= 1)
        BuildingGUI.currentVobId = 1;

    switch(BuildingGUI.currentVobId)
    {
        case 1:
            BuildingGUI.currentVob = Vob("NW_HARBOUR_CRATE_01.3DS");
        break;
        case 2:
            BuildingGUI.currentVob = Vob("NW_NATURE_BUSH_25P.3DS");
        break;
        case 3:
            BuildingGUI.currentVob = Vob("EVT_NC_MAINGATE01.3DS");
        break;
        case 4:
            BuildingGUI.currentVob = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
        break;
        case 5:
            BuildingGUI.currentVob = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
        break;
        case 6:
            BuildingGUI.currentVob = Vob("NC_STAIRS_V01.3DS");
        break;
        case 7:
            BuildingGUI.currentVob = Vob("EVT_GATE_SMALL_01.3DS");
        break;
    }
    BuildingGUI.currentVob.addToWorld()
}

function BuildingGUI::right() {
    BuildingGUI.currentVobId = BuildingGUI.currentVobId + 1;
    if(BuildingGUI.currentVobId >= 7)
        BuildingGUI.currentVobId = 7;

    switch(BuildingGUI.currentVobId)
    {
        case 1:
            BuildingGUI.currentVob = Vob("NW_HARBOUR_CRATE_01.3DS");
        break;
        case 2:
            BuildingGUI.currentVob = Vob("NW_NATURE_BUSH_25P.3DS");
        break;
        case 3:
            BuildingGUI.currentVob = Vob("EVT_NC_MAINGATE01.3DS");
        break;
        case 4:
            BuildingGUI.currentVob = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
        break;
        case 5:
            BuildingGUI.currentVob = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
        break;
        case 6:
            BuildingGUI.currentVob = Vob("NC_STAIRS_V01.3DS");
        break;
        case 7:
            BuildingGUI.currentVob = Vob("EVT_GATE_SMALL_01.3DS");
        break;
    }

    BuildingGUI.currentVob.addToWorld()
}

function BuildingGUI::place() {
    local position = BuildingGUI.currentVob.getPosition();

    local packet = Packet();
    packet.writeUInt8(Packets.Structure);
    packet.writeUInt8(StructurePackets.Add);
    packet.writeInt8(BuildingGUI.currentVobId);
    packet.writeFloat(position.x);
    packet.writeFloat(position.y);
    packet.writeFloat(position.z);
    packet.writeInt16(getPlayerAngle(heroId));
    packet.send(RELIABLE_ORDERED);
}

bindKey(KEY_Q, BuildingGUI.left, PlayerGUIEnum.Building)
bindKey(KEY_E, BuildingGUI.right, PlayerGUIEnum.Building)
bindKey(KEY_RETURN, BuildingGUI.place, PlayerGUIEnum.Building)
bindKey(KEY_ESCAPE, BuildingGUI.hide, PlayerGUIEnum.Building)

addEventHandler("onRender", function () {
    if(BuildingGUI.currentVob == null)
        return;

    local position = getPlayerPosition(heroId);
    local angle = getPlayerAngle(heroId);

    position.x = position.x + (sin(angle * 3.14 / 180.0) * 200);
    position.z = position.z + (cos(angle * 3.14 / 180.0) * 200);

    switch(BuildingGUI.currentVobId)
    {
        case 2:
            position.y = position.y - 120;
        break;
        case 3:
            position.y = position.y + 30;
        break;
        case 7: case 4:
            position.y = position.y + 50;
        break;
        case 5:
            position.y = position.y + 230;
        break;
    }

    if(BuildingGUI.currentVobId == 5)
        BuildingGUI.currentVob.setRotation(90, angle, 0);
    else
        BuildingGUI.currentVob.setRotation(0, angle, 0);

    BuildingGUI.currentVob.setPosition(position.x, position.y, position.z);
})