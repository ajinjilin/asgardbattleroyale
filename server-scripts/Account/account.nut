local LoadedAccounts = {};

function getAllAccounts() {
    return LoadedAccounts;
}

function loadAccount(pid)
{
    if(!(getPlayerSerial(pid) in LoadedAccounts))
        return null;

    local obj = LoadedAccounts[getPlayerSerial(pid)];
    local pObj = getPlayer(pid);
    pObj.avatarId = obj.avatarId;
    pObj.deaths = obj.deaths;
    pObj.kills = obj.kills;
    pObj.winGames = obj.winGames;
    return obj;
}

function countPointsBySerial(serial)
{
    if(!(serial in LoadedAccounts))
        return null;    

    local obj = LoadedAccounts[serial];

    local _deaths = obj.deaths;
    local _kills = obj.kills * 2;
    local _winGames = obj.winGames * 10;

    local _points = _kills + _winGames - _deaths;
    if(_points < 0)
        _points = 0;

    return _points;
}

function saveAccount(pid)
{
    local obj = getPlayer(pid);
    if(!(getPlayerSerial(pid) in LoadedAccounts))
    {
        LoadedAccounts[getPlayerSerial(pid)] <- {
            avatarId = rand() % 30,
            deaths = obj.deaths,
            kills = obj.kills,
            winGames = obj.winGames,
            name = getPlayerName(pid)
        }
        Account.save();
        return;
    }

    local pObj = LoadedAccounts[getPlayerSerial(pid)];
    pObj.avatarId = obj.avatarId;
    pObj.deaths = obj.deaths;
    pObj.kills = obj.kills;
    pObj.winGames = obj.winGames;
    pObj.name = getPlayerName(pid);

    Account.save();
}

function CountPoints(obj) {
    local deaths = obj.deaths;
    local kills = obj.kills * 2;
    local winGames = obj.winGames * 10;

    local points = kills + winGames - deaths;
    if(points < 0)
        points = 0;

    return points;
}

class Account
{
    function load()
    {
        local myfile = io.file("accounts.txt", "r");
        if (myfile.isOpen)
        {
            LoadedAccounts = JSON.decode(myfile.read(io_type.ALL));
            myfile.close();
        }
        else
            print(myfile.errorMsg);
    }

    function save()
    {
        local myfile = io.file("accounts.txt", "w");
        if (myfile.isOpen)
        {
            myfile.write(JSON.encode(LoadedAccounts));
            myfile.close();
        }
        else
            print(myfile.errorMsg)
    }
}

