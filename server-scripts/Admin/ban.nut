
local loadedBans = [];

function giveBan(pid, minutes, reason)
{
    local timeout = 0;
    if(minutes != 0)
        timeout = time() + ( minutes * 60);

    BanList.Add(getPlayerName(pid), getPlayerMacAddr(pid), getPlayerSerial(pid), reason, timeout);
    kick(pid, reason);
}

class BanList
{
    function load()
    {
        local myfile = io.file("bans.txt", "r");
        if (myfile.isOpen)
        {
            loadedBans = JSON.decode(myfile.read(io_type.ALL));
            myfile.close();
        }
        else
            print(myfile.errorMsg);
    }

    function save()
    {
        local myfile = io.file("bans.txt", "w");
        if (myfile.isOpen)
        {
            myfile.write(JSON.encode(loadedBans));
            myfile.close();
        }
        else
            print(myfile.errorMsg)
    }

    function Add(name, mac, serial, reason, expiration)
    {
        loadedBans.append({ name = name, mac = mac, serial = serial, reason = reason, expiration = expiration });
        BanList.save();
    }

    function RemoveByName(name)
    {
        foreach(c, b in loadedBans)
        {
            if(b.name == name)
                loadedBans.remove(c);
        }
    }

    function isPersonBanned(obj, pid)
    {
        if(obj.expiration != 0 && obj.expiration < time())
            return false;

        if(obj.mac == getPlayerMacAddr(pid) || obj.serial == getPlayerSerial(pid))
            return true;

        return false;
    }

    function onPlayerJoin(pid)
    {
        foreach(ban in loadedBans)
        {
            if(BanList.isPersonBanned(ban, pid))
                kick(pid, ban.reason);
        }
    }
}


