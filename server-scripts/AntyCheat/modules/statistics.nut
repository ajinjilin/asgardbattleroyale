class StatisticsDetector extends AntyCheatModule
{
    function Write(value)
    {
        local playerId = id;
        local obj = {player = playerId, original = originalValue, sendedValue = value};

        data.append(obj);
        AntyCheatLog.Write("STATISTICS", getPlayerName(playerId), getPlayerSerial(playerId), value, originalValue);
    }
}