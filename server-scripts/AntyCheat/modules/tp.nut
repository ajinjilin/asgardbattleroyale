
_setPlayerPosition <- setPlayerPosition;

class TpDetector extends AntyCheatModule
{
    safeSend = false;

    constructor(pid)
    {
        safeSend = false;

        base.constructor(pid);
    }

    function Write()
    {
        local playerId = id;
        local exist = false;
        local obj = {player = playerId, count = 1};

        if(safeSend) {
            safeSend = false;
            return;
        }   

        foreach(item in data)
        {
            if(item.player == playerId) {
                item.count = item.count + 1;
                exist = true;
                AntyCheatLog.Update("TP", getPlayerSerial(playerId));
                return;
            }
        }

        if(exist == false)
        {
            data.append(obj);
            AntyCheatLog.Write("TP", getPlayerName(playerId), getPlayerSerial(playerId), 1);
        }        
    }
}

function setPlayerPosition(pid, posx, posy, posz)
{
    getPlayerAntyCheat(pid).Tp().safeSend = true;
    _setPlayerPosition(pid, posx, posy, posz);
}