
local ticks = {};

class createBoss extends Bot
{
    drop = null;
    enemies = null;
    basePosition = null;
    lastAttack = -1;

    constructor(name, x, y, z, angle)
    {
        base.constructor(name);

        this.position = {x = x, y = y, z = z}
        this.basePosition = {x = x, y = y, z = z}
        this.angle = angle

        this.schemeId = BotScheme.Boss;
        this.ai = BotAI.Search;

        this.enemy = -1;
        this.timer = 0;
        this.enemies = [];

        this.lastAttack = -1;

        ticks[id] <- getTickCount() + 1000;

        instance = "";
        drop = [];

        this.onPositionUpdate();
    }

    function writeAdditionalInformations(packet) {
        packet.writeUInt8(ai);
    }

    function onTimer() {
        if(exist == false) {
            ticks[id] = getTickCount() + 1000;
            return;
        }

        if(ai == BotAI.Search)
            searchNearbyEnemy();
        else if(ai == BotAI.Attack)
            attackEnemy();
        else if(ai == BotAI.Dead)
            goesDead();
    }

    function addEnemy(pid)
    {
        foreach(enemyObj in enemies)
            if(enemyObj._id == pid)
                return enemyObj;

        local typ = 1;
        if(isPlayerConnected(pid)) {
            typ = 0;

            foreach(bot in getPlayerHelpers(pid))
                bot.addEnemy(id);
        }

        enemies.append({_type = typ, _id = pid});
        return enemies[enemies.len()-1];
    }

    function removeEnemy(pid)
    {
        foreach(index, enemyId in enemies)
            if(enemy._id == pid)
                enemies.remove(index);
    }

    function goesDead()
    {
        removeBot(id);
    }

    function attackEnemy() {
        if(getEnemy() == -1)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 100;
            return;
        }

        local distance = getDistanceBetweenPositions(getEnemyPosition(), position);
        if(distance > 1600)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 1000;
            return;
        }

        turnIntoEnemy();
        
        if(distance > 500)
        {
            local randAttack = rand() % 4;
            switch(randAttack)
            {
                case 2:
                    hitTarget(BotBossAttack.RangeAdditional);
                break;
                default:
                    hitTarget(BotBossAttack.Range);
                break;
            }
            setBotPosition(id, basePosition.x, basePosition.y, basePosition.z);
            ticks[id] = getTickCount() + 1500;
            return;
        } else {     
            local randAttack = rand() % 4;
            switch(randAttack)
            {
                case 2:
                    hitTarget(BotBossAttack.MeleeAdditional);
                break;
                default:
                    hitTarget(BotBossAttack.Melee);
                break;
            }
            ticks[id] = getTickCount() + 1000;
            return;
        }        
    }

    function searchNearbyEnemy() {
        local lastDistance = 1600;
        local positionObject = null;

        foreach(pid,player in getAllPlayers())
            if(player != null)
                if(getDistanceBetweenPositions(getPlayerPosition(pid), position) <= 1600)
                    addEnemy(pid);

        foreach(index, enemyObj in enemies)
        {
            if(enemyObj._type == 1)
            {
                local bObj = getBot(enemyObj._id);
                if(bObj == null)
                {
                    enemies.remove(index);
                    continue;
                }
                if(bObj.health <= 0)
                {
                    enemies.remove(index);
                    continue;
                }

                positionObject = bObj.position;
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyObj);
                }
                else if(dist > 1000)
                {
                    enemies.remove(index);
                    continue;
                }
            }
            else
            {
                if(!isPlayerConnected(enemyObj._id))
                {
                    enemies.remove(index);
                    continue;
                }
                if(!isPlayerSpawned(enemyObj._id))
                {
                    enemies.remove(index);
                    continue;
                }
                if(getPlayerInvisible(enemyObj._id))
                {
                    enemies.remove(index);
                    continue;
                }
                positionObject = getPlayerPosition(enemyObj._id);
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyObj);
                }
                else if(dist > 1600)
                {
                    enemies.remove(index);
                    continue;
                }
            }
        }

        ticks[id] = getTickCount() + 1000;
    }


    function setEnemy(enemyObj) {
        enemy = enemyObj;
        ai = BotAI.Attack;
    }

    function getEnemy() {
        if(enemy == -1)
            return enemy;

        if(enemy._type == 0)
        {
            if(!isPlayerConnected(enemy._id))
                enemy = -1;
            else if(!isPlayerSpawned(enemy._id))
                enemy = -1;
            else if(getPlayerHealth(enemy._id) <= 0)
                enemy = -1;
            else if(getPlayerInvisible(enemy._id))
                enemy = -1;
        }else {
            local bObj = getBot(enemy._id);
            if(bObj == null)
                enemy = -1;
            else if(bObj.health <= 0)
                enemy = -1;
        }

        return enemy;
    }

    function getEnemyPosition()
    {
        if(enemy._type == 0)
            return getPlayerPosition(enemy._id);
        else
            return getBotPosition(enemy._id);
    }

    function turnIntoEnemy()
    {
        local pos = getEnemyPosition();
        local _angle = getVectorAngle(position.x,position.z,pos.x,pos.z);
        local angleDiff = abs(_angle - angle);

        if(angleDiff > 10)
            setBotAngle(id, _angle);
    }

    function hitTarget(typeAttackId) {
        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.BossAttack);
        packet.writeInt16(id);
        packet.writeInt16(enemy._id);
        packet.writeInt16(enemy._type);
        packet.writeUInt8(typeAttackId);
        packet.sendToPlayersInTable(RELIABLE_ORDERED, visiblePlayers);

        if(enemy._type == 1)
            getBot(enemy._id).getAttackedByNpc(this);
    }

    function attackPlayer(pid) {
        local dmg = calculateDamageToPlayerByNPC(this, pid, DAMAGE_BEAST);

        if(dmg <= 3)
            dmg = 3;

        Bot_addHelperBotsEnemy(pid, id);

        local hp = getPlayerHealth(pid) - dmg;
        if(hp < 0) {
            hp = 0;
            setBotAnimation(id, "STOP");
            enemy = -1;
            ai = BotAI.Search;
        }

        setPlayerHealth(pid, hp);
    }

    function playerKillBot(pid) {
        ticks[id] = getTickCount() + 5000;
        ai = BotAI.Dead;

        for(local i = 0; i <= DROP_AMOUNT_FROM_BOT; i = i + 1)
        {
            local chance = rand() % drop.len();
            local dropItem = drop[chance];

            Drop(dropItem[0], dropItem[1], pid, position.x + rand() % 100,position.y + 50, position.z + rand() % 100);
        }

        local chanceForArrows = rand() % 100;

        if(chanceForArrows < 20)
            Drop("ITRW_ARROW", 20, pid, position.x - rand() % 100,position.y + 50, position.z - rand() % 100);
        else if(chanceForArrows < 40)
            Drop("ITRW_BOLT", 20, pid, position.x - rand() % 100,position.y + 50, position.z - rand() % 100);

        setPlayerBuildingPoints(pid, getPlayerBuildingPoints(pid) + 50);
    }

    function getAttackedByPlayer(pid) {
        local dmg = calculateDamageForNPC(pid, this);

        if(getDistanceBetweenPositions(getPlayerPosition(pid), position) >= 1800)
            return;

        if(getEnemy() == -1)
        {
            local newEnemy = addEnemy(pid);
            setEnemy(newEnemy);
            ai = BotAI.Attack;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttack == pid && getEnemy()._id != pid)
        {
            local newEnemy = addEnemy(pid);
            setEnemy(newEnemy);
        }

        Bot_addHelperBotsEnemy(pid, id);

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            playerKillBot(pid);
            removeBot(id);
            return;
        }

        lastAttack = pid;
        setBotHealth(id, health);
    }

    function getAttackedByNpc(bot)
    {
        local dmg = calculateDamageToNPCByNPC(bot, this, DAMAGE_EDGE);

        if(getEnemy() == -1)
        {
            local newEnemy = addEnemy(bot.id);
            setEnemy(newEnemy);
            ai = BotAI.Attack;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttack == bot.id && getEnemy()._id != bot.id)
        {
            local newEnemy = addEnemy(bot.id);
            setEnemy(newEnemy);
        }

        lastAttack = bot.id;

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            removeBot(id);

            if(bot instanceof createHelper)
            {
                local playerHelperId = bot.helperUserId;
                for(local i = 0; i <= DROP_AMOUNT_FROM_BOT; i = i + 1)
                {
                    local chance = rand() % drop.len();
                    local dropItem = drop[chance];

                    Drop(dropItem[0], dropItem[1], playerHelperId, position.x + rand() % 100,position.y - 50, position.z + rand() % 100);
                }

                local chanceForArrows = rand() % 100;

                if(chanceForArrows < 20)
                    Drop("ITRW_ARROW", 20, playerHelperId, position.x - rand() % 100,position.y - 50, position.z - rand() % 100);
                else if(chanceForArrows < 40)
                    Drop("ITRW_BOLT", 20, playerHelperId, position.x - rand() % 100,position.y - 50, position.z - rand() % 100);
            }
            return;
        }

        setBotHealth(id, health);
    }

    function beforeRemove()
    {
        ticks.rawdelete(id);
    }

    ai = -1;
    enemy = -1;

    timer = -1;
}

setTimer(function () {
    local current = getTickCount();
    foreach(botId, time in ticks)
    {
        if(time <= current) {
            getBot(botId).onTimer();
        }
    }
}, 100, 0);

function clearBossBotTicks() {
    ticks.clear();
}