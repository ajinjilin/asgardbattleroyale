local All = {};

class Bot
{
    id = -1;
    name = null;

    visiblePlayers = null;
    gridIndex = -1;

    streamerId = -1;
    schemeId = -1;

    animation = "";
    
    health = 0;
    maxHealth = 0;

    angle = null;
    position = null;

    damage = null;
    magicDamage = null;

    protection = null;
    magicProtection = null;

    armor = -1;
    melee = -1;
    ranged = -1;

    headTxt = -1;
    headModel = "";
    bodyTxt = -1;
    bodyModel = "";

    weaponMode = 0;
    instance = "";

    exist = false;

    constructor(_name)
    {
        id = getNextFreeId();
        name = _name;
        visiblePlayers = [];
        gridIndex = -1;

        streamerId = -1;
        schemeId = -1;

        armor = -1;
        melee = -1;
        ranged = -1;

        headTxt = -1;
        headModel = "";
        bodyTxt = -1;
        bodyModel = "";
        
        health = 1000;
        maxHealth = 1000;

        animation = "S_RUN";

        weaponMode = false;
        instance = "PC_HERO";

        angle = 0;
        position = {x = 0, y = 0, z = 0};

        magicDamage = 0;
        damage = 0;

        magicProtection = 0;
        protection = 0;

        exist = false;

        All[id] <- this;
    }

    function writeAdditionalInformations(packet) {
        
    }

    function beforeRemove()
    {
        
    }

    function registerForClients() {
        exist = true;
        
        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.Init);
        packet.writeInt16(id);
        packet.writeInt16(streamerId);
        packet.writeUInt8(schemeId);
        packet.writeString(name);
        packet.writeString(animation);
        packet.writeFloat(position.x);
        packet.writeFloat(position.y);
        packet.writeFloat(position.z);
        packet.writeInt16(angle);
        writeAdditionalInformations(packet);
        packet.sendToPlayers(RELIABLE_ORDERED);  

        onPositionUpdate();
    }

    function checkForNewStreamer() {
        local ping = {value = 100000 id = -1};

        foreach(playerId in visiblePlayers)
            if(getPlayerPing(playerId) < ping.value)
                ping = {value = getPlayerPing(playerId), id = playerId}

        setBoStreamer(id, ping.id);
    }

    function spawn(pid) {
        local find = visiblePlayers.find(pid);
        if(find != null)
            return;

        visiblePlayers.append(pid); 

        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.Spawn);
        packet.writeInt16(id)
        packet.writeString(animation);
        packet.writeFloat(position.x);
        packet.writeFloat(position.y);
        packet.writeFloat(position.z);
        packet.writeInt16(angle);
        packet.writeInt16(health);
        packet.writeInt16(maxHealth);
        packet.send(pid, RELIABLE_ORDERED);      

        if(streamerId == -1)
            checkForNewStreamer();   
        else if((getPlayerPing(streamerId) - 20) > getPlayerPing(pid))
            setBoStreamer(id, pid);
    }

    function unspawn(pid) {
        local find = visiblePlayers.find(pid);
        if(find == null)
            return;

        visiblePlayers.remove(find);   

        if(streamerId == pid)
            checkForNewStreamer();

        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.Unspawn);
        packet.writeInt16(id)
        packet.send(pid, RELIABLE_ORDERED);         
    }

    function onPositionUpdate() {
        if(exist)
            grid.onBotPositionChange(id, position.x, position.y, position.z);
    }
}

function onPlayerUpdateBot(pid, botid, x, y, z, angle) {
    if(!(botid in All))
        return;

    local packet = Packet(Packets.Bots);

    All[botid].position = {x = x, y = y, z = z};

    packet.writeUInt8(BotPackets.Synchronization);
    packet.writeInt16(botid);
    packet.writeFloat(x);
    packet.writeFloat(y);
    packet.writeFloat(z);
    packet.writeInt16(angle);
    packet.sendToPlayersInTableWithIgnore(RELIABLE_ORDERED, All[botid].visiblePlayers, pid);

    grid.onBotPositionChange(botid, x, y, z);
}

function setBoStreamer(id, streamer) 
{
    All[id].streamerId = streamer;

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Streamer);
    packet.writeInt16(id);
    packet.writeInt16(streamer);
    packet.sendToPlayers(RELIABLE_ORDERED);       
}
function setBotHealth(id, value) 
{
    All[id].health = value;

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Health);
    packet.writeInt16(id);
    packet.writeInt16(value);
    packet.sendToPlayersInTable(RELIABLE_ORDERED, All[id].visiblePlayers);   
}
function setBotPosition(id, posx, posy, posz)
{
    All[id].position = {x = posx, y = posy, z = posz};
    All[id].onPositionUpdate();

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Position);
    packet.writeInt16(id);
    packet.writeFloat(posx);
    packet.writeFloat(posy);
    packet.writeFloat(posz);
    packet.sendToPlayersInTable(RELIABLE_ORDERED, All[id].visiblePlayers);
}
function setBotAngle(id, angle)
{
    All[id].angle = angle;

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Angle);
    packet.writeInt16(id);
    packet.writeInt16(angle);
    packet.sendToPlayersInTable(RELIABLE_ORDERED, All[id].visiblePlayers);
}
function openWeapon(id, value)
{
    All[id].weaponMode = value;

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.WeaponMode);
    packet.writeInt16(id);
    packet.writeBool(value);
    packet.sendToPlayers(RELIABLE_ORDERED);   
}
function setBotAnimation(id, value) 
{
    All[id].animation = value;

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Animation);
    packet.writeInt16(id);
    packet.writeString(value);
    packet.sendToPlayersInTable(RELIABLE_ORDERED, All[id].visiblePlayers);
}

function removeBot(id) 
{
    local bot = All[id];
    bot.beforeRemove();

    local packet = Packet(Packets.Bots);
    packet.writeUInt8(BotPackets.Remove);
    packet.writeInt16(id);
    packet.sendToPlayers(RELIABLE_ORDERED);

    grid.removeBot(bot.gridIndex, id);

    All.rawdelete(id);
}

function Bot_onPlayerJoin(pid) {
    foreach(bot in All)
    {
        if(!bot.exist)
            continue;

        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.Init);
        packet.writeInt16(bot.id);
        packet.writeInt16(bot.streamerId);
        packet.writeUInt8(bot.schemeId);
        packet.writeString(bot.name);
        packet.writeString(bot.animation);
        packet.writeFloat(bot.position.x);
        packet.writeFloat(bot.position.y);
        packet.writeFloat(bot.position.z);
        packet.writeInt16(bot.angle);
        bot.writeAdditionalInformations(packet);
        packet.send(pid, RELIABLE_ORDERED);
    }
}

function Bot_onPlayerDead(pid, kid) {
    foreach(bot in All)
    {
        if(bot instanceof createMount) {
            if(bot.mountedUserId == pid)
                removeBot(bot.id);

        } else if(bot instanceof createHelper) {
            if(bot.helperUserId == pid)
                removeBot(bot.id);
        }
    }
}

function Bot_onPlayerDisconnect(pid, res) {
    foreach(bot in All)
    {
        if(bot instanceof createMount) {
            if(bot.mountedUserId == pid)
                removeBot(bot.id);

        } else if(bot instanceof createHelper) {
            if(bot.helperUserId == pid)
                removeBot(bot.id);
        }

        bot.unspawn(pid);
    }
}

function Bot_onStartGame() {
    Bot_CreateRandom();
    Boss_CreateRandom();

    foreach(bot in All)
        bot.registerForClients();
}

function Bot_onEndGame() {
    All.clear();
    grid.clearGridFromBots();
    clearBotTicks();
}

function getBot(id) {
    if(id in All)
        return All[id];

    return null;
}

function getNextFreeId() {
    if(All.len() == 0)
        return 0;
    
    local lastId = -1;
    foreach(ind,inx in All)
        lastId = inx.id;

    return lastId + 1;
}

function getBotPosition(id)
{
    return getBot(id).position;
}

function destroyMount(pid) {
    foreach(bot in All)
        if(bot instanceof createMount)
            if(bot.mountedUserId == pid)
                removeBot(bot.id);
}

function destroyHelper(pid) {
    foreach(bot in All)
        if(bot instanceof createHelper)
            if(bot.helperUserId == pid)
                removeBot(bot.id);
}

function getPlayerHelpers(pid)
{
    local botsReturn = [];
    foreach(bot in All)
        if(bot instanceof createHelper)
            if(bot.helperUserId == pid)
                botsReturn.append(bot);

    return botsReturn;
}