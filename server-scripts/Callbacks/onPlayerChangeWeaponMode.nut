


addEventHandler("onPlayerChangeWeaponMode", function (pid, oldWM, currWM) {
    if(currWM == 0)
        Bot_openHelpersWeapons(pid, false);
    else {
        Bot_openHelpersWeapons(pid, true);

        if(getPlayer(pid).invisibility > 0) {
            getPlayer(pid).invisibility = 0;
            setPlayerInvisible(pid, false);
            smallNote(pid, "Jeste� ju� widoczny!");
        }
    }
})

