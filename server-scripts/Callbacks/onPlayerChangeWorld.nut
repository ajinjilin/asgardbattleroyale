
addEventHandler("onPlayerChangeWorld", function(pid, worldId)
{
    if(getPlayerWorld(pid) != getServerWorld()) {
        setPlayerWorld(pid, getServerWorld());   
    } 
    Game.goOnRandomSpawn(pid);
});
