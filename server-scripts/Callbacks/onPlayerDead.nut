
addEventHandler("onPlayerDead", function(playerid, killerid)
{
    if(killerid != -1)
    {
        local pObj = getPlayer(playerid), kObj = getPlayer(killerid);

        setPlayerHealth(killerid, getPlayerHealth(killerid) + 100);
        if(getPlayerHealth(killerid) > 1000)
            setPlayerHealth(killerid, 1000);

        if(Game.timer < 1800)
        {
            pObj.deaths = pObj.deaths + 1;
            kObj.kills = kObj.kills + 1;
            smallKillNote(getPlayerName(killerid) + "|"+getPlayerName(playerid));
            setPlayerBuildingPoints(killerid, getPlayerBuildingPoints(killerid) + 3);

            sendAdditionalInformations(playerid);
            sendAdditionalInformations(killerid);
        }
    }else {
        sendAdditionalInformations(playerid);
    }

    if(Game.timer <= 1800)
    {
        getItems(playerid).sendIntoBucket();
    }

    PlayerItem[playerid].equipped.clear();

    Bot_onPlayerDead(playerid, killerid);
});
