// Helpers

function getPlayerWeaponDamage(pid)
{
	local weaponMode = getPlayerWeaponMode(pid);
	local weapon = null, weaponPlayer = null;
    switch (weaponMode)
	{
		case WEAPONMODE_FIST:
            return {damage = 50, magicDamage = 50};
		break;

		case WEAPONMODE_1HS:
		case WEAPONMODE_2HS:
			weapon = getPlayerMeleeWeapon(pid);
		break;

		case WEAPONMODE_BOW:
		case WEAPONMODE_CBOW:
			weapon = getPlayerRangedWeapon(pid);
		break;

		case WEAPONMODE_MAG:
			weapon = getPlayerMagicWeapon(pid);
		break;
	}

    local weaponPlayer = ItemIntegration.getItem(weapon);
    if(weaponPlayer == null)
        return {damage = 50, magicDamage = 50}

    return {damage = weaponPlayer.damage, magicDamage = weaponPlayer.magicDamage}
}

function getPlayerAmorDefense(pid)
{
    local armor = getPlayerArmor(pid);
    local armorPlayer = ItemIntegration.getItem(armor);
    if(armorPlayer == null)
        return {protection = 0, magicProtection = 0}

    return {protection = armorPlayer.protection, magicProtection = armorPlayer.magicProtection}
}

// METHODS

local function handler(pid, kid, dmg, trueType)
{
	local now = getTickCount()
	if (now < getPlayerLastHitInterval(kid))
	{
		eventValue(0)
		return
	}

    if(getPlayerMounting(kid) == true)
    {
        stopMounting(kid)
		eventValue(0)
		return;
	}

    if(getPlayerObserver(kid) == true)
    {
        eventValue(0);
        return;
    }

    Bot_addHelperBotsEnemy(pid, kid);
    Bot_addHelperBotsEnemy(kid, pid);

    if(getPlayerMounting(pid) == true)
    {
        stopMounting(pid)
	}

	setPlayerLastHitInterval(kid, now + 200);

    local armor = getPlayerAmorDefense(pid);
    local weapon = getPlayerWeaponDamage(kid);

    local defense = armor.protection;
    local magicDefense = armor.magicProtection;

    local finalDamage = 0;

    local dmg = weapon.damage;
    local dmgMagic = weapon.magicDamage;

    local pObjBonusTable = Player[pid].bonus;
    local kObjBonusTable = Player[kid].bonus;

    if(pObjBonusTable[PlayerBonus.DefenseSmall] > 0)
    {
        defense = defense + 10;
        magicDefense = defense + 10;
    }
    if(pObjBonusTable[PlayerBonus.Defense] > 0)
    {
        defense = defense + 10;
        magicDefense = magicDefense + 10;
    }
    if(kObjBonusTable[PlayerBonus.DamageSmall] > 0)
    {
        dmg = dmg + 10;
        dmgMagic = dmgMagic + 10;
    }
    if(kObjBonusTable[PlayerBonus.Damage] > 0)
    {
        dmg = dmg + 10;
        dmgMagic = dmgMagic + 10;
    }

    if(Player[kid].hitBonus > 0)
    {
        Player[kid].hitBonus = Player[kid].hitBonus - 1;

        if(Player[pid].shieldBonus == 0)
        {
            dmg = abs(dmg * 1.3);
            dmgMagic = abs(dmgMagic * 1.3);
        }
    }

    if(Player[kid].hitMultiplier > 0)
    {
        Player[kid].hitMultiplier = Player[kid].hitMultiplier - 1;

        if(Player[pid].shieldBonus == 0)
        {
            dmg = dmg * 2;
            dmgMagic = dmgMagic * 2;
        }
    }

    if(Player[kid].smellyHit > 0)
    {
        Player[kid].smellyHit = Player[kid].smellyHit - 1;

        if(Player[pid].shieldBonus == 0)
        {
            defense = 0;
            magicDefense = 0;
        }
    }

    if(Player[pid].shieldBonus != 0)
    {
        Player[pid].shieldBonus = Player[pid].shieldBonus - 1;
    }

    if(dmg > 0)
    {
        if(defense > 0)
            finalDamage = finalDamage + dmg - abs((dmg * defense)/100)
        else
            finalDamage = finalDamage + dmg;
    }

    if(dmgMagic > 0)
    {
        if(magicDefense > 0)
            finalDamage = finalDamage + dmgMagic - abs((dmgMagic * magicDefense)/100)
        else
            finalDamage = finalDamage + dmgMagic;
    }    

    if(finalDamage < 1)
        finalDamage = 1;

    smallDmgNote(pid, finalDamage, getPlayerPosition(pid));

    Player[pid].hp = Player[pid].hp - finalDamage;
    if(Player[pid].hp < 0)
        Player[pid].hp = 0;

    eventValue(-finalDamage);
    return;
}

addEventHandler("onPlayerHit", handler);

function calculateDamageForNPC(playerId, bot) {
	local now = getTickCount()
	if (now < getPlayerLastHitInterval(playerId))
	{
		eventValue(0)
		return
	}

    if(getPlayerMounting(playerId) == true)
        stopMounting(playerId)


    if(getPlayerObserver(playerId) == true)
    {
        eventValue(0);
        return;
    }

	setPlayerLastHitInterval(playerId, now + 200);

    local weapon = getPlayerWeaponDamage(playerId);

    local defense = bot.protection;
    local magicDefense = bot.magicProtection;

    local finalDamage = 0;

    local dmg = weapon.damage;
    local dmgMagic = weapon.magicDamage;

    if(Player[playerId].hitBonus > 0)
    {
        Player[playerId].hitBonus = Player[playerId].hitBonus - 1;

        dmg = abs(dmg * 1.3);
        dmgMagic = abs(dmgMagic * 1.3);
    }

    if(Player[playerId].hitMultiplier > 0)
    {
        Player[playerId].hitMultiplier = Player[playerId].hitMultiplier - 1;

        dmg = dmg * 2;
        dmgMagic = dmgMagic * 2;
    }

    if(Player[playerId].smellyHit > 0)
    {
        Player[playerId].smellyHit = Player[playerId].smellyHit - 1;

        defense = 0;
        magicDefense = 0;
    }

    if(dmg > 0)
    {
        if(defense > 0)
            finalDamage = finalDamage + dmg - abs((dmg * defense)/100)
        else
            finalDamage = finalDamage + dmg;
    }

    if(dmgMagic > 0)
    {
        if(magicDefense > 0)
            finalDamage = finalDamage + dmgMagic - abs((dmgMagic * magicDefense)/100)
        else
            finalDamage = finalDamage + dmgMagic;
    }    

    if(finalDamage < 1)
        finalDamage = 1;

    smallDmgNote(bot.id, finalDamage, bot.position);

    if(getOrcPlayer() == playerId)
        finalDamage = finalDamage + 40;

    return finalDamage;
}


function calculateDamageToPlayerByNPC(bot, playerId, type) {

    if(getPlayerMounting(playerId) == true)
        stopMounting(playerId)

    local armor = getPlayerAmorDefense(playerId);

    local defense = armor.protection;
    local magicDefense = armor.magicProtection;

    local finalDamage = 0;

    local dmg = bot.damage;
    local dmgMagic = bot.magicDamage;

    if(dmg > 0)
    {
        if(defense > 0)
            finalDamage = finalDamage + dmg - abs((dmg * defense)/100)
        else
            finalDamage = finalDamage + dmg;
    }

    if(dmgMagic > 0)
    {
        if(magicDefense > 0)
            finalDamage = finalDamage + dmgMagic - abs((dmgMagic * magicDefense)/100)
        else
            finalDamage = finalDamage + dmgMagic;
    }    

    if(finalDamage < 1)
        finalDamage = 1;

    smallDmgNote(playerId, finalDamage, getPlayerPosition(playerId));

    return finalDamage;
}

function calculateDamageToNPCByNPC(bot, otherbot, type) {
    local dmg = bot.damage;
    local dmgMagic = bot.magicDamage;

    local defense = otherbot.protection;
    local magicDefense = otherbot.magicProtection;

    local finalDamage = 0;

    if(dmg > 0)
    {
        if(defense > 0)
            finalDamage = finalDamage + dmg - abs((dmg * defense)/100)
        else
            finalDamage = finalDamage + dmg;
    }

    if(dmgMagic > 0)
    {
        if(magicDefense > 0)
            finalDamage = finalDamage + dmgMagic - abs((dmgMagic * magicDefense)/100)
        else
            finalDamage = finalDamage + dmgMagic;
    }

    return finalDamage;
}