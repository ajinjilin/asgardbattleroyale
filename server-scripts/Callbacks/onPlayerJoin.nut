


addEventHandler("onPlayerJoin", function (pid) {
    switch(Game.activeWorld)
    {
        case Worlds.Jarkendar:
            setPlayerPosition(pid, -32675.5, 942.734, 8951.88);
        break;
        default:
            setPlayerPosition(pid, 0, 1000, 0);
        break;
    }


    getPlayer(pid).join();
    Bot_onPlayerJoin(pid);
    Draw3d_onPlayerJoin(pid);
    BanList.onPlayerJoin(pid);
    Clan.onJoin(pid);
    Structure.onPlayerJoin(pid);

    setPlayerRespawnTime(pid, 10000);
})

