addEventHandler("onPlayerMessage", function(pid, message)
{
    if(Game.status == GameStatus.Game)
    {
        if(message.slice(0, 1) == "#")
        {
            local clanId = getPlayerClan(pid);
            if(clanId != -1)
            {
                message = getPlayerName(pid) +": "+ message.slice(1);
                for(local i = 0; i < getMaxSlots(); i++)
                {
                    if(!isPlayerConnected(i))
                        continue;

                    if(getPlayerClan(i) != clanId)
                        continue;

                    local stripedText = StringLib.textWrap(message);
                    foreach(tekst in stripedText)
                        sendMessageToPlayer(i, 126, 186, 6, tekst);
                }
            }
            return;
        }
        if(message.slice(0, 1) == "!")
        {
            message = getPlayerName(pid) +": "+ message.slice(1);
            StringLib.distanceChat(pid, {r = 5, g = 160, b = 210}, -1, message);
            return;
        }

        message = getPlayerName(pid) + " m�wi: "+message;
        StringLib.distanceChat(pid, {r = 255, g = 255, b = 255}, 1300, message);
    }
});