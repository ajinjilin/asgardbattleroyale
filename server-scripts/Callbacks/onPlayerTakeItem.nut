
local function handler(pid, item)
{
    local GroundItemsBucket = getItemInGroundBucket();
    foreach(_index, _item in GroundItemsBucket)
    {
        if(_item.item == item)
        {
            giveItem(pid, item.instance, item.amount, false);
            GroundItemsBucket.remove(_index);
            return;
        }
    }
    local All = getAllDropedItems();
    foreach(_index, _item in All)
    {
        if(_item.item == item)
        {
            if(_item.time > 0)
            {
                if(pid != _item.owner){
                    cancelEvent();
                    return false;
                }else{
                    giveItem(pid, item.instance, item.amount, false);
                    _item.remove();
                    All.remove(_index);
                    return true;
                }
            }else{
                giveItem(pid, item.instance, item.amount, false);
                _item.remove();
                All.remove(_index);
                return true;
            }
        }
    }
    cancelEvent();
    return false;
}

addEventHandler("onPlayerTakeItem", handler)
