class mDate {

	static ADD_DAYS = 1;
	static REMOVE_DAYS = 2;

	static ADD_HOURS = 3;
	static REMOVE_HOURS = 4;

	static ADD_MINUTES = 5;
	static REMOVE_MINUTES = 6;


	static function get(timestamp = time()) {
		local DObj = date(timestamp);
		DObj.time <- timestamp;
		DObj.str <- format("%02d-%02d-%02d %02d:%02d", DObj.year, DObj.month+1, DObj.day, DObj.hour, DObj.min);
		return DObj;
	}

	function getPoor(timestamp = time()) {
		local DObj = date();
		return format("%02d-%02d-%02d", DObj.year, DObj.month+1, DObj.day);
	}

	static function strtotime(_date, type, count)
	{
		local timestamp = _date.time;

		switch(type)
		{
			case ADD_DAYS: timestamp += (24*60*60) * count; break;
			case ADD_HOURS: timestamp += (60*60) * count; break;
			case ADD_MINUTES: timestamp += 60 * count; break;
			case REMOVE_DAYS: timestamp -= (24*60*60) * count; break;
			case REMOVE_HOURS: timestamp -= (60*60) * count; break;
			case REMOVE_MINUTES: timestamp -= 60 * count; break;
		}

		local DObj = date(timestamp);
		DObj.time <- timestamp;
		DObj.str <- format("%02d-%02d-%02d %02d:%02d", DObj.year, DObj.month+1, DObj.day, DObj.hour, DObj.min);
		return DObj;
	}

	static function isDatesBetween(_date, dateOne, dateTwo)
	{
		return dateOne.time <= _date.time && dateTwo.time >= _date.time;
	}


	// simple version to '21-12-2000 20:21'
	static function strToDate(str)
	{
		local ex = null;

		if(str.len() > 11)
			ex = regexp(@"([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)");
		else
			ex = regexp(@"([0-9]+)-([0-9]+)-([0-9]+)");

		local ca = ex.capture(str);
		if (ca.len() != 6 && ca.len() != 4) throw "Keep in format '2020-12-21 10:10 or 2020-12-21'";

		local year = str.slice(ca[1].begin, ca[1].end),
		 month = str.slice(ca[2].begin, ca[2].end).tointeger() -1,
		 day = str.slice(ca[3].begin, ca[3].end).tointeger();

		local hour = -1, minute = -1;
		if(ca.len() == 6) {
			hour = str.slice(ca[4].begin, ca[4].end).tointeger();
			minute = str.slice(ca[5].begin, ca[5].end).tointeger();
		}

		local epoch_offset = {"2016":1451606400, "2017":1483228800, "2018":1514764800, "2019":1546300800, "2020":1577836800, "2021":1609459200, "2022":1640995200};
		local seconds_per_month = [ 2678400, 2419200, 2678400, 2592000, 2678400, 2592000, 2678400, 2678400, 2592000, 2678400, 2592000, 2678400];
		local leap = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
		if (leap) seconds_per_month[1] = 2505600;

		local offset = epoch_offset[year.tostring()];
		for (local m = 0; m < month; m++) offset += seconds_per_month[m];

		offset += (day * 86400);
		if(hour != -1) {
			offset += (hour * 3600);
			offset += (minute * 60);
		}

		offset += -120 * 60;

		local DObj = date(offset);
		DObj.time <- offset;
		DObj.str <- format("%02d-%02d-%02d %02d:%02d", DObj.year, DObj.month+1, DObj.day, DObj.hour, DObj.min);
		return DObj;
	}

	// expand version for use in logs and sql like "Tuesday, January 21, 2020 21:57"
	static function fullStringDate(str) {
		local ex = regexp(@" ([a-zA-Z]+) ([0-9]+), ([0-9]+) ([0-9]+):([0-9]+)");
		local ca = ex.capture(str);
		if (ca.len() != 6) throw "Keep in format 'Tuesday, January 7, 2022 10:00'";

		local month = str.slice(ca[1].begin, ca[1].end);
		switch (month) {
			case "January": month = 0; break;
			case "February": month = 1; break;
			case "March": month = 2; break;
			case "April": month = 3; break;
			case "May": month = 4; break;
			case "June": month = 5; break;
			case "July": month = 6; break;
			case "August": month = 7; break;
			case "September": month = 8; break;
			case "October": month = 9; break;
			case "November": month = 10; break;
			case "December": month = 11; break;
			default: throw "Not given month";
		}

		local day = str.slice(ca[2].begin, ca[2].end).tointeger()-1,
		 year = str.slice(ca[3].begin, ca[3].end).tointeger(),
		 hour = str.slice(ca[4].begin, ca[4].end).tointeger(),
		 min = str.slice(ca[5].begin, ca[5].end).tointeger(),
		 sec = 0;

		local epoch_offset = {"2016":1451606400, "2017":1483228800, "2018":1514764800, "2019":1546300800, "2020":1577836800, "2021":1609459200, "2022":1640995200};
		local seconds_per_month = [ 2678400, 2419200, 2678400, 2592000, 2678400, 2592000, 2678400, 2678400, 2592000, 2678400, 2592000, 2678400];
		local leap = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
		if (leap) seconds_per_month[1] = 2505600;

		local offset = epoch_offset[year.tostring()];
		for (local m = 0; m < month; m++) offset += seconds_per_month[m];

		offset += (day * 86400);
		offset += (hour * 3600);
		offset += (min * 60);
		offset += sec;
		offset += -60 * 60;

		local DObj = date(offset);
		DObj.time <- offset;
		DObj.str <- format("%02d-%02d-%02d %02d:%02d", DObj.year, DObj.month+1, DObj.day, DObj.hour, DObj.min);
		return DObj;
	}

	static function getDayOfMonth(d=date().day, m=date().month+1, y=date().year) {
        // Use Zeller's Rule (see http://mathforum.org/dr.math/faq/faq.calendar.html)
        m -= 2;
        if (m < 1) m += 12;
        local e = y.tostring().slice(2).tointeger();
        local s = y.tostring().slice(0,2).tointeger();
        local t = m > 10 ? e - 1 : e;
        local f = d + ((13 * m - 1) / 5) + t + (t / 4) + (s / 4) - (2 * s);
        f = f % 7;
        if (f < 0) f += 7;

		local days = ["Sunday","Monday", "Tuesday", "Wednesday","Thursday","Friday","Saturday"];

        return days[f];
    }
}




/*
Tests

local obj = mDate.fullStringDate("Tuesday, January 21, 2020 21:57");

print(obj.str);
print(mDate.get().str);
print(mDate.getDayOfMonth());
print(mDate.strtotime(mDate.strToDate("2020-06-22 10:12"), mDate.ADD_MINUTES, 4).str);

local _date = mDate.strToDate("2020-06-22");
local dateOne = mDate.strToDate("2020-04-10 10:10");
local dateTwo = mDate.strToDate("2020-05-22 15:20");

local compare = mDate.isDatesBetween(_date, dateOne, dateTwo);
print(compare);

*/