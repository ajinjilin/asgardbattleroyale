local All = [];

class ItemInGroundBucket
{
    item = null;

    constructor(itemId, amount, x,y,z)
    {
        item = ItemsGround.spawn(itemId, amount, x,y,z, getServerWorld());

        All.append(this);
    }

    static function onEndGame()
    {
        foreach(_item in All)
            ItemsGround.destroy(_item.item.id);

        All.clear();
    }
}

function getItemInGroundBucket()
{
    return All;
}