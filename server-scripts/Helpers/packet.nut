
class Packet extends Packet
{
    constructor(packetId){
        base.constructor();
        writeUInt8(packetId);
    }

    function sendToPlayersIgnore(priority, ignore) {
        foreach(pid, player in getAllPlayers())
            if(player != null && pid != ignore)
                send(pid, priority)        
    }
    
    function sendToPlayers(priority) {
        foreach(pid, player in getAllPlayers())
            if(player != null)
                send(pid, priority)
    }

    function sendInRange(id, range, priority) {
        foreach(pid,player in getAllPlayers())
            if(player != null)
                if(getDistanceBetweenPlayers(id, pid) <= range)
                    send(pid, priority)
    }

    function sendInDistanceRange(pos, range, priority) {
        foreach(pid,player in getAllPlayers())
            if(player != null)
                if(getDistanceBetweenPositions(getPlayerPosition(pid), pos) <= range)
                    send(pid, priority)
    }   

    function sendToPlayersInTableWithIgnore(priority, table, ignore) 
    {
        foreach(playerId in table)
            if(playerId != null && playerId != ignore)
                send(playerId, priority)   
    }

    function sendToPlayersInTable(priority, table) 
    {
         foreach(playerId in table)
            if(playerId != null)
                send(playerId, priority)          
    }
}