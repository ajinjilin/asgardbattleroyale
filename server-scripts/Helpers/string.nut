StringLib <- {};

function StringLib::distanceChat(pid, color, distance, message)
{
    for(local i = 0; i < getMaxSlots(); i++)
    {
        if(!isPlayerConnected(i))
            continue;

        if(distance != -1)
            if(getDistanceBetweenPlayers(pid, i) > distance)
                continue;

        local stripedText = StringLib.textWrap(message);
        foreach(tekst in stripedText)
            sendMessageToPlayer(i, color.r, color.g, color.b, tekst);
    }
}

function StringLib::textWrap(text, lenght = 55)
{
    local result = [];

    if(text.len() < lenght)
        return [text];

    local findSpaces = findInTextChar(text, " ");

    local textLen = text.len()/lenght;
    local recognize = text;

    for(local i = 0; i <= textLen; i ++)
    {
        if(recognize.len() > lenght)
        {
            local tekstToAppend = recognize.slice(0, lenght);
            local findSpaces = findInTextChar(tekstToAppend, " ");
            if(findSpaces.len() == 0 )
                findSpaces = [lenght];

            local findCharSpace = findSpaces[findSpaces.len()-1];
            tekstToAppend = recognize.slice(0, findCharSpace);
            recognize = recognize.slice(findCharSpace+1, (recognize.len()));
            result.append(tekstToAppend);
        }else{
            result.append(recognize.slice(0, recognize.len()))
        }
    }

    return result;
}

function StringLib::findInTextChar(text, char)
{
    local returnTab = [];
    local index = 999;
    local wordCount = 0;

    do {
        index = text.find(char);

        if (index != null) {
            text = text.slice(index + char.len());
            wordCount++;
            if(returnTab.len() == 0)
                returnTab.append(index);
            else
                returnTab.append((index+1) + returnTab[returnTab.len()-1]);
        }
    } while (index != null);

    return returnTab;
}

function StringLib::ReplaceChar(text, x, newChar)
{
    local newText = text.slice(0, x-1);
    local nextText = text.slice(x, text.len());
    return newText + newChar + nextText;
}

function StringLib::DePolonizer(text)
{
    local specialChars = ["�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"];
    local nonSpecialChars = ["E","O","A","S","L","Z","Z","C","N","e","o","a","s","l","z","z","c","n"];
    for(local i = 0; i < specialChars.len(); i++)
    {
        local char = specialChars[i], index = null;
        do {
            index = text.find(char);
            if (index != null) {
                text = StringLib.ReplaceChar(text,index+1,nonSpecialChars[i])
            }
        } while (index != null);
    }
    return text;
}

function StringLib::parseBool(text) {
    text = text.toupper();
    if(text == "1" || text == "TAK" || text == "TRUE")
        return true;

    return false;
}