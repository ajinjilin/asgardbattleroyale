function eqUsePlayerItem(pid, idItem)
{
    local item = ItemIntegration.getItem(idItem);
    if(!item)
        return;

    local instance = Items.name(idItem);

    if(hasPlayerItem(pid, instance) == 0)
        return;

    switch(item.flag)
    {
        case ITEM_KAT_OTHER:
            return;
        break;
        case ITEM_KAT_FOOD:
            local hp = getPlayerHealth(pid);
            hp = hp + item.damage;

            if(hp > getPlayerMaxHealth(pid))
                hp = getPlayerMaxHealth(pid);

            setPlayerHealth(pid, hp);
            removeItem(pid, instance, 1);
        break;
        default:
            if(isPlayerItemEquiped(pid, idItem))
                _unequipItem(pid, idItem);
            else
                _equipItem(pid, idItem);
        break;
    }
    ItemIntegration.packetEqUse(pid);
}

function equipItem(pid, instance, wear = true)
{
    local id = -1;
    if(type(instance) == "string")
    {
        instance = instance.toupper();
        id = Items.id(instance);
        if(id == -1)
            return;
    }else{
        id = instance;
        instance = Items.name(id);
    }

    local item = ItemIntegration.getItem(id);
    if(!item)
        return;

    if(hasPlayerItem(pid, instance) == 0)
        return;

    if(wear)
        _equipItem(pid, id);

    PlayerItem[pid].equipped.append(id);
}

function unEquipItem(pid, instance, wear = true)
{
    local id = -1;
    if(type(instance) == "string")
    {
        instance = instance.toupper();
        id = Items.id(instance);
        if(id == -1)
            return;
    }else{
        id = instance;
        instance = Items.name(id);
    }

    local item = ItemIntegration.getItem(id);
    if(!item)
        return;

    if(wear)
        _unequipItem(pid, id);

    if(PlayerItem[pid].equipped.find(id) != null)
        PlayerItem[pid].equipped.remove(PlayerItem[pid].equipped.find(id));
}

function addItemToEquipedTable(pid, instance) {
    instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
        return;

    PlayerItem[pid].equipped.append(id);
}

function isPlayerItemEquiped(pid, id)
{
    return PlayerItem[pid].equipped.find(id) != null;
}

function getEquippedItems(pid)
{
    return PlayerItem[pid].equipped;
}

function removeItem(pid, instance, amount, gave = true)
{
	instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return;

    local item = ItemIntegration.getItem(id);
    if(!item)
        return;

    local items = PlayerItem[pid].items;

    if(items.rawin(id))
    {
        if(items[id] > amount)
            items[id] = items[id] - amount;
        else
            items.rawdelete(id);
    }
    if(gave)
        _removeItem(pid, id, amount);
}

function giveItem(pid, instance, amount, gave = true)
{
    local id = instance;
    if(type(instance) == "string")
    {
        instance = instance.toupper();
        id = Items.id(instance);
        if(id == -1)
		    return;
    }

	local item = ItemIntegration.getItem(id);
	if(!item)
	 	return;

    local items = PlayerItem[pid].items;

    if(items.rawin(id))
        items[id] = items[id] + amount;
    else
        items[id] <- amount;

    if(gave)
        _giveItem(pid, id, amount);
}

function hasPlayerItem(pid,instance)
{
	instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return 0;

    if(!PlayerItem[pid].items.rawin(id))
        return 0;

	return PlayerItem[pid].items[id];
}

function getPlayerItems(pid)
{
	return PlayerItem[pid].items;
}

function clearItems(pid)
{
	PlayerItem[pid].items.clear();  
	PlayerItem[pid].equipped.clear();  

    local packet = Packet(Packets.Player);
	packet.writeUInt8(PlayerPackets.ClearIventory);
    packet.send(pid, RELIABLE_ORDERED);
}