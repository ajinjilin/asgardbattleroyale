
class Item
{
    constructor(_instance, arg)
    {
        instance = _instance;

        name = arg.name;
        value = arg.value;

        flag = arg.flag;

        description = "";

        if ("description" in arg)
            description = arg.description;

        damage = 0;
        magicDamage = 0;

        if ("damage" in arg)
            damage = arg.damage;

        if ("magicDamage" in arg)
            magicDamage = arg.magicDamage;

        protection = 0;
        magicProtection = 0;

        if ("protection" in arg)
            protection = arg.protection;

        if ("magicProtection" in arg)
            magicProtection = arg.magicProtection;
    }

	instance = null;

    protection = null;
    magicProtection = null;

    flag = null;

    name = null;
    value = null;
    description = null;

    damage = null;
	magicDamage = null;
}
