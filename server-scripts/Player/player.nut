class _Player
{
    id = -1;

    inGame = false;
    isAdmin = false;

    protection = null;
    position = null;
    gridIndex = -1;
    chunks = null;

    temporaryDMG = -1;
    lastUsedSpell = -1;
    lastHitInterval = -1;

    hitMultiplier = 0;
    hitBonus = 0;
    smellyHit = 0;
    shieldBonus = 0;

    mounting = false;
    pwMode = false;
    readyToPlay = false;
    choosenWorld = -1;

    buildingPoints = 10;
    hearts = 3;
    points = -1;

    bodyModel = 0;
    bodyTxt = 0;
    headModel = 0;
    headTxt = 0;

    str = 10;
    dex = 10;
    hp = 40;
    maxHp = 40;
    mana = 10;
    maxMana = 10;

    avatarId = -1;
    deaths = -1;
    kills = -1;
    winGames = -1;

    account = null;
    invisibility = 0;

    clanId = -1;
    isObserver = false;

    bonus = null;

    constructor(pid)
    {
        id = pid;

        inGame = false;
        isAdmin = false;
        readyToPlay = false;

        choosenWorld = -1;

        bonus = [0,0,0,0];
        protection = [0,0,0,0,0,0,0,0];
        position = {x = 0, y = 0, z = 0};
        gridIndex = -1;
        chunks = [];

        temporaryDMG = -1;
        lastUsedSpell = -1;
		lastHitInterval = -1;

		hitMultiplier = 0;
        hitBonus = 0;
        smellyHit = 0;
        shieldBonus = 0;

        avatarId = 0;
        deaths = 0;
        kills = 0;
        winGames = 0;

        mounting = false;
        pwMode = false;

        buildingPoints = 10;
        hearts = 3;
        points = 3;

        bodyModel = "Hum_Body_Naked0";
        bodyTxt = 2;
        headModel = "Hum_Head_FatBald";
        headTxt = 22;

        str = 10;
        dex = 10;
        hp = 40;
        maxHp = 40;
        mana = 10;
        maxMana = 10;

        account = null;
        invisibility = 0;

        clanId = -1;
        isObserver = false;
    }

    function respawn() {
        if(Game.timer <= 1800)
        {
            hearts = hearts - 1;
            setPlayerHearts(id, hearts);
        }

        setPlayerMaxMana(id, 100000);
        setPlayerMana(id, 100000);

        setPlayerMaxHealth(id, 1000);
        setPlayerHealth(id, 1000);

        setPlayerStrength(id, 200);
        setPlayerDexterity(id, 200);
        setPlayerMagicLevel(id, 6);

        setPlayerSkillWeapon(id, 0, 100);
        setPlayerSkillWeapon(id, 1, 100);
        setPlayerSkillWeapon(id, 2, 500);
        setPlayerSkillWeapon(id, 3, 500);

        setPlayerVisualFromAccountAvatarId(id, avatarId)

        if(getPlayerHearts(id) <= 0)
        {
            spawnPlayer(id);
            isObserver = true;
            enterObserver(id);
            return;
        }

        invisibility = 5;
        smallNote(id, "B�dziesz niewidoczny przez najbli�sze 5 sekund.");
        setPlayerInvisible(id, true);
        spawnPlayer(id);
    }

    function join() {
        addPlayerToSlot(id, getPlayerName(id));
        onPlayerJoinLobbyQueries(id);

        account = loadAccount(id);
        if(account == null)
        {
            saveAccount(id)
            account = loadAccount(id)
        }

        setPlayerVisualFromAccountAvatarId(id, avatarId);

        local record = Memory.UserRecordRead(getPlayerSerial(id));
        local itemsRecord = Memory.ItemsRecordRead(getPlayerSerial(id));

        setPlayerMaxMana(id, 100000);
        setPlayerMana(id, 100000);

        setPlayerMaxHealth(id, 1000);
        setPlayerHealth(id, 1000);

        if(itemsRecord.len() == 0)
        {
            giveItem(id, "ITAR_PRISONER", 1);
            giveItem(id, "ITMW_SHORTSWORD4", 1);
        }else{
            foreach(_item in itemsRecord)
            {
                giveItem(id, _item.itemId, _item.amount);
            }
            getItems(id).equipBestItems();
        }

        setPlayerStrength(id, 200);
        setPlayerDexterity(id, 200);
        setPlayerMagicLevel(id, 6);

        setPlayerSkillWeapon(id, 0, 100);
        setPlayerSkillWeapon(id, 1, 100);
        setPlayerSkillWeapon(id, 2, 500);
        setPlayerSkillWeapon(id, 3, 500);

        sendAdditionalInformations(id);

        if(record != null)
        {
            setPlayerPosition(id, record.x, record.y, record.z);
            setPlayerHealth(id, record.hp);
        }else{
            if(Game.status == GameStatus.Game)
                Game.goOnRandomSpawn(id);
        }

        Game.Controller.onEnter(id);

        if(record != null)
        {
            setPlayerHearts(id, record.hearts);
            setPlayerBuildingPoints(id, record.buildingPoints);
            forceUnusualSkillTimeout(id, 0, record.timeout0);
            forceUnusualSkillTimeout(id, 1, record.timeout1);
            forceUnusualSkillTimeout(id, 2, record.timeout2);
            forceUnusualSkillTimeout(id, 3, record.timeout3);
            forceUnusualSkillTimeout(id, 4, record.timeout4);

            bonus = [record.bonus1, record.bonus2, record.bonus3, record.bonus4];
        }

        spawnPlayer(id);

        isObserver = false;

        if(getPlayerHearts(id) == 0 || (Game.status == GameStatus.Game && Game.timer <= 360))
        {
            isObserver = true;
            enterObserver(id);
            return;
        }
    }

    function onStartGame()
    {
        inGame = true;

        setPlayerHearts(id, 3);
        setPlayerBuildingPoints(id, 10);

        setPlayerMaxMana(id, 100000);
        setPlayerMana(id, 100000);

        setPlayerMaxHealth(id, 1000);
        setPlayerHealth(id, 1000);

        setPlayerStrength(id, 200);
        setPlayerDexterity(id, 200);
        setPlayerMagicLevel(id, 6);

        setPlayerSkillWeapon(id, 0, 100);
        setPlayerSkillWeapon(id, 1, 100);
        setPlayerSkillWeapon(id, 2, 500);
        setPlayerSkillWeapon(id, 3, 500);

        setPlayerScale(id, 1.0, 1.0, 1.0);
        setPlayerInvisible(id, false);

        if(hasPlayerItem(id, "ITAR_PRISONER") == 0)
        {
            giveItem(id, "ITAR_PRISONER", 1);
            giveItem(id, "ITMW_SHORTSWORD4", 1);
            equipItem(id, Items.id("ITAR_PRISONER"));
            equipItem(id, Items.id("ITMW_SHORTSWORD4"));
        }

        isObserver = false;
    }

    function onEndGame()
    {
        isObserver = false;
        inGame = false;
        readyToPlay = false;
        choosenWorld = -1;

        clearItems(id);
    }

    function CountPoints() {
        local _deaths = deaths;
        local _kills = kills * 2;
        local _winGames = winGames * 10;

        local _points = _kills + _winGames - _deaths;
        if(_points < 0)
            _points = 0;

        return _points;
    }

    function onSecond() {
        if(!isPlayerSpawned(id))
            return;

        local pos = getPlayerPosition(id);

        if(getDistance3d(position.x, position.y, position.z, pos.x, pos.y, pos.z) > 100)
        {
		    position.x = pos.x; position.y = pos.y; position.z = pos.z;
		    callEvent("onPlayerPositionChange", id, position.x, position.y, position.z);
        }

        local hpStan = getPlayerHealth(id);

        if(hpStan > 0)
        {
            hpStan = hpStan + 5;
            if(hpStan > 1000)
                hpStan = 1000;

            setPlayerHealth(id, hpStan);
        }

        if(inGame && !isObserver && hpStan > 0)
        {
            if(Game.timer < 1800)
            {
                if(Game.circle.checkIsIn(pos.x, pos.y, pos.z) == false)
                {
                    addEffect(id, "spellFX_RingRitual1");
                    hpStan = hpStan - 50;
                    if(hpStan < 0)
                        hpStan = 0;

                    setPlayerHealth(id, hpStan);
                }
            }

            foreach(_indexBonus, _valueBonus in bonus)
            {
                if(_valueBonus <= 0)
                    continue;

                bonus[_indexBonus] = bonus[_indexBonus] - 1;

                if(bonus[_indexBonus] <= 0)
                {
                    sendMessageToPlayer(id, 255, 0, 0, "Wygas� ci bonus.");
                }
            }
        }

        if(invisibility > 0) {
            invisibility = invisibility - 1;
            if(invisibility <= 0)
            {
                invisibility = 0;
                setPlayerInvisible(id, false);
                smallNote(id, "Jeste� ju� widoczny!");
            }
        }
    }

    function clear() {
        removePlayerFromSlot(id);

        if(Game.status == GameStatus.Lobby)
        {
            if(choosenWorld != -1)
            {
                Game.votes[choosenWorld] = Game.votes[choosenWorld] - 1;
            }
        }

        account = null;

        local pos = getPlayerPosition(id);
        local tim = getPlayerSkills(id).timeouts;
        Memory.UserRecord(getPlayerSerial(id), getPlayerName(id), pos.x, pos.y, pos.z, getPlayerHealth(id), hearts, buildingPoints, tim[0], tim[1], tim[2], tim[3], tim[4], points, bonus[0], bonus[1], bonus[2], bonus[3]);

        Game.checkTotalInGamePlayers();
        inGame = false;
        isAdmin = false;

        protection = [0,0,0,0,0,0,0,0];

        saveAccount(id);
        pwMode = false;
        readyToPlay = false;

        avatarId = 0;
        deaths = 0;
        kills = 0;
        winGames = 0;

        temporaryDMG = -1;
        lastUsedSpell = -1;
        lastHitInterval = -1;
        choosenWorld = -1;

		hitMultiplier = 0;
        hitBonus = 0;
        smellyHit = 0;
        shieldBonus = 0;

        bodyModel = "Hum_Body_Naked0";
        bodyTxt = 2;
        headModel = "Hum_Head_FatBald";
        headTxt = 22;

        str = 10;
        dex = 10;
        hp = 40;
        maxHp = 40;
        mana = 10;
        maxMana = 10;

        mounting = false;

        buildingPoints = 0;
        hearts = 3;
        gridIndex = -1;

        isObserver = false;

        bonus = [0,0,0,0];

        foreach(chunk in chunks)
            grid.removePlayer(chunk, id);

        chunks = [];

        points = 3;
        invisibility = 0;

        setPlayerClan(id, -1);

        Game.Controller.onExit(id);
    }
}

Player <- [];

for(local i = 0; i<= getMaxSlots(); i++)
    Player.append(_Player(i));

getPlayer <- @(id) Player[id];
getPlayers <- @() Player;

setTimer(function () {
    foreach(player in getPlayers())
        player.onSecond();
}, 1000, 0)

//Helpers

function setPlayerClan(pid, value)
{
	Player[pid].clanId = value;

    if(value == -1)
        setPlayerColor(pid, 255, 255, 255);
    else {
        if(Clan.get(value) == null) {
            setPlayerClan(pid, -1);
            return;
        }
        local color = Clan.get(value).color;
        setPlayerColor(pid, color.r, color.g, color.b);
    }

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.Clan);
    packet.writeInt16(pid);
    packet.writeInt16(value);
    packet.sendToAll(RELIABLE_ORDERED);
}

function getPlayerClan(pid)
{
	return Player[pid].clanId;
}

function setPlayerBuildingPoints(pid, value)
{
	Player[pid].buildingPoints = value;

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.BuildingPoints);
    packet.writeInt16(value);
    packet.send(pid, RELIABLE_ORDERED);
}

function getPlayerBuildingPoints(pid)
{
	return Player[pid].buildingPoints;
}

function setPlayerHearts(pid, value)
{
	Player[pid].hearts = value;

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.Hearts);
    packet.writeInt16(value);
    packet.send(pid, RELIABLE_ORDERED);
}

function getPlayerHearts(pid)
{
	return Player[pid].hearts;
}

function setPlayerTemporaryDMG(pid, value)
{
	Player[pid].temporaryDMG = value;
}

function getPlayerTemporaryDMG(pid)
{
	return Player[pid].temporaryDMG;
}

function setPlayerMagicWeapon(pid, value)
{
	Player[pid].lastUsedSpell = value;
}

function getPlayerMagicWeapon(pid)
{
	return Player[pid].lastUsedSpell;
}

function setPlayerLastHitInterval(pid, value)
{
	Player[pid].lastHitInterval = value;
}

function getPlayerLastHitInterval(pid)
{
	return Player[pid].lastHitInterval;
}

function setPlayerHitMultiplier(pid, value)
{
	Player[pid].hitMultiplier = value;
}

function getPlayerHitMultiplier(pid)
{
	return Player[pid].hitMultiplier;
}

function setPlayerProtection(pid, id, value)
{
	Player[pid].protection[id] = value;
}

function getPlayerProtection(pid, id)
{
	return Player[pid].protection[id];
}

function setPlayerMounting(pid, value)
{
	Player[pid].mounting = value;
}

function getPlayerMounting(pid)
{
	return Player[pid].mounting;
}

function getPlayerObserver(pid)
{
    return Player[pid].isObserver;
}

function setPlayerObserver(pid, value)
{
    Player[pid].isObserver = value;
}

function givePlayerBonus(pid, type) {
    local obj = getPlayer(pid);
    switch(type)
    {
        case PlayerBonus.DefenseSmall:
            obj.bonus[PlayerBonus.DefenseSmall] = 180;
        break;
        case PlayerBonus.Defense:
            obj.bonus[PlayerBonus.Defense] = 300;
        break;
        case PlayerBonus.Damage:
            obj.bonus[PlayerBonus.Damage] = 300;
        break;
        case PlayerBonus.DamageSmall:
            obj.bonus[PlayerBonus.DamageSmall] = 180;
        break;
    }
}