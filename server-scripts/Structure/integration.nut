
addPacketListener(Packets.Structure, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case StructurePackets.Add:
            local btype = packet.readInt8();
            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();
            local angle = packet.readInt16();

            local requiredPoints = 3;
            switch(btype)
            {
                case StructureType.Box:
                    requiredPoints = 1;
                break;
                case StructureType.Bush:
                    requiredPoints = 2;
                break;
                case StructureType.WallHorizontal:
                    requiredPoints = 5;
                break;
                case StructureType.Wall:
                    requiredPoints = 5;
                break;
                case StructureType.Stairs:
                    requiredPoints = 4;
                break;
                case StructureType.Krate:
                    requiredPoints = 7;
                break;
            }

            if(getPlayerBuildingPoints(pid) >= requiredPoints)
            {
                setPlayerBuildingPoints(pid, getPlayerBuildingPoints(pid) - requiredPoints);
            }else{
                smallNote(pid, "Posiadasz "+getPlayerBuildingPoints(pid) + " pkt. budowy.");
                return;
            }

            Structure(btype,posx,posy,posz,angle, pid);
        break;
        case StructurePackets.Remove:
        break;
        case StructurePackets.Hit:
            try {
                local struct = getStructure(packet.readInt16());
                struct.onGetHit(pid);
            }catch(exception){}
        break;
    }
})
