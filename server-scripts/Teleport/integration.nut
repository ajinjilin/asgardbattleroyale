addPacketListener(Packets.Teleport, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case TeleportPackets.Use:
            local teleportObject = getTeleport(packet.readInt16());
            teleportObject.use(pid);
        break;
    }
})