const DROP_AMOUNT_FROM_BOT = 2;
const DROP_AMOUNT_FROM_LOCKET = 4;

Worlds <- {
    "Jarkendar" : "ADDON\\ADDONWORLD.ZEN",
    "Dolina" : "OLDWORLD\\OLDWORLD.ZEN",
    "Khorinis" : "NEWWORLD\\NEWWORLD.ZEN",
}

enum PlayerBonus {
    DefenseSmall,
    Defense,
    Damage,
    DamageSmall,
}
enum PlayerGUIEnum {
    Start,
    Lobby,
    Landing,
    Mounting,
    Menu,
    Ranking,
    Statistics,
    Detector,
    HelpMenu,
    Building,
    ClanList,
    ClanInfo,
    Map,
    Observer,
    Net,
    Animation,
}
enum Packets
{
    AntyCheat,
    Helpers,
    Game,
    Player,
    Items,
    Bots,
    Draws,
    Clan,
    Teleport,
    Award,
    Ranking,
    SmallNotes,
    Message,
    Structure,
}
enum StructurePackets
{
    Add,
    Remove,
    Hit
}
enum AntyCheatPackets
{
    Makro,
    Tp,
    Statistics,
}
enum MesssagePackets
{
    Receive,
    Send
}
enum MessageType
{
    Received,
    Sended,
}
enum SmallNotesPackets
{
    Add,
}
enum RankingPackets
{
    SendOnline,
    SendRanking,
    Page,
}
enum AwardPackets
{
    Create,
    Remove,
    Attack,
}
enum TeleportPackets
{
    Use,
}
enum ClanPackets
{
    Create,
    Destroy,
    Color,
    AddMember,
    RemoveMember,
    JoinClan,
    Info
}
enum ClanRank
{
    Member,
    Leader
}
enum DrawPackets
{
    Create,
    Destroy
}
enum PlayerPackets
{
    UseSkill,
    ResetSkill,
    Mount,
    Admin,
    Hearts,
    AdditionalPackets,
    UseItem,
    Effect,
    Orc,
    Observer,
    PlayVideo,
    BuildingPoints,
    Clan,
    ClearIventory,
    UpdateSkill,
}
enum ItemPackets
{
	GiveItem,
	RemoveItem,
	Equip,
	UnEquip,
	UseItem,
    EqUse,
}
enum HelperPackets
{
    AddPlayer,
    RemovePlayer,
    AddLobby,
    Print,
    Kick
}
enum GamePackets
{
    Enter,
    Exit,
    StartGame,
    EndGame,
    Timer,
    ResetState
}
enum BotPackets
{
    Init,
    Streamer,
    Synchronization,
    Attack,
    Health,
    Position,
    Remove,
    Spawn,
    Unspawn,
    Angle,
    Animation,
    WeaponMode,
    BossAttack
}
enum BotScheme
{
    Monster,
    Mount,
    Helper,
    Boss
}
enum BotAI
{
    Search,
    Run,
    Attack,
    Dead,
}
enum BotBossAttack
{
    Range,
    RangeAdditional,
    Melee,
    MeleeAdditional
}
enum GameStatus
{
    Lobby,
    Game,
    Result
}
enum PlayerSkill
{
    Arrows,
    DeathKiss,
    Flash,
    Shield,
    Devil,
}
enum NoteType
{
    Damage,
    Information,
    Kill,
    Skill
}
enum StructureType
{
    Locket,
    Box,
    Bush,
    WoodenWall,
    Wall,
    WallHorizontal
    Stairs
    Krate,
    Bonus,
}

const PROTECTION_BLUNT = 0;
const PROTECTION_EDGE = 1;
const PROTECTION_FIRE = 2;
const PROTECTION_MAGIC = 3;
const PROTECTION_POINT = 4;
const PROTECTION_HUMAN = 5;
const PROTECTION_BEAST = 6;
const PROTECTION_UNDEAD = 7;

const DAMAGE_BLUNT = 0;
const DAMAGE_EDGE = 1;
const DAMAGE_FIRE = 2;
const DAMAGE_MAGIC = 3;
const DAMAGE_POINT = 4;
const DAMAGE_HUMAN = 5;
const DAMAGE_BEAST = 6;
const DAMAGE_UNDEAD = 7;

const ITEM_KAT_OTHER = 0;
const ITEM_KAT_MELEE = 1;
const ITEM_KAT_RANGED = 2;
const ITEM_KAT_FOOD = 3;
const ITEM_KAT_MAGIC = 4;
const ITEM_KAT_ARMOR = 5;
const ITEM_KAT_SCROLL = 6;
const ITEM_KAT_RUNE = 7;
const ITEM_KAT_HERB = 8;

const ITEM_VAL_NORMAL = 0;
const ITEM_VAL_EPIC = 1;
const ITEM_VAL_LEGENDARY = 2;
const ITEM_VAL_GODLIKE = 3;