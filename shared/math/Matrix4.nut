local matrixIdentity = null
local matrixZero = null

// class

/*
	TO DO:
		Other:
			-Niby naprawione zostało Quaternion.euler, ale coś mi się wydaje, że dalej coś jest nie tak, do sprawdzenia
			-sprawdzić, czy Quaternion.eulerAngles działa jak powinno

		Matrix:
			-fixnąć rotacje (nie działa jak powinno)
			-dodać validTRS (wewnątrz metody są linki)
			-dodać TRS 
			-dodać setTRS (prawie to samo co TRS)

	// row-column order m[row][column]

	// rotate
	//https://github.com/KhronosGroup/glTF-Validator/blob/794d8d8ed5ffab23948b8f5883690c23626f8a81/lib/src/utils.dart#L657-L672

	Matrix4
	(
		vecRight.x,		vecUp.x,	vecForward.x,	translation.x
		vecRight.y,		vecUp.y,	vecForward.y,	translation.y
		vecRight.z,		vecUp.z,	vecForward.z,	translation.z
		0,				0,			0,				1
	)
*/

class Matrix4
{
	m = null

    constructor(...)
    {
		m = 
		[
			Vector4(0, 0, 0, 0),
			Vector4(0, 0, 0, 0),
			Vector4(0, 0, 0, 0),
			Vector4(0, 0, 0, 0)
		]

        switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Matrix4))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Matrix4'"

				local matrix = vargv[0]

				setRow(0, matrix.getRow(0))
				setRow(1, matrix.getRow(1))
				setRow(2, matrix.getRow(2))
				setRow(3, matrix.getRow(3))
			
				break
				
			// initial constructor
			
			case 4:
				if (!validArgument(vargv[0], Vector4, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector4|table'"

				if (!validArgument(vargv[1], Vector4, "table"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'Vector4|table'"

				if (!validArgument(vargv[2], Vector4, "table"))
					throw "parameter 3 has an invalid type '"+type(vargv[2])+"' ; expected: 'Vector4|table'"

				if (!validArgument(vargv[3], Vector4, "table"))
					throw "parameter 4 has an invalid type '"+type(vargv[3])+"' ; expected: 'Vector4|table'"

				setColumn(0, vargv[0])
				setColumn(1, vargv[1])
				setColumn(2, vargv[2])
				setColumn(3, vargv[3])

				break

			// initial constructor

			case 16:			
				for (local i = 0; i < 16; ++i)
				{			
					if (type(vargv[i]) != "integer" && type(vargv[i]) != "float")
						throw "parameter " + (i + 1) + " has an invalid type '"+type(vargv[i])+"' ; expected: 'integer|float'"
				}
				
				m[0][0] = vargv[0].tofloat();  m[0][1] = vargv[1].tofloat();  m[0][2] = vargv[2].tofloat();  m[0][3] = vargv[3].tofloat();
				m[1][0] = vargv[4].tofloat();  m[1][1] = vargv[5].tofloat();  m[1][2] = vargv[6].tofloat();  m[1][3] = vargv[7].tofloat();
				m[2][0] = vargv[8].tofloat();  m[2][1] = vargv[9].tofloat();  m[2][2] = vargv[10].tofloat(); m[2][3] = vargv[11].tofloat();
				m[3][0] = vargv[12].tofloat(); m[3][1] = vargv[13].tofloat(); m[3][2] = vargv[14].tofloat(); m[3][3] = vargv[15].tofloat();
			
				break
		}
    }

	// overloaded operators

	function _mul(arg)
	{
		local result

		if (arg instanceof Matrix4)
		{
			result = Matrix4(this)

			local column0 = arg.getColumn(0)
			local column1 = arg.getColumn(1)
			local column2 = arg.getColumn(2)
			local column3 = arg.getColumn(3)

			result.m[0][0] = Vector4.dot(getRow(0), column0)
			result.m[0][1] = Vector4.dot(getRow(0), column1)
			result.m[0][2] = Vector4.dot(getRow(0), column2)
			result.m[0][3] = Vector4.dot(getRow(0), column3)

			result.m[1][0] = Vector4.dot(getRow(1), column0)
			result.m[1][1] = Vector4.dot(getRow(1), column1)
			result.m[1][2] = Vector4.dot(getRow(1), column2)
			result.m[1][3] = Vector4.dot(getRow(1), column3)

			result.m[2][0] = Vector4.dot(getRow(2), column0)
			result.m[2][1] = Vector4.dot(getRow(2), column1)
			result.m[2][2] = Vector4.dot(getRow(2), column2)
			result.m[2][3] = Vector4.dot(getRow(2), column3)

			result.m[3][0] = Vector4.dot(getRow(3), column0)
			result.m[3][1] = Vector4.dot(getRow(3), column1)
			result.m[3][2] = Vector4.dot(getRow(3), column2)
			result.m[3][3] = Vector4.dot(getRow(3), column3)
		}
		else if (arg instanceof Vector4)
		{
			result = Vector4
			(
				Vector4.dot(getRow(0), arg),
				Vector4.dot(getRow(1), arg),
				Vector4.dot(getRow(2), arg),
				Vector4.dot(getRow(3), arg)
			)
		}

		return result
	}

    // methods

	function mul(scalar)
	{
		if (!validArgument(scalar, "integer", "float"))
			throw "parameter 1 has an invalid type '"+type(scalar)+"' ; expected: 'integer|float'"

		m[0][0] *= scalar
		m[0][1] *= scalar
		m[0][2] *= scalar
		m[0][3] *= scalar

		m[1][0] *= scalar
		m[1][1] *= scalar
		m[1][2] *= scalar
		m[1][3] *= scalar

		m[2][0] *= scalar
		m[2][1] *= scalar
		m[2][2] *= scalar
		m[2][3] *= scalar

		m[3][0] *= scalar
		m[3][1] *= scalar
		m[3][2] *= scalar
		m[3][3] *= scalar
	}

	function getTranslation()
	{
		return Vector3(m[0][3], m[1][3], m[2][3])
	}

	function setTranslation(position)
	{
		if (!validArgument(position, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(position)+"' ; expected: 'Vector3|table'"

		m[0][3] = position.x
		m[1][3] = position.y
		m[2][3] = position.z
	}

	function lossyScale()
	{
		return Vector3(getRightVector().length(), getUpVector().length(), getAtVector().length())
	}

	function getForwardVector()
	{
		return Vector3(m[0][2], m[1][2], m[2][2])
	}

	function setForwardVector(forward)
	{
		if (!validArgument(forward, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(forward)+"' ; expected: 'Vector3|table'"

		m[0][2] = forward.x
		m[1][2] = forward.y
		m[2][2] = forward.z
	}

	function getUpVector()
	{
		return Vector3(m[0][1], m[1][1], m[2][1])
	}

	function setUpVector(up)
	{
		if (!validArgument(up, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(up)+"' ; expected: 'Vector3|table'"

		m[0][1] = up.x
		m[1][1] = up.y
		m[2][1] = up.z
	}

	function getRightVector()
	{
		return Vector3(m[0][0], m[1][0], m[2][0])
	}

	function setRightVector(right)
	{
		if (!validArgument(right, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(right)+"' ; expected: 'Vector3|table'"

		m[0][0] = right.x
		m[1][0] = right.y
		m[2][0] = right.z
	}

	function transpose()
	{
		return Matrix4
		(
			m[0][0], m[1][0], m[2][0], m[3][0],
			m[0][1], m[1][1], m[2][1], m[3][1],
			m[0][2], m[1][2], m[2][2], m[3][2],
			m[0][3], m[1][3], m[2][3], m[3][3]
		)
	}

	function determinant()
	{
		//http://www.euclideanspace.com/maths/algebra/matrix/functions/determinant/fourD/index.htm

		return  m[0][3] * m[1][2] * m[2][1] * m[3][0] - m[0][2] * m[1][3] * m[2][1] * m[3][0] 
			-	m[0][3] * m[1][1] * m[2][2] * m[3][0] + m[0][1] * m[1][3] * m[2][2] * m[3][0]
			+	m[0][2] * m[1][1] * m[2][3] * m[3][0] - m[0][1] * m[1][2] * m[2][3] * m[3][0]
			- 	m[0][3] * m[1][2] * m[2][0] * m[3][1] + m[0][2] * m[1][3] * m[2][0] * m[3][1]
			+	m[0][3] * m[1][0] * m[2][2] * m[3][1] - m[0][0] * m[1][3] * m[2][2] * m[3][1]
			- 	m[0][2] * m[1][0] * m[2][3] * m[3][1] + m[0][0] * m[1][2] * m[2][3] * m[3][1]
			+	m[0][3] * m[1][1] * m[2][0] * m[3][2] - m[0][1] * m[1][3] * m[2][0] * m[3][2]
			-	m[0][3] * m[1][0] * m[2][1] * m[3][2] + m[0][0] * m[1][3] * m[2][1] * m[3][2]
			+	m[0][1] * m[1][0] * m[2][3] * m[3][2] - m[0][0] * m[1][1] * m[2][3] * m[3][2]
			-	m[0][2] * m[1][1] * m[2][0] * m[3][3] + m[0][1] * m[1][2] * m[2][0] * m[3][3]
			+	m[0][2] * m[1][0] * m[2][1] * m[3][3] - m[0][0] * m[1][2] * m[2][1] * m[3][3]
			-	m[0][1] * m[1][0] * m[2][2] * m[3][3] + m[0][0] * m[1][1] * m[2][2] * m[3][3]
	}

	function inverse()
	{
		//http://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm

		local result = Matrix4()

		result.m[0][0] = m[1][2]*m[2][3]*m[3][1] - m[1][3]*m[2][2]*m[3][1] + m[1][3]*m[2][1]*m[3][2] - m[1][1]*m[2][3]*m[3][2] - m[1][2]*m[2][1]*m[3][3] + m[1][1]*m[2][2]*m[3][3]
		result.m[0][1] = m[0][3]*m[2][2]*m[3][1] - m[0][2]*m[2][3]*m[3][1] - m[0][3]*m[2][1]*m[3][2] + m[0][1]*m[2][3]*m[3][2] + m[0][2]*m[2][1]*m[3][3] - m[0][1]*m[2][2]*m[3][3]
		result.m[0][2] = m[0][2]*m[1][3]*m[3][1] - m[0][3]*m[1][2]*m[3][1] + m[0][3]*m[1][1]*m[3][2] - m[0][1]*m[1][3]*m[3][2] - m[0][2]*m[1][1]*m[3][3] + m[0][1]*m[1][2]*m[3][3]
		result.m[0][3] = m[0][3]*m[1][2]*m[2][1] - m[0][2]*m[1][3]*m[2][1] - m[0][3]*m[1][1]*m[2][2] + m[0][1]*m[1][3]*m[2][2] + m[0][2]*m[1][1]*m[2][3] - m[0][1]*m[1][2]*m[2][3]
		result.m[1][0] = m[1][3]*m[2][2]*m[3][0] - m[1][2]*m[2][3]*m[3][0] - m[1][3]*m[2][0]*m[3][2] + m[1][0]*m[2][3]*m[3][2] + m[1][2]*m[2][0]*m[3][3] - m[1][0]*m[2][2]*m[3][3]
		result.m[1][1] = m[0][2]*m[2][3]*m[3][0] - m[0][3]*m[2][2]*m[3][0] + m[0][3]*m[2][0]*m[3][2] - m[0][0]*m[2][3]*m[3][2] - m[0][2]*m[2][0]*m[3][3] + m[0][0]*m[2][2]*m[3][3]
		result.m[1][2] = m[0][3]*m[1][2]*m[3][0] - m[0][2]*m[1][3]*m[3][0] - m[0][3]*m[1][0]*m[3][2] + m[0][0]*m[1][3]*m[3][2] + m[0][2]*m[1][0]*m[3][3] - m[0][0]*m[1][2]*m[3][3]
		result.m[1][3] = m[0][2]*m[1][3]*m[2][0] - m[0][3]*m[1][2]*m[2][0] + m[0][3]*m[1][0]*m[2][2] - m[0][0]*m[1][3]*m[2][2] - m[0][2]*m[1][0]*m[2][3] + m[0][0]*m[1][2]*m[2][3]
		result.m[2][0] = m[1][1]*m[2][3]*m[3][0] - m[1][3]*m[2][1]*m[3][0] + m[1][3]*m[2][0]*m[3][1] - m[1][0]*m[2][3]*m[3][1] - m[1][1]*m[2][0]*m[3][3] + m[1][0]*m[2][1]*m[3][3]
		result.m[2][1] = m[0][3]*m[2][1]*m[3][0] - m[0][1]*m[2][3]*m[3][0] - m[0][3]*m[2][0]*m[3][1] + m[0][0]*m[2][3]*m[3][1] + m[0][1]*m[2][0]*m[3][3] - m[0][0]*m[2][1]*m[3][3]
		result.m[2][2] = m[0][1]*m[1][3]*m[3][0] - m[0][3]*m[1][1]*m[3][0] + m[0][3]*m[1][0]*m[3][1] - m[0][0]*m[1][3]*m[3][1] - m[0][1]*m[1][0]*m[3][3] + m[0][0]*m[1][1]*m[3][3]
		result.m[2][3] = m[0][3]*m[1][1]*m[2][0] - m[0][1]*m[1][3]*m[2][0] - m[0][3]*m[1][0]*m[2][1] + m[0][0]*m[1][3]*m[2][1] + m[0][1]*m[1][0]*m[2][3] - m[0][0]*m[1][1]*m[2][3]
		result.m[3][0] = m[1][2]*m[2][1]*m[3][0] - m[1][1]*m[2][2]*m[3][0] - m[1][2]*m[2][0]*m[3][1] + m[1][0]*m[2][2]*m[3][1] + m[1][1]*m[2][0]*m[3][2] - m[1][0]*m[2][1]*m[3][2]
		result.m[3][1] = m[0][1]*m[2][2]*m[3][0] - m[0][2]*m[2][1]*m[3][0] + m[0][2]*m[2][0]*m[3][1] - m[0][0]*m[2][2]*m[3][1] - m[0][1]*m[2][0]*m[3][2] + m[0][0]*m[2][1]*m[3][2]
		result.m[3][2] = m[0][2]*m[1][1]*m[3][0] - m[0][1]*m[1][2]*m[3][0] - m[0][2]*m[1][0]*m[3][1] + m[0][0]*m[1][2]*m[3][1] + m[0][1]*m[1][0]*m[3][2] - m[0][0]*m[1][1]*m[3][2]
		result.m[3][3] = m[0][1]*m[1][2]*m[2][0] - m[0][2]*m[1][1]*m[2][0] + m[0][2]*m[1][0]*m[2][1] - m[0][0]*m[1][2]*m[2][1] - m[0][1]*m[1][0]*m[2][2] + m[0][0]*m[1][1]*m[2][2]
		
		local scalar = determinant()

		if (scalar != 0.0)
			scalar = 1.0 / scalar

		result.mul(scalar)

		return result
	}

	function decomposeProjection()
	{
		//https://stackoverflow.com/questions/10830293/decompose-projection-matrix44-to-left-right-bottom-top-near-and-far-boundary

		local decomposition = FrustumPlanes()

		if (m[3][3] == 1)
		{ //orthographic matrix
			decomposition.zNear   =  (1.0 + m[2][3]) / m[2][2]
			decomposition.zFar    = -(1.0 - m[2][3]) / m[2][2]
			decomposition.bottom =  (1.0 - m[1][3]) / m[1][1]
			decomposition.top    = -(1.0 + m[1][3]) / m[1][1]
			decomposition.left   = -(1.0 + m[0][3]) / m[0][0]
			decomposition.right  =  (1.0 - m[0][3]) / m[0][0]
		}
		else if (m[3][3] == 0)
		{ //perspective matrix
			decomposition.zNear   = m[2][3] / (m[2][2] - 1.0)
			decomposition.zFar    = m[2][3] / (m[2][2] + 1.0)
			decomposition.bottom = decomposition.zNear * (m[1][2] - 1.0) / m[1][1]
			decomposition.top    = decomposition.zNear * (m[1][2] + 1.0) / m[1][1]
			decomposition.left   = decomposition.zNear * (m[0][2] - 1.0) / m[0][0]
			decomposition.right  = decomposition.zNear * (m[0][2] + 1.0) / m[0][0]
		}

		return decomposition
	}

	function isIdentity()
	{
		return 	m[0][0] == 1.0 && m[0][1] == 0.0 && m[0][2] == 0.0 && m[0][3] == 0.0
		&&		m[1][0] == 0.0 && m[1][1] == 1.0 && m[1][2] == 0.0 && m[1][3] == 0.0
		&&		m[2][0] == 0.0 && m[2][1] == 0.0 && m[2][2] == 1.0 && m[2][3] == 0.0
		&&		m[3][0] == 0.0 && m[3][1] == 0.0 && m[3][2] == 0.0 && m[3][3] == 1.0
	}

	function validTRS()
	{
		//https://github.com/gltf-rs/gltf/issues/21
		//https://github.com/KhronosGroup/glTF-Validator/blob/794d8d8ed5ffab23948b8f5883690c23626f8a81/lib/src/utils.dart#L657-L672

		if (m[3][0] != 0.0 && m[3][1] != 0.0 && m[3][2] != 0.0 && m[3][3] != 1.0)
			return false
	}

	function setTRS(pos, q, s)
	{
		if (!validArgument(pos, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(pos)+"' ; expected: 'Vector3|table'"

		if (!(q instanceof Quaternion))
			throw "parameter 2 has an invalid type '"+type(q)+"' ; expected: 'Quaternion'"

		if (!validArgument(s, Vector3, "table"))
			throw "parameter 3 has an invalid type '"+type(s)+"' ; expected: 'Vector3|table'"

		setTranslation(pos)
	}

	function getColumn(index)
	{
		if (type(index) != "integer")
			throw "parameter 1 has an invalid type '"+type(index)+"' ; expected: 'integer'"

		return Vector4(m[0][index], m[1][index], m[2][index], m[3][index])
	}

	function setColumn(index, column)
	{
		if (type(index) != "integer")
			throw "parameter 1 has an invalid type '"+type(index)+"' ; expected: 'integer'"

		if (!(column instanceof Vector4))
			throw "parameter 2 has an invalid type '"+type(column)+"' ; expected: 'Vector4'"

		m[0][index] = column[0]
		m[1][index] = column[1]
		m[2][index] = column[2]
		m[3][index] = column[3]
	}

	function getRow(index)
	{
		if (type(index) != "integer")
			throw "parameter 1 has an invalid type '"+type(index)+"' ; expected: 'integer'"

		return m[index]
	}

	function setRow(index, row)
	{
		if (type(index) != "integer")
			throw "parameter 1 has an invalid type '"+type(index)+"' ; expected: 'integer'"

		if (!(row instanceof Vector4))
			throw "parameter 2 has an invalid type '"+type(row)+"' ; expected: 'Vector4'"

		m[index][0] = row[0]
		m[index][1] = row[1]
		m[index][2] = row[2]
		m[index][3] = row[3]
	}

	function multiplyPoint3x4(point)
	{
		if (!validArgument(point, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(point)+"' ; expected: 'Vector3|table'"

		local result = Vector3()

		result.x = m[0][0] * point.x + m[0][1] * point.y + m[0][2] * point.z + m[0][3]
		result.y = m[1][0] * point.x + m[1][1] * point.y + m[1][2] * point.z + m[1][3]
		result.z = m[2][0] * point.x + m[2][1] * point.y + m[2][2] * point.z + m[2][3]

		return result
	}

	function multiplyPoint(point)
	{
		local result = multiplyPoint3x4(point)
		local w  = m[3][0] * point.x + m[3][1] * point.y + m[3][2] * point.z + m[3][3]

		result.mul(1.0 / w)

		return result
	}

	function multiplyVector(vec)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
	
		local result = Vector3()

		result.x = m[0][0] * vec.x + m[0][1] * vec.y + m[0][2] * vec.z
		result.y = m[1][0] * vec.x + m[1][1] * vec.y + m[1][2] * vec.z
		result.z = m[2][0] * vec.x + m[2][1] * vec.y + m[2][2] * vec.z

		return result
	}

	function tostring(pattern = "Matrix4\n(\n\t%f, %f, %f, %f\n\t%f, %f, %f, %f\n\t%f, %f, %f, %f\n\t%f, %f, %f, %f\n)")
	{
		return format(pattern,
			m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3]
		)
	}

    // static methods

    static function identity() { return matrixIdentity }
    static function zero() { return matrixZero }

	static function lookAt(from, to, up)
	{
		local camForward = Vector3.normalize(to - from)
		local camRight = Vector3.normalize(Vector3.cross(up, camForward))
		local camUp = Vector3.normalize(Vector3.cross(camForward, camRight))

		if (camForward.equals(Vector3.zero()))
			camForward.set(Vector3.forward().x, Vector3.forward().y, Vector3.forward().z)

		if (camUp.equals(Vector3.zero()))
			camUp.set(Vector3.up().x, Vector3.up().y, Vector3.up().z)

		if (camRight.equals(Vector3.zero()))
			camRight.set(Vector3.right().x, Vector3.right().y, Vector3.right().z)

		local result = Matrix4()

		result.setRightVector(camRight)
		result.setUpVector(camUp)
		result.setForwardVector(camForward)
		result.setTranslation(from)

		result.m[3][3] = 1

		return result
	}

	static function ortho(left, right, bottom, top, zNear, zFar)
	{
		if (!validArgument(left, "integer", "float"))
			throw "parameter 1 has an invalid type '"+type(left)+"' ; expected: 'integer|float'"

		if (!validArgument(right, "integer", "float"))
			throw "parameter 2 has an invalid type '"+type(right)+"' ; expected: 'integer|float'"

		if (!validArgument(bottom, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(bottom)+"' ; expected: 'integer|float'"

		if (!validArgument(top, "integer", "float"))
			throw "parameter 4 has an invalid type '"+type(top)+"' ; expected: 'integer|float'"

		if (!validArgument(zNear, "integer", "float"))
			throw "parameter 5 has an invalid type '"+type(zNear)+"' ; expected: 'integer|float'"

		if (!validArgument(zFar, "integer", "float"))
			throw "parameter 6 has an invalid type '"+type(zFar)+"' ; expected: 'integer|float'"

		local result = Matrix4()

		local RMinusL = (right - left).tofloat()
		local TMinusB = (top - bottom).tofloat()
		local FMinusN = (zFar - zNear).tofloat()

		result.m[0][0] = 2.0 / RMinusL
		result.m[1][1] = 2.0 / TMinusB
		result.m[2][2] = -2.0 / FMinusN
		result.m[3][3] = 1.0

		result.m[0][3] = -(right + left) / RMinusL
		result.m[1][3] = (top + bottom) / -TMinusB
		result.m[2][3] = -(zFar + zNear) / FMinusN

		return result
	}

	static function frustum(left, right, bottom, top, zNear, zFar)
	{
		if (!validArgument(left, "integer", "float"))
			throw "parameter 1 has an invalid type '"+type(left)+"' ; expected: 'integer|float'"

		if (!validArgument(right, "integer", "float"))
			throw "parameter 2 has an invalid type '"+type(right)+"' ; expected: 'integer|float'"

		if (!validArgument(bottom, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(bottom)+"' ; expected: 'integer|float'"

		if (!validArgument(top, "integer", "float"))
			throw "parameter 4 has an invalid type '"+type(top)+"' ; expected: 'integer|float'"

		if (!validArgument(zNear, "integer", "float"))
			throw "parameter 5 has an invalid type '"+type(zNear)+"' ; expected: 'integer|float'"

		if (!validArgument(zFar, "integer", "float"))
			throw "parameter 6 has an invalid type '"+type(zFar)+"' ; expected: 'integer|float'"

		local result = Matrix4()

		local RMinusL = (right - left).tofloat()
		local TMinusB = (top - bottom).tofloat()
		local FMinusN = (zFar - zNear).tofloat()

		result.m[0][0] = (2 * zNear) / RMinusL
		result.m[1][1] = (2 * zNear) / TMinusB

		result.m[0][2] = (right + left) / RMinusL
		result.m[1][2] = (top + bottom) / TMinusB
		result.m[2][2] = -(zFar + zNear) / FMinusN
		result.m[3][2] = -1

		result.m[2][3] = -(2 * zFar * zNear) / FMinusN

		result.m[3][3] = 0

		return result
	}

	static function perspective(FOV, aspect, zNear, zFar)
	{
		if (!validArgument(FOV, "integer", "float"))
			throw "parameter 1 has an invalid type '"+type(FOV)+"' ; expected: 'integer|float'"
	
		if (!validArgument(aspect, "integer", "float"))
			throw "parameter 2 has an invalid type '"+type(aspect)+"' ; expected: 'integer|float'"

		if (!validArgument(zNear, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(zNear)+"' ; expected: 'integer|float'"

		if (!validArgument(zFar, "integer", "float"))
			throw "parameter 4 has an invalid type '"+type(zFar)+"' ; expected: 'integer|float'"

		local right = zNear * tan(FOV / 2 * deg2Rad) * aspect
		local left = -right

		local bottom = left / aspect
		local top = right / aspect

		return frustum(left, right, bottom, top, zNear, zFar)
	}

	static function translate(vec)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
	
		return Matrix4
		(
			1, 0, 0, vec.x,
			0, 1, 0, vec.y,
			0, 0, 1, vec.z,
			0, 0, 0, 1
		)
	}

	static function rotate(q)
	{
		if (!(q instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(q)+"' ; expected: 'Quaternion'"

		local mat = Matrix4(identity())

		mat.m[0][0] = q.w*q.w + q.x*q.x - q.y*q.y - q.z*q.z
		mat.m[0][1] = 2.0 * (q.x*q.y + q.w*q.z)
		mat.m[0][2] = 2.0 * (q.x*q.z - q.w*q.y)

		mat.m[1][0] = 2.0 * (q.x * q.y - q.w * q.z)
		mat.m[1][1] = q.w*q.w - q.x*q.x + q.y*q.y - q.z*q.z
		mat.m[1][2] = 2.0 * (q.y*q.z + q.w*q.x)

		mat.m[2][0] = 2.0 * (q.x*q.z + q.w*q.y)
		mat.m[2][1] = 2.0 * (q.y*q.z - q.w*q.x)
		mat.m[2][2] = q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z

		return mat
	}

	static function scale(vec)
	{
		if (!validArgument(vec, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"

		return Matrix4
		(
			vec.x, 0, 0, 0,
			0, vec.y, 0, 0,
			0, 0, vec.z, 0,
			0, 0, 0, 1
		)
	}

	static function TRS(pos, q, s)
	{
		if (!validArgument(pos, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(pos)+"' ; expected: 'Vector3|table'"

		if (!(q instanceof Quaternion))
			throw "parameter 2 has an invalid type '"+type(q)+"' ; expected: 'Quaternion'"

		if (!validArgument(s, Vector3, "table"))
			throw "parameter 3 has an invalid type '"+type(s)+"' ; expected: 'Vector3|table'"

		return translate(pos) * rotate(q) * scale(s)
	}
}

// creating shorthand matrix objects

matrixIdentity = Matrix4
(
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
)

matrixZero = Matrix4
(
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0
)
