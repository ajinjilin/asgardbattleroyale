local vecUp = null
local vecRight = null

local vecDown = null
local vecLeft = null

local vecZero = null
local vecOne = null

// class

class Vector2Int
{
	x = 0
	y = 0
	
	constructor (...)
	{
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Vector2Int, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector2Int|table'"
			
				x = vargv[0].x
				y = vargv[0].y
			
				break
				
			// initial constructor
			
			case 2:	
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
				
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
				
				x = vargv[0].tointeger()
				y = vargv[1].tointeger()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{			
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			default: 
				throw null
		}
	}
	
	function _add(arg)
	{
		local vec = Vector2Int(x, y)
	
		if (validArgument(arg, Vector2Int, "table"))
		{
			vec.x += arg.x
			vec.y += arg.y
		}
		else
		{
			vec.x += arg
			vec.y += arg
		}
		
		return vec
	}
	
	function _sub(arg)
	{		
		return this + -arg
	}
	
	function _unm()
	{		
		return Vector2Int(-x, -y)
	}
	
	function _mul(arg)
	{
		local vec = Vector2Int(x, y)
	
		if (validArgument(arg, Vector2Int, "table"))
		{
			vec.x *= arg.x
			vec.y *= arg.y
		}
		else
		{
			vec.x *= arg
			vec.y *= arg
		}
		
		return vec
	}
	
	function _div(arg)
	{
		if (validArgument(arg, Vector2Int, "table"))
			return this * Vector2Int(1 / arg.x, 1 / arg.y)
		else
			return this * (1 / arg)
	}
	
	// methods
	
	function add(arg)
	{
		if (validArgument(arg, Vector2Int, "table"))
		{
			x += arg.x
			y += arg.y
		}
		else
		{
			x += arg
			y += arg
		}
	}
	
	function sub(arg)
	{
		add(-arg)
	}
	
	function mul(arg)
	{
		if (validArgument(arg, Vector2Int, "table"))
		{
			x *= arg.x
			y *= arg.y
		}
		else
		{
			x *= arg
			y *= arg
		}
	}
	
	function div(arg)
	{
		if (validArgument(arg, Vector2Int, "table"))
		{
			x /= arg.x
			y /= arg.y
		}
		else
		{
			x /= arg
			y /= arg
		}
	}
	
	function set(x, y)
	{
		this.x = x
		this.y = y
	}
	
	function equals(vec)
	{
		if (!validArgument(vec, Vector2Int, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector2Int|table'"

        return (x == vec.x && y == vec.y)
	}

    function getHashCode()
    {
       return x ^ (y << 2)
    }
	
	function sqrLength()
	{
		return (x * x + y * y)
	}
	
	function length()
	{
		return sqrt(sqrLength())
	}
	
	function sqrMagnitude()
	{
		return sqrLength()
	}
	
	function magnitude()
	{
		return length()
	}
	
	function tostring(pattern = "Vector2Int(%f, %f)")
	{
		return format(pattern, x, y)
	}
	
	// static methods
	
	static function up() { return vecUp }
	static function right() { return vecRight }

	static function down() { return vecDown }
	static function left() { return vecLeft}

	static function zero() { return vecZero }
	static function one() { return vecOne }
	
	static function min(lhs, rhs)
	{
		if (!validArgument(lhs, Vector2Int, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector2Int|table'"
			
		if (!validArgument(rhs, Vector2Int, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector2Int|table'"
		
		return Vector2Int(min(lhs.x, rhs.x), min(lhs.y, rhs.y))
	}
	
	static function max(lhs, rhs)
	{
		if (!validArgument(lhs, Vector2Int, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector2Int|table'"
			
		if (!validArgument(rhs, Vector2Int, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector2Int|table'"
			
		return Vector2Int(max(lhs.x, rhs.x), max(lhs.y, rhs.y))
	}
	
	static function distance(a, b)
	{
		if (!validArgument(a, Vector2Int, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2Int|table'"
			
		if (!validArgument(b, Vector2Int, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector2Int|table'"
	
		return (a - b).magnitude()
	}
	
	static function scale(a, b)
	{
		if (!validArgument(a, Vector2Int, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2Int|table'"
			
		if (!validArgument(b, Vector2Int, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector2Int|table'"
		
		return a * b
	}

    static function ceilToInt(v)
    {
        if (!validArgument(v, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"

        return Vector2Int(ceil(v.x), ceil(v.y))
    }

    static function floorToInt(v)
    {
        if (!validArgument(v, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"

        return Vector2Int(floor(v.x), floor(v.y))
    }

    static function roundToInt(v)
    {
        if (!validArgument(v, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"

        return Vector2Int(round(v.x), round(v.y))
    }
}

// creating shorthand vector objects

vecUp = Vector2Int(0, 1)
vecRight = Vector2Int(1, 0)

vecDown = Vector2Int(0, -1)
vecLeft = Vector2Int(-1, 0)

vecZero = Vector2Int(0, 0)
vecOne = Vector2Int(1, 1)
