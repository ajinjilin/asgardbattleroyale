local vecForward = null
local vecUp = null
local vecRight = null

local vecBack = null
local vecDown = null
local vecLeft = null

local vecZero = null
local vecOne = null

// class

class Vector3Int
{
	x = 0
	y = 0
	z = 0
	
	constructor (...)
	{
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Vector3Int, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector3Int|table'"
			
				x = vargv[0].x
				y = vargv[0].y
				z = vargv[0].z
			
				break
				
			// initial constructor
			
			case 3:			
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
				
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[2], "integer", "float"))
					throw "parameter 3 has an invalid type '"+type(vargv[2])+"' ; expected: 'integer|float'"
				
				x = vargv[0].tointeger()
				y = vargv[1].tointeger()
				z = vargv[2].tointeger()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			case 2: return z
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{			
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			case 2:
				z = value
				break
				
			default: 
				throw null
		}
	}
	
	function _add(arg)
	{
		local vec = Vector3Int(x, y, z)
	
		if (validArgument(arg, Vector3Int, "table"))
		{
			vec.x += arg.x
			vec.y += arg.y
			vec.z += arg.z
		}
		else
		{
			vec.x += arg
			vec.y += arg
			vec.z += arg
		}
		
		return vec
	}
	
	function _sub(arg)
	{		
		return this + -arg
	}
	
	function _unm()
	{		
		return Vector3Int(-x, -y, -z)
	}
	
	function _mul(arg)
	{
		local vec = Vector3Int(x, y, z)
	
		if (validArgument(arg, Vector3Int, "table"))
		{
			vec.x *= arg.x
			vec.y *= arg.y
			vec.z *= arg.z
		}
		else
		{
			vec.x *= arg
			vec.y *= arg
			vec.z *= arg
		}
		
		return vec
	}
	
	function _div(arg)
	{
		if (validArgument(arg, Vector3Int, "table"))
			return this * Vector3Int(1 / arg.x, 1 / arg.y, 1 / arg.z)
		else
			return this * (1 / arg)
	}
	
	// methods
	
	function add(arg)
	{
		if (validArgument(arg, Vector3Int, "table"))
		{
			x += arg.x
			y += arg.y
			z += arg.z
		}
		else
		{
			x += arg
			y += arg
			z += arg
		}
	}
	
	function sub(arg)
	{
		add(-arg)
	}
	
	function mul(arg)
	{
		if (validArgument(arg, Vector3Int, "table"))
		{
			x *= arg.x
			y *= arg.y
			z *= arg.z
		}
		else
		{
			x *= arg
			y *= arg
			z *= arg
		}
	}
	
	function div(arg)
	{
		if (validArgument(arg, Vector3Int, "table"))
		{
			x /= arg.x
			y /= arg.y
			z /= arg.z
		}
		else
		{
			x /= arg
			y /= arg
			z /= arg
		}
	}
	
	function set(x, y, z)
	{
		this.x = x
		this.y = y
		this.z = z
	}
	
	function equals(vec)
	{
		if (!validArgument(vec, Vector3Int, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3Int|table'"

        return (x == vec.x && y == vec.y && z == vec.z)
	}

    function getHashCode()
    {
       return x ^ (y << 4) ^ (y >> 28) ^ (z >> 4) ^ (z << 28)
    }
	
	function sqrLength()
	{
		return (x * x + y * y + z * z)
	}
	
	function length()
	{
		return sqrt(sqrLength())
	}
	
	function sqrMagnitude()
	{
		return sqrLength()
	}
	
	function magnitude()
	{
		return length()
	}
	
	function tostring(pattern = "Vector3Int(%f, %f, %f)")
	{
		return format(pattern, x, y, z)
	}
	
	// static methods
	
	static function forward() { return vecForward }
	static function up() { return vecUp }
	static function right() { return vecRight }

	static function back() { return vecBack }
	static function down() { return vecDown }
	static function left() { return vecLeft}

	static function zero() { return vecZero }
	static function one() { return vecOne }
	
	static function min(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3Int, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3Int|table'"
			
		if (!validArgument(rhs, Vector3Int, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3Int|table'"
		
		return Vector3Int(min(lhs.x, rhs.x), min(lhs.y, rhs.y), min(lhs.z, rhs.z))
	}
	
	static function max(lhs, rhs)
	{
		if (!validArgument(lhs, Vector3Int, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector3Int|table'"
			
		if (!validArgument(rhs, Vector3Int, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector3Int|table'"
			
		return Vector3Int(max(lhs.x, rhs.x), max(lhs.y, rhs.y), max(lhs.z, rhs.z))
	}
	
	static function distance(a, b)
	{
		if (!validArgument(a, Vector3Int, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector3Int|table'"
			
		if (!validArgument(b, Vector3Int, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector3Int|table'"
	
		return (a - b).magnitude()
	}
	
	static function scale(a, b)
	{
		if (!validArgument(a, Vector3Int, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector3Int|table'"
			
		if (!validArgument(b, Vector3Int, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector3Int|table'"
		
		return a * b
	}

    static function ceilToInt(v)
    {
        if (!validArgument(v, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(v)+"' ; expected: 'Vector3|table'"

        return Vector3Int(ceil(v.x), ceil(v.y), ceil(v.z))
    }

    static function floorToInt(v)
    {
        if (!validArgument(v, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(v)+"' ; expected: 'Vector3|table'"

        return Vector3Int(floor(v.x), floor(v.y), floor(v.z))
    }

    static function roundToInt(v)
    {
        if (!validArgument(v, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(v)+"' ; expected: 'Vector3|table'"

        return Vector3Int(round(v.x), round(v.y), round(v.z))
    }
}

// creating shorthand vector objects

vecForward = Vector3Int(0, 0, 1)
vecUp = Vector3Int(0, 1, 0)
vecRight = Vector3Int(1, 0, 0)

vecBack = Vector3Int(0, 0, -1)
vecDown = Vector3Int(0, -1, 0)
vecLeft = Vector3Int(-1, 0, 0)

vecZero = Vector3Int(0, 0, 0)
vecOne = Vector3Int(1, 1, 1)
